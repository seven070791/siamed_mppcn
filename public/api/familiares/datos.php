<?php
header('Access-Control-Allow-Origin:  http://sihic.com');
header('Access-Control-Allow-Methods: GET');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
$cedula = "";
$apikey = "";
$filtroAnd = "";

if (isset($_GET['cedula_trabajador'])) 
{
	if (isset($_GET['cedula'])) 
	{
		//módulo de conexion a la BD
		require('Conexion_class.php');
		//Me conecto a la BD con las credenciales que están por allá
		$con = new Conexion_class();
		$conexion = $con->conectar();
		//Verifico si me pude conectar
		if (!isset($conexion)) {
			//('Problemas en la conexión, favor verificar  ...');
			//No se conectó;
			$respuesta =
				[
					"cedula"   => '777',
				];
		} else 
		{
			
			//die('Se Conectó al SIGEFIRRHH');
			//Se conectó;

			// titular
			if (!isset($_GET['cedula_trabajador'])) {
				$filtroWhere = "";
			} else {
				$cedula_trabajador = $_GET['cedula_trabajador'];
				$filtroWhere .= " where pt.cedula='$cedula_trabajador'";
			}
			// familiar
			if (!isset($_GET['cedula'])) {
				$filtroAnd = "";
			} else {
				$cedula = $_GET['cedula'];
				$filtroAnd .= " AND f.cedula_familiar='$cedula'";
			}

			//Armo la Consulta:
			$sqlConsulta = "SELECT ";
			$sqlConsulta .= "  f.cedula_familiar,f.nombres,f.apellidos,f.fecha_nacimiento, ";
			$sqlConsulta .= "case ";
			$sqlConsulta .= "	when f.parentesco='M' THEN 1 ";
			$sqlConsulta .= "	when f.parentesco='P' THEN 2 ";
			$sqlConsulta .= "	when f.parentesco='H' THEN 4 ";
			$sqlConsulta .= "	when f.parentesco='C' THEN 5 ";
			$sqlConsulta .= "end parentesco ";
			$sqlConsulta .= "FROM ";
			$sqlConsulta .= "(";
			$sqlConsulta .= "SELECT ";
			$sqlConsulta .= " cedula_familiar,id_personal,";
			$sqlConsulta .= "concat(trim(primer_nombre),' ',trim(segundo_nombre)) AS nombres,";
			$sqlConsulta .= " concat(trim(primer_apellido),' ',trim(segundo_apellido)) AS apellidos,";
			$sqlConsulta .= "fecha_nacimiento,parentesco ";
			$sqlConsulta .= "FROM ";
			$sqlConsulta .= "familiar ";
			$sqlConsulta .= ") AS f ";
			$sqlConsulta .= "JOIN ";
			$sqlConsulta .= "(";
			$sqlConsulta .= "SELECT ";
			$sqlConsulta .= " per.cedula ,per.id_personal ";
			$sqlConsulta .= "FROM ";
			$sqlConsulta .= " personal per ";
			$sqlConsulta .= "JOIN ";
			$sqlConsulta .= "trabajador tra ON per.id_personal=tra.id_personal ";
			$sqlConsulta .= "WHERE ";
			$sqlConsulta .= "tra.estatus='A'";
			$sqlConsulta .= ") AS pt ";
			$sqlConsulta .= "ON f.id_personal=pt.id_personal";
			$sqlConsulta .= " $filtroWhere ";
			$sqlConsulta .= "$filtroAnd";


			//Se conectó;

			//Obtengo los registros de la consulta
			$Rs = $con->registros($sqlConsulta);
			$num_rows = pg_num_rows($Rs);
			if ($num_rows>0) 
			{	
				//Verifico si hay registros
				if (!$Rs) 
				{
					//No hay registros
					//echo('No existen registros coincidentes');
					$respuesta =
						[
							"cedula"   => '000',
						];
				} 
				else 
				{
					//Hay registros
					//Esta es la Respuesta de la API
					//Aqui guardo todos los registros de la consulta
					//$respuesta=[];
					//while($registro=pg_fetch_assoc($Rs))
					//Obtengo un arreglo asociativo con los registros de la consulta 

					while ($campo = $con->arrCamposAsociativos($Rs)) {
						foreach ($campo as &$value) {
							$encoding = mb_detect_encoding($value);
							if ($encoding != 'UTF-8') {
								$value = iconv($encoding, 'UTF-8', $value);
							}
						}
						$respuesta = [
							"cedula"   => $campo['cedula_familiar'],
							"nombres"  => $campo['nombres'] . ".",
							"apellidos" => $campo['apellidos'] . ".",
							//"cargo"    => $campo['cargo'],
							//"ubicacion" => $campo['ubicacion'],
							"fecha_nacimiento" => $campo['fecha_nacimiento'],
							"parentesco" => $campo['parentesco'],
						];
					}
					
					
				}

			}else
			{
				$respuesta =
				[
				"cedula"   => '888',
				];
	
			}
		}

		
	}else
	{
		die('NO LLEGO LA CEDULA DEL FAMILIAR');
	}
}
else {
	//echo('No se recibió la cedula del titular ');
	$respuesta =
		[
			"cedula"   => '999',
		];
}
//$respuesta = json_encode($respuesta);
$json = json_encode($respuesta, JSON_UNESCAPED_UNICODE);
echo $json;





