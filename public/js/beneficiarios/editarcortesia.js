/*
 *Este es el document ready
 */
$(function() {
    $(".motivo_estatus").hide();
    $("#modal_editar").modal("show");
    $('#observacion').val('');
    var tipodesangre_id = $('#tipodesangre_id').val();
    var estadocivil_id = $('#estadocivil_id').val();
    var grado_intruccion_id = $('#grado_intruccion_id').val();
    var ocupacion_id = $('#ocupacion_id').val();
    var id_adscrito = $('#estatus_id_adscrito').val();
    var adscrito = $('#estatus_adscrito').val();

    var estatus = $('#borrado').val();

    if (estatus == 'f') {
        $('#check_borrado').attr('checked', 'checked');
        $('#check_borrado').val('false');
        $('#borrado_anterior').val('false');
    }
    if (estatus == 't') {
        $('#check_borrado').removeAttr('checked')
        $('#check_borrado').val('true')
        $('#borrado_anterior').val('true');
    }

    if (adscrito == 'f') {
        $('#adscrito').removeAttr('checked')
        $('#adscrito').val('true')
        $('.ocultar_entes').hide();
    }
    if (adscrito == 't') {

        $('#adscrito').attr('checked', 'checked');
        $('#adscrito').val('false');
    }

    llenar_combo_entes(Event, id_adscrito);
    llenar_combo_tipo_de_sangre(Event, tipodesangre_id)
    llenar_combo_estado_civil(Event, estadocivil_id)
    llenar_combo_Ocupacion(Event, ocupacion_id)
    llenar_combo_Grado_Instruccion(Event, grado_intruccion_id)

    //******METODO QUE TOMA EL ESTATUS ORIGINAL DEL CHECKBO*****
    if ($('#check_borrado').is(':checked')) {
        estatus = 'false';
        $('#estatus_actual').val(estatus);

        $('#estatus_anterior').val(estatus);
    } else {
        estatus = 'true';
        $('#estatus_actual').val(estatus);
        $('#estatus_anterior').val(estatus);
    }



});

/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_Grado_Instruccion(e, grado_intruccion_id) {
    e.preventDefault;

    url = '/listar_Grado_Instruccion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#gradointruccion').empty();
                $('#gradointruccion').append('<option value=0>Seleccione</option>');
                if (grado_intruccion_id === undefined) {
                    $.each(data, function(i, item) {
                        
                        $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === grado_intruccion_id) {
                            $('#gradointruccion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#gradointruccion_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 /*
 *Metodo para llenar el combo ocupacion
 */
function llenar_combo_Ocupacion(e, ocupacion_id) {
    e.preventDefault;

    url = '/listar_Ocupacion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#ocupacion').empty();
                $('#ocupacion').append('<option value=0>Seleccione</option>');
                if (ocupacion_id === undefined) {
                    $.each(data, function(i, item) {
                        
                        $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === ocupacion_id) {
                            $('#ocupacion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#ocupacion_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}



/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_tipo_de_sangre(e, tipodesangre_id) {

    e.preventDefault;

    url = '/listar_tipo_de_sangre';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#tipodesangre').empty();
                $('#tipodesangre').append('<option value=0>Seleccione</option>');
                if (tipodesangre_id === undefined) {
                    $.each(data, function(i, item) {
                        
                        $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item)

                        {
                            if (item.id === tipodesangre_id) {

                                $('#tipodesangre').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                                $('#tipodesangre_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            } else {
                                $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            }
                        });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 *Metodo para llenar el combo estado civil
 */
function llenar_combo_estado_civil(e, estado_civil) {
    e.preventDefault;

    url = '/listar_estado_civil';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length > 1) {
                $('#estadocivil').empty();
                $('#estadocivil').append('<option value=0>Seleccione</option>');
                if (estado_civil === undefined) {
                    $.each(data, function(i, item) {
                        
                        $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === estado_civil) {
                            $('#estadocivil').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#estadocivil_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

function llenar_combo_entes(e, id_adscrito) {

    e.preventDefault
    url = '/listar_entes_activos';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#entes_adscritos').empty();
                $('#entes_adscritos').append('<option value=0  selected disabled>Seleccione</option>');
                if (id_adscrito === undefined) {
                    $.each(data, function(i, item) {
                        
                        $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id_adscrito) {
                            $('#entes_adscritos').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#entes_adscritos_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {

                            $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

/*
 * Función para definir datatable:
 */
function listar_Cortesia() {
    $('#table_cortesia').DataTable({
        "order": [
            [0, "asc"]
        ],
        "paging": true,
        "info": true,
        "filter": true,
        "responsive": true,
        "autoWidth": true,
        //"stateSave":true,
        "ajax": {
            "url":base_url+ "/buscar_Cortesia/" + $('#cedula_trabajador').val(),
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            { data: 'nacionalidad' },
            { data: 'cedula' },
            { data: 'nombre' },
            { data: 'apellido' },
            { data: 'telefono' },
            { data: 'sexo' },
            { data: 'fecha_nac_cortesia' },
            {
                orderable: true,
                render: function(data, type, row) {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary Editar" style=" font-size:2px" data-toggle="tooltip" title="Editar" ocupacion_id =' + row.ocupacion_id + ' grado_intruccion_id=' + row.grado_intruccion_id + '  cedula=' + row.cedula + ' descripcion="' + row.descripcion + '" fecha_creacion=' + row.fecha_creacion + ' tipo_de_sangre=' + row.tipo_de_sangre + ' estadocivil_id=' + row.estado_civil + ' estatus=' + row.estatus + '> <i class="material-icons " >create</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-info Detalle" style=" font-size:2px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula=' + row.cedula + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">add_to_photos</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-primary Historial" style=" font-size:2px" data-toggle="tooltip" title="Historial  Medico" cedula=' + row.cedula + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">create_new_folder</i></a>'
                }
            }


        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }],
        }
    });
}
$('#btnAgregar').on('click', function(e) {

    $('#modal').find('#btnGuardar').hide();
    $('#modal').find('#btnActualizar').hide();
    $('#modal').find('#borrado').hide();
    $('#modal').find('#activo').hide();
    $("#modal").modal("show");

    $("#cajas").hide();
    $("#btnCerrar").hide();
    $("#btnAgregarusuario").hide();


});

//******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
$('#check_borrado').click(function() {
    $(".motivo_estatus").show();
    if ($('#check_borrado').is(':checked')) {

        estatus_check = 'false';

    } else {
        estatus_check = 'true';

    }
    $('#estatus_actual').val(estatus_check);
});



// ******************METODO PARA CAMBIAR EL ESTATUS DE ADSCRITO ********************
$('#adscrito').click(function() {
    let adscrito = $('#adscrito').val();

    if ($('#adscrito').is(':checked')) {
        adscrito = 'true';
        $(".ocultar_entes").css("display", "block");

    } else {
        adscrito = 'false';
        $(".ocultar_entes").css("display", "none");
    }

});

// ******************METODO PARA ACTUALIZAR CORTESIA********************
$(document).on('click', '#btnActulizarCortesia', function(e) {
    e.preventDefault();
    var id_cortesia = $('#id_cortesia').val();
    var cedula_trabajador = $('#cedula_trabajador').val();
    cedula_trabajador = cedula_trabajador.trim();
    var cedula_anterior = $('#cedula_anterior').val();
    var cedula = $('#cedula').val();
    cedula = cedula.trim();
    var nombre = $('#nombre_cortesia').val();
    var apellido = $('#apellido_cortesia').val();
    var nacionalidad = $('#nacionalidad').val();
    var telefono = $('#telefono').val();
    var tipo_de_sangre_id = $('#tipodesangre').val();
    var estado_civil_id = $('#estadocivil').val();
    var grado_intruccion_id = $('#gradointruccion').val();
    var ocupacion_id = $('#ocupacion').val();
    let observacion = $('#observacion').val();
    let estatus_modificado = $('#estatus_actual').val();
    let estatus_anterior = $('#estatus_anterior').val();
    let nombre_anterior=$('#nombre_cortesia_anterior').val();
    let apellido_anterior=$('#apellido_cortesia_anterior').val();
    let id_usuario = $('#id_user').val();
    if (estatus_anterior != estatus_modificado) {
        bandera_Modificacion = 'true'
    } else {
        bandera_Modificacion = 'false'
    }

    if (bandera_Modificacion == 'true' && observacion == '') {
        alert('DEBE INDICAR EL MOTIVO DEL CAMBIO DE ESTATUS');
    } else {
        if ($('#adscrito').is(':checked')) {
            check_adscrito = 'true';

        } else {
            check_adscrito = 'false';

        }
    }
    if (check_adscrito == 'true') {
        var id_adscrito = $('#entes_adscritos').val();
        if (id_adscrito == null) {
            alert('DEDE SELECCIONAR EL ENTE ADSCRITO');
        } else {


            var objeto_anterior = 
            {
                "Cedula": $('#cedula_anterior').val(),
                "Nombre": $('#nombre_cortesia_anterior').val(),
                "Apellido": $('#apellido_cortesia_anterior').val(),
                "Telefono": $('#telefono_anterior').val(),
                "Nacionalidad": $('#nacionalidad_anterior').val(),
                "Fecha": $('#fecha_anterior').val(),
                "Ocupacion": $('#ocupacion_anterior option:selected').text(),
                "TipoSangre": $('#tipodesangre_anterior option:selected').text(),
                "EstadoCivil": $('#estadocivil_anterior option:selected').text(),
                "GradoIntruccion": $('#gradointruccion_anterior option:selected').text(),
                "Sexo": $('#sexo_anterior option:selected').text(),
                "Inactivo": $('#borrado_anterior').val(),   
                "EnteAdscrito": $('#entes_adscritos_anterior option:selected').text(),
                              
            }
             var objeto_actual= 
             {
                "Cedula": $('#cedula').val(),
                "Nombre": $('#nombre_cortesia').val(),
                "Apellido": $('#apellido_cortesia').val(),
                "Telefono": $('#telefono').val(),
                "Nacionalidad": $('#nacionalidad').val(),
                "Fecha": $('#fecha').val(),
                "Ocupacion": $('#ocupacion option:selected').text(),
                "TipoSangre": $('#tipodesangre option:selected').text(),
                "EstadoCivil": $('#estadocivil option:selected').text(),
                "GradoIntruccion": $('#gradointruccion option:selected').text(),
                "Sexo": $('#sexo option:selected').text(),
                "Inactivo": $('#estatus_actual').val(),
                "EnteAdscrito": $('#entes_adscritos option:selected').text()
                    
             }
             var camposModificados = [];
                for (var propiedad in objeto_anterior) {
                    if (objeto_anterior.hasOwnProperty(propiedad)) {
                        if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                            camposModificados.push({
                                propiedad: propiedad,
                                valorAnterior: objeto_anterior[propiedad],
                                valorNuevo: objeto_actual[propiedad]
                            });
                        }
                    }
                }
                if (camposModificados.length > 0) {
                    var datos_modificados = camposModificados.map(function(campo) {
                        let string_anterior = campo.valorAnterior;
                        let patron = /option-/;
                        if (patron.test(string_anterior)) {
                            campo.valorAnterior = string_anterior.replace(patron, "");
                        }
                        let string_Actual = campo.valorNuevo;
                        let patron_Actual = /option-/;
                        if (patron.test(string_Actual)) {
                            campo.valorNuevo = string_Actual.replace(patron, "");
                        }
                        campo.valorAnterior = campo.valorAnterior.trim();
        
                        return campo.valorAnterior === '' ?
                            `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                            `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
                    }).join(", ");      
                }
            if (datos_modificados == undefined) {
                alert('NO SE HA REALIZADO NINGUNA MODIFICACION');
    
            } else 
            {
                var id_adscrito = $('#entes_adscritos').val();
                var sexo = $('#sexo').val();
                var fecha_nac_cortesia = $('#fecha').val();
                var url = '/actualizar_Cortesia' + '/' + cedula_anterior;
                var ruta_regreso = '/listar_Cortesia/' + cedula_trabajador;
                var data = 
                {
                    datos_modificados:datos_modificados,
                    cedula_trabajador: cedula_trabajador,
                    cedula: cedula,
                    nombre: nombre,
                    apellido: apellido,
                    observacion: observacion,
                    nacionalidad: nacionalidad,
                    telefono: telefono,
                    sexo: sexo,
                    fecha_nac_cortesia: fecha_nac_cortesia,
                    tipo_de_sangre_id: tipo_de_sangre_id,
                    estado_civil_id: estado_civil_id,
                    grado_intruccion_id: grado_intruccion_id,
                    ocupacion_id: ocupacion_id,
                    estatus_modificado: estatus_modificado,
                    bandera_Modificacion,
                    bandera_Modificacion,
                    observacion,
                    observacion,
                    id_usuario,
                    id_usuario,
                    observacion,
                    observacion,
                    check_adscrito: check_adscrito,
                    id_adscrito: id_adscrito,
                    id_cortesia: id_cortesia,
                    nombre_anterior:nombre_anterior,
                    apellido_anterior:apellido_anterior,
                }
            $.ajax({
                url: url,
                method: 'POST',
                data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                dataType: 'JSON',
                beforeSend: function(data) {},
                success: function(data) {

                    if (data == '1') {
                        alert('Registro Actualizado ');
                        window.location = ruta_regreso;
                    } else {
                        alert('Error en la Incorporación del Registro');
                    }
                },
                error: function(xhr, status, errorThrown) {
                    alert(xhr.status);
                    alert(errorThrown);
                }
            })
        }

        }
    } else {
        var id_adscrito = $('#entes_adscritos').val();
        var sexo = $('#sexo').val();
        var fecha_nac_cortesia = $('#fecha').val();
        var url = '/actualizar_Cortesia' + '/' + cedula_anterior;
        var ruta_regreso = '/listar_Cortesia/' + cedula_trabajador;
        var objeto_anterior = 
        {
            "Cedula": $('#cedula_anterior').val(),
            "Nombre": $('#nombre_cortesia_anterior').val(),
            "Apellido": $('#apellido_cortesia_anterior').val(),
            "Telefono": $('#telefono_anterior').val(),
            "Nacionalidad": $('#nacionalidad_anterior').val(),
            "Fecha": $('#fecha_anterior').val(),
            "Ocupacion": $('#ocupacion_anterior option:selected').text(),
            "TipoSangre": $('#tipodesangre_anterior option:selected').text(),
            "EstadoCivil": $('#estadocivil_anterior option:selected').text(),
            "GradoIntruccion": $('#gradointruccion_anterior option:selected').text(),
            "Sexo": $('#sexo_anterior option:selected').text(),
            "Inactivo": $('#borrado_anterior').val(),   
            "EnteAdscrito": $('#entes_adscritos_anterior option:selected').text()                
        }
         var objeto_actual= 
         {
            "Cedula": $('#cedula').val(),
            "Nombre": $('#nombre_cortesia').val(),
            "Apellido": $('#apellido_cortesia').val(),
            "Telefono": $('#telefono').val(),
            "Nacionalidad": $('#nacionalidad').val(),
            "Fecha": $('#fecha').val(),
            "Ocupacion": $('#ocupacion option:selected').text(),
            "TipoSangre": $('#tipodesangre option:selected').text(),
            "EstadoCivil": $('#estadocivil option:selected').text(),
            "GradoIntruccion": $('#gradointruccion option:selected').text(),
            "Sexo": $('#sexo option:selected').text(),
            "Inactivo": $('#estatus_actual').val(),
            "EnteAdscrito": $('#entes_adscritos option:selected').text()
             
         }
         var camposModificados = [];
            for (var propiedad in objeto_anterior) {
                if (objeto_anterior.hasOwnProperty(propiedad)) {
                    if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                        camposModificados.push({
                            propiedad: propiedad,
                            valorAnterior: objeto_anterior[propiedad],
                            valorNuevo: objeto_actual[propiedad]
                        });
                    }
                }
            }
            if (camposModificados.length > 0) {
                var datos_modificados = camposModificados.map(function(campo) {
                    let string_anterior = campo.valorAnterior;
                    let patron = /option-/;
                    if (patron.test(string_anterior)) {
                        campo.valorAnterior = string_anterior.replace(patron, "");
                    }
                    let string_Actual = campo.valorNuevo;
                    let patron_Actual = /option-/;
                    if (patron.test(string_Actual)) {
                        campo.valorNuevo = string_Actual.replace(patron, "");
                    }
                    campo.valorAnterior = campo.valorAnterior.trim();
    
                    return campo.valorAnterior === '' ?
                        `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                        `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
                }).join(", ");      
            }

            if (datos_modificados == undefined) {
                alert('NO SE HA REALIZADO NINGUNA MODIFICACION');
    
            } else 
            {
                var data = 
                {
                    datos_modificados:datos_modificados,
                    cedula_trabajador: cedula_trabajador,
                    cedula: cedula,
                    nombre: nombre,
                    apellido: apellido,
                    observacion: observacion,
                    nacionalidad: nacionalidad,
                    telefono: telefono,
                    sexo: sexo,
                    fecha_nac_cortesia: fecha_nac_cortesia,
                    tipo_de_sangre_id: tipo_de_sangre_id,
                    estado_civil_id: estado_civil_id,
                    grado_intruccion_id: grado_intruccion_id,
                    ocupacion_id: ocupacion_id,
                    estatus_modificado: estatus_modificado,
                    bandera_Modificacion,
                    bandera_Modificacion,
                    observacion,
                    observacion,
                    id_usuario,
                    id_usuario,
                    observacion,
                    observacion,
                    check_adscrito: check_adscrito,
                    id_adscrito: id_adscrito,
                    id_cortesia: id_cortesia,
                    nombre_anterior:nombre_anterior,
                    apellido_anterior:apellido_anterior,
                }
        $.ajax({
            url: url,
            method: 'POST',
            data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
            dataType: 'JSON',
            beforeSend: function(data) {},
            success: function(data) {

                if (data == '1') {
                    alert('Registro Actualizado ');
                    window.location = ruta_regreso;
                } else {
                    alert('Error en la Incorporación del Registro');
                }
            },
            error: function(xhr, status, errorThrown) {
                alert(xhr.status);
                alert(errorThrown);
            }
        })
            }
    }



});
$('#lista_cortesia').on('click', '.Editar', function(e) {

    var tipodesangre_id = $(this).attr('tipo_de_sangre');
    var estadocivl_id = $(this).attr('estadocivil_id');
    var cedula = $(this).attr('cedula');
    var grado_intruccion_id = $(this).attr('grado_intruccion_id');
    var ocupacion_id = $(this).attr('ocupacion_id');
    window.location = '/editar_Cortesia/' + cedula + '/' + tipodesangre_id + '/' + estadocivl_id + '/' + grado_intruccion_id + '/' + ocupacion_id;






});

$('#lista_cortesia').on('click', '.Detalle', function(e) {
    var cedula = $(this).attr('cedula');
    window.location = '/info_medicamentos_cortesia/' + cedula;

});




$(document).on('click', '#btnCerrarCortesia ', function(e) {
    e.preventDefault();
    var cedula_trabajador = $('#cedula_trabajador').val();

    var ruta_regreso = '/listar_Cortesia/' + cedula_trabajador;
    window.location = ruta_regreso;

});




$(document).on('click', '#btnRegresar_hacia_titulares ', function(e) {
    e.preventDefault();
    window.location = '/titulares/';

});

$('#lista_cortesia').on('click', '.Historial', function(e) {

    var cedula_trabajador = $('#cedula_trabajador').val();
    var cedula_beneficiario = $(this).attr('cedula');
    window.location = '/datos_titular_cortesia/' + cedula_trabajador + '/' + cedula_beneficiario;
});