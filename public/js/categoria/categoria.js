/*
 *Este es el document ready
 */
 $(function()
 {
     listarcategoria();
   
     //Lleno la persiana de la unidad de medida
 });
 /*
  * Función para definir datatable:
  */
 function listarcategoria()
 {
      $('#table_categoria').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_categoria",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [{
                data:'id'},
                {data:'descripcion'},
                {data:'fecha_creacion'},                          

                {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.borrado=='Bloqueado')
                   
                      {
                         return '<button id="btnbloqueado"class=" btn-danger btnbloqueado "disabled="disabled">'+row.borrado+'</button>'
                      }
                      else
                      {
                         return '<button id="btnactivo"class=" btn-success btnactivo"disabled="disabled">'+row.borrado+'</button>'
                       
                      } 
                   
                    }
               },




                {orderable: true,
                 render:function(data, type, row)
                 {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary  Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar" id='+row.id+' descripcion="'+row.descripcion+'" unidad_medida='+row.unidad_medida+' cantidad='+row.cantidad+' id_unidad_medida='+row.id_unidad_medida+' borrado='+row.borrado+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
     $('#categoria').val('');
    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

      $("#modal").modal("show");
      llenar_combo_unidad_medida(e);
      llenar_combo_tipo_medicamento(e);

 });
 
 $('#lista_de_categoria').on('click','.Editar', function(e)
 {
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#borrado').show();
     $('#modal').find('#borrado_anterior').show();
     var id            =$(this).attr('id');  
     var categoria   =$(this).attr('descripcion'); 
     var estatus       =$(this).attr('borrado');
     $('#categoria').val(categoria);
     $('#categoria_anterior').val(categoria);
     $('#id_categoria').val(id);
     $("#modal").modal("show");
     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
          $('#borrado_anterior').val('false');
          
     }
     if(estatus=='Eliminado')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
          $('#borrado_anterior').val('true');
     }


 });


$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();
      var id           =$('#id').val();
      var categoria   =$('#categoria').val();
      var url='/agregar_categoria';
      var data=
      {
          id :id ,
         categoria  :categoria,        
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/vistacategoria/'; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });

});

$(document).on('click','#btnActualizar', function(e)
{
     var id_categoria=$('#id_categoria').val();        
     var categoria    =$('#categoria').val();
    let descripcion_anterior=$('#categoria_anterior ').val();
   
     var borrado='false';

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }
     var objeto_anterior = {
          "Categoria": $('#categoria_anterior ').val(),
          "Bloqueado": $('#borrado_anterior ').val(),
              
      }
      var objeto_actual= {
          "Bloqueado": borrado,
          "Categoria": $('#categoria ').val(),
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }
     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else 
      {

          var data=
          {
               id_categoria:id_categoria,
               categoria :categoria ,
               borrado        :borrado,
               datos_modificados:datos_modificados,
               descripcion_anterior:descripcion_anterior
          }
     
          var url='/actualizar_categoria';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  //alert(data);
                  if(data===1)
                  {
                     alert('Registro Actualizado');
                  }
                  else
                  {
                    alert('Error en la Incorporación del registro');
                  }
                  window.location = '/vistacategoria';             
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
          });
      }
 });
 $(document).on('click','#btnRegresar', function(e)
{
     window.location = '/vistamedicamentos/';
})
