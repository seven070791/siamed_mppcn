/*
 *Este es el document ready
 */
 $(function()
 {

     let cedula_usuario   =$('#cedula_usuario').val();
     buscar_medico(Event,cedula_usuario);
     llenar_combo_especialidad(Event)
     llenar_combo_MedicoTratante(Event)
     document.getElementById("medico").disabled=true;

 });

 function buscar_medico(e, cedula_usuario) {
     let desde          =$('#desde').val();
     let hasta          =$('#hasta').val();
     let rol          =$('#rol').val();
     let idespecialidad          =$('#idespecialidad').val();
     if (desde==''&&hasta=='')
     {
     
         desde='null'
         hasta='null'     
     } 
   
     let id_medico   =$('#cmbmedicos').val();
    
     $('#medico').val('0')

     $('#especialidad').val('0')
     let especialidad=0
     
     let asistio='Seleccione'
     let control='Seleccione'   
     nombre_medico='Seleccione'
     nombre_especialidad='Seleccione' 
     nombre_asistio='Seleccione' 
     medico=0;
     personal=0;
     $.ajax({
         url: '/buscar_medico/' + cedula_usuario,
         method: 'get',
         dataType: 'JSON',
         beforeSend: function() {
             // Puedes agregar un indicador de carga aquí si lo deseas.
         },
         success: function(response) {
             // Verifica si la respuesta tiene elementos
             if (response.length > 0) {
                 $('#id_medico').val(response[0].id);
                 $('#idespecialidad').val(response[0].id_especialidad);
                 
                 
                 
                 listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio,personal, nombre_medico='Seleccione',nombre_especialidad='Seleccione',nombre_asistio='Seleccione');
             } else {
                 // Si no hay valores, establece como cadenas vacías
                 $('#id_medico').val('');
                 $('#idespecialidad').val('');
                 listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio,personal, nombre_medico='Seleccione',nombre_especialidad='Seleccione',nombre_asistio='Seleccione');
             }
         },
         error: function(xhr, status, errorThrown) {
             console.error('Error en la solicitud:', xhr.status, errorThrown);
             alert('Error: ' + xhr.status + ' - ' + errorThrown);
         }
     });
 }







 
function listar_citas_consultas(desde=null,hasta=null,medico=0,especialidad=null,control=0,asistio=0,personal=0,nombre_medico='null',nombre_especialidad='Seleccione',nombre_asistio='Seleccione')
{
    
   
     let rol          =$('#rol').val();
     let idespecialidad =$('#idespecialidad').val();
     let now = new Date();
     let day = ("0" + now.getDate()).slice(-2);
     let month = ("0" + (now.getMonth() + 1)).slice(-2);
     let today= day+"-"+month+"-"+now.getFullYear(); 
     let fecha_desde = desde;
     let partesFecha = fecha_desde.split("-");
     let dataFormatada_desde = partesFecha[2] + "-" + partesFecha[1] + "-" + partesFecha[0];
     let fecha_hasta = hasta;
     let partesFecha_ = fecha_hasta.split("-");
     let dataFormatada_hasta = partesFecha_[2] + "-" + partesFecha_[1] + "-" + partesFecha_[0];
     let encabezado=''; 
     if(control!='0'&& control!='Seleccione')
     {
          encabezado=encabezado+'  Control:'+' '+' '+control+' '+' ';
     }

     if(dataFormatada_desde!='undefined-undefined-null' && dataFormatada_hasta!='undefined-undefined-null') 
     {
          encabezado= encabezado + 'Desde:'+' '+dataFormatada_desde+' '+' '+'hasta'+' '+' '+dataFormatada_hasta+' '+' ';
     }
     if(nombre_especialidad!='Seleccione') 
     {
          encabezado=encabezado+'Especialidad:'+' '+' '+nombre_especialidad+' '+' ';
     }

     if(nombre_medico!='Seleccione'&& nombre_medico!='Medicos'&& nombre_medico!='null')
     {
          encabezado=encabezado+'Medico:'+' '+' '+nombre_medico+' '+' ';
     }
      
     if(nombre_asistio!='Seleccione')
     {
          encabezado=encabezado+'Asitencia:'+' '+' '+nombre_asistio+' '+' ';
     }

     let ruta_imagen =rootpath
    let table = $('#table_consulta_citas').DataTable( {

          dom: "Bfrtip",
          buttons:{
              dom: {
                  button: {
                      className: 'btn-md'
                  },
                  
              },
              
              buttons: [
              {
                  //definimos estilos del boton de pdf
                  extend: "pdf",
                  text:'PDF',
                  className:'btn-xs btn-dark',
                  orientation: 'landscape',
                  pageSize: 'LETTER',
                 // title:'RELACION ENTRADAS MEDICAMENTOS',
                  header:true,
                  footer: true,
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4,5,6],
                              
                  },   
                  alignment: 'center',
                  
                  customize:function(doc)
                  {                       
                  //Remove the title created by datatTables
                       doc.content.splice(0,1);
                       doc.styles.title= 
                       { 
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'                     
                       }
                       doc.styles['td:nth-child(2)'] = 
                       {
                            width: '130px',
                            'max-width': '130px'
                       },
                       doc.styles.tableHeader = 
                       {
                            fillColor:'#4c8aa0',
                            color:'white',
                            alignment:'center'
                       }, 
             // Create a header
             doc.pageMargins = [10,95,0,70];
             doc['header'] = (function (page, pages)
             {                      
                  doc.styles.title = 
                  {    
                       color: '#4c8aa0',
                       fontSize: '18',
                       alignment: 'center',  
                  } 
                  return {  
                            columns:
                            [
                                 {                              
                                   margin: [ 170, 3, 40, 40 ],                                  
                                      image:ruta_imagen,
                                      width: 500,  
                                                               
                                 },                        
                                 {
                                      margin :[ -400, 50, -25, 0 ],
                                      color: '#4c8aa0',
                                      fontSize: '18',
                                      alignment: 'center' , 
                                      text: 'ATENCION DE BENEFICIARIOS',
                                      fontSize: 18,                             
                                 },
                                 {
                                 margin :[ -530, 80, -25, 0 ],     
                                 text:encabezado,
                                 },
                            ], 
                       }                               
              }); 
             // Create a footer
             doc['footer'] = (function (page, pages)
             {    
                  return {
                            columns:
                            [
                                 {
                                      alignment: 'center',
                                      text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                 }
                            ],               
                            }                            
                  });
    
                  },
             },
 
              {    
                   //definimos estilos del boton de excel
                   extend: "excel",
                   text:'Excel',
                   className:'btn-xs btn-dark',
                   title:'ATENCION DE BENEFICIARIOS', 
                  
                   download: 'open',
                   exportOptions: {
                     columns: [1,2,3,4,5,6],
                   },
                   excelStyles: {                                                
                     "template": [
                         "blue_medium",
                         "header_blue",
                         "title_medium"
                     ]                                  
                 },
 
              }
              ]            
          }, 
         
 
          //"order":[[5,"desc"]],					
          "paging": true,
          "lengthChange": true,
         
          dom: 'Blfrtip',
          "searching": true,
          "lengthMenu": [
               [ 10, 25, 50, -1 ],
               [ '10', '25', '50', 'Todos' ]
          ], 
         
          "ordering": true,
          "info": true,
          "autoWidth": true,
          //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
          "ajax":
          {
            
             "url":base_url+"/listar_reporte_citas/"+desde+'/'+hasta+'/'+medico+'/'+especialidad+'/'+control+'/'+asistio+'/'+personal+'/'+today,
           "type":"GET",
           dataSrc:''
          },
          "columns":
          [


               {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.signos_activos=='t' || row.diagnostico_activo=='t')
                   
                      {
                         return '<input type="checkbox" checked onclick = "this.checked=!this.checked" >';
                         
                      }
                      else
                      {
                         return '<input type="checkbox" disabled>';
                       
                      } 
                   
                    }
               },
              
           // {data:'id'}, 
           {data:'consulta'}, 
           {data:'n_historial'},  
           {data:'nombre'},   
           {data:'nombremedico'},
           {data:'especialidad'},
           {data:'format_fecha_asistencia'},
           {data:'controlasistencia'},
          
       

  {
     orderable: true,
            render:function(data, type, row)
            {
              
          if (rol==1) 
          {
               if(row.consulta=='Consulta')
                    {
                       return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  disabled"  style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
                    }
                  else 
                    {
      
                    return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  Confirmar" style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+' asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
                    }              
          }
          
         else  if (rol==7) 
               {
                    if(row.consulta=='Consulta')
                         {
                            return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  disabled"  style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
                         }
                       else 
                         {
           
                         return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  Confirmar" style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+' asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
                         }              
               }
          else
          {
               if (idespecialidad==12) 
               {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id_consulta='+row.id_consulta+' nombre='+row.nombre+'id_consulta='+row.id_consulta+' tipo_beneficiario='+row.tipo_beneficiario+' n_historial='+row.n_historial+'> <i class="material-icons " >create</i></a>'
                    
               }
                     
          }
             
            }
       },









        
     //   {orderable: true,
     //        render:function(data, type, row)
     //        {
              
     //      if (rol!=5) 
     //      {
     //           if(row.consulta=='Consulta')
     //                {
     //                   return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  disabled"  style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
     //                }
     //              else 
     //                {
      
     //                return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  Confirmar" style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+' asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
     //                } 
                   
               
     //      }else
     //      {
     //          return '<a href="javascript:;" class="btn btn-xs btn-secondary editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id_consulta='+row.id_consulta+' nombre='+row.nombre+'id_consulta='+row.id_consulta+' tipo_beneficiario='+row.tipo_beneficiario+' n_historial='+row.n_historial+'> <i class="material-icons " >create</i></a>'
                  
     //      }
             
     //        }
     //   },

   
          ],
          
    "language":
        {
            "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
               {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
               },
               
           ]
        
     },   
          
      });

}

 $('#labelenfermedad_actual').on('click',function(e)
 {   
   $(".efa").css("display", "block")
 });

 $('.labelealergias_medicamentos').on('click',function(e)
 {   
   $(".efa").css("display", "none")
 });



 $(document).on('click','#btnRegresar', function(e)
 {
     e.preventDefault();
     window.location = '/titulares/';
     
 });


 $(document).on('click','#btnGuardar', function(e)
 { 
     e.preventDefault();
    let tipo_beneficiario=$('#tipo_beneficiario').val();
    let id_especialidad=$('#especialidad').val();
    let id_medico=$('#medico').val();
    let cedula=$('#cedulaT').val();
     if($('#especialidad').val()==0) 
     {
          alert('Debe Seleccionar una  especialidad');   
     }
     else if($('#medico').val()>=1) 
     {
         let fecha_asistencia=$('#fecha_del_dia').val();
          let fechaconvertida=moment(fecha_asistencia,'DD-MM-YYYY'); 
          fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
         let n_historial=$('#numeroHistorial').val();
         let peso=$('#peso').val();
         let talla=$('#talla').val();
         let spo2=$('#spo2').val();
         let frecuencia_c=$('#frecuencia_c').val();
         let frecuencia_r=$('#frecuencia_r').val();
         let temperatura=$('#temperatura').val();
         let ta_alta=$('#ta_alta').val();
         let ta_baja=$('#ta_baja').val();
         let imc=$('#imc').val();
         let url='/agregar_cita_historial';
         let ruta_regreso='/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
          if (peso=='') 
          {
               peso=0; 
          }
          if (talla=='') 
          {
               talla=0; 
               }
          if (spo2=='') 
          {
               spo2=0; 
          }if (frecuencia_c=='') 
          {
               frecuencia_c=0; 
          } if (frecuencia_r=='')
          {
               frecuencia_r=0; 
          }if (temperatura=='') 
          {
               temperatura=0; 
          } if (ta_alta=='') 
          {
               ta_alta=0; 
          } if (ta_baja=='')
          {
               ta_baja=0; 
          }

         let data=
           {
              fecha_asistencia:fechaconvertida,
              n_historial   :n_historial,
              id_medico     :id_medico,
              peso          :peso,               
              talla         :talla,
              spo2          :spo2,
              frecuencia_c  :frecuencia_c,
              frecuencia_r  :frecuencia_r,
              temperatura   :temperatura,
              ta_alta       :ta_alta,
              ta_baja       :ta_baja,
              imc:imc,
              tipo_beneficiario:tipo_beneficiario,
                
           }
      //console.log(data);
          $.ajax
          ({
                url:url,
                method:'POST',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                },
                success:function(data)
                {
                     
                     if(data=='1')
                     {
                          alert('Registro Incorporado ');
                      
                          
                          window.location = ruta_regreso;
                     }
                     else if(data=='0')
                     {
                          alert('Error en la Incorporación del Registro');
                     }
      
                     else if(data=='2')
                     {
                          alert('La cedula posee Historial');
                     }
      
      
                },
                error:function(xhr, status, errorThrown)
                {
                     alert(xhr.status);
                     alert(errorThrown);
                }
          });

     }else 
     {
          alert('Debe Seleccionar un Medico'); 
     }
    

 
});   

$('#consulta_citas').on('click','.Confirmar', function(e) 

{
    //let cedula=$(this).attr('cedula'); 
     let consulta_id =$(this).attr('id_consulta'); 
     let asistencia =$(this).attr('asistencia'); 
     let now = new Date();
     let day = ("0" + now.getDate()).slice(-2);
     let month = ("0" + (now.getMonth() + 1)).slice(-2);
     let today= day+"-"+month+"-"+now.getFullYear(); 
     let fecha_asistencia=$(this).attr('fecha_asistencia'); 
          controlasistencia =$(this).attr('controlasistencia'); 
     //SE FORMATEO EL OREDEN DE LA FECHA PARA PODER HACER LA COMPARACION DE DD/MM/YYYY A YYYY/MM/DD
     format_fecha_asistencia=(convertDateFormat(fecha_asistencia));
     format_today=(convertDateFormat(today));
     function convertDateFormat(fecha_asistencia) {
     let info = fecha_asistencia.split('-');
     return info[2] + '/' + info[1] + '/' + info[0];
}
//alert(asistencia);
   
if(format_fecha_asistencia<format_today&&asistencia=='f')
     {
          alert('EL BENEFICIARIO HA PERDIDO SU CITA');
         // window.location = '/reporte_citas/'
     }
   
    
  else if (format_fecha_asistencia!=format_today&& asistencia=='f') {
      alert('NO SE PUEDE CONFIRMAR UNA CITA ANTES DE VENCERSE ');
      //window.location = '/reporte_citas/' 
           
      }
   
   else if(format_fecha_asistencia<format_today&& asistencia=='t')
     {
          alert('NO SE PUEDE MODIFICAR EL ESTATUS , COMUNIQUESE CON EL ADMINISTRADOR DEL SISTEMA');
         // window.location = '/reporte_citas/'
     }
 
     else  {
            //alert('si');  
            asistencia='true'; 
          

               Swal.fire({
                    title: 'ASISTENCIA DE CITA !!',
                    icon:'warning',
                    showCancelButton: true,
                    cancelButtonColor:'#d33',
                    confirmButtonText:'Confirmar',
                         
                    }).then((result)=> {
                         if(result.isConfirmed){ 
               
                              Swal.fire(  
                                                       
                                   'LA CITA HA SIDO CONFIRMADA!',
                                   '...',
                                   'success',
                                   {
                                        
                                   }
                              )
                              
                              let data=
                              {
                                   consulta_id:consulta_id,
                                   asistencia     :asistencia,
                              }   
                              console.log(data);
                              let url='/actualizar_asistencia';
                              $.ajax
                              ({
                                   url:url,
                                   method:'POST',
                                   data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                                   dataType:'JSON',
                                   beforeSend:function(data)
                                   {
                                        //alert('Procesando Información ...');
                                   },
                                   success:function(data)
                                   {
                                        //alert(data);
                                        if(data===1)
                                        {
                                        
                                        
               
                                        }
                                        else
                                        {
                                        alert('Error en la Incorporación del registro');
                                        }
                                        setTimeout(function () {
                                             window.location = '/reporte_citas'
                                        }, 1500);
                                        //window.location = '/datos_titular/'+cedula;             
                                   },
                                   error:function(xhr, status, errorThrown)
                                   {
                                        alert(xhr.status);
                                        alert(errorThrown);
                                   }
                              });  
                              
                              }
                         
                         }) 
                    }  
     
        
 });




 $(document).on('click','#btnfiltrar ', function(e)
  {
      
     let desde                =$('#desde').val();
     let hasta                =$('#hasta').val();
     let medico               =$('#medico').val();
     let especialidad         =$('#especialidad').val();
     let personal         =$('#personal').val();
     let nombre_medico       =$('#medico option:selected' ).text();
     let nombre_especialidad       =$('#especialidad  option:selected' ).text();
     //let nombre_medico       =$('#medico option:selected' ).text();
     let asistio     =$('#asistio').val();
     let control  =$('#control option:selected' ).text();
     let nombre_asistio  =$('#asistio option:selected' ).text();

   


     


          if(asistio==1){
               asistio='t'
          }
          
          else if(asistio==2){
               asistio='f'
          }
          else if(asistio==3){
               asistio='En espera'
          }
          else{
               asistio='Seleccione'
          }
         // alert(asistio);
          

     if (medico==null) 
     {
          medico=0;
     }


      if (desde=='') 
      {
          desde='null' 
      }
       if (hasta=='') 
      {
          hasta='null' 
      }
  
       
   $("#table_consulta_citas").dataTable().fnDestroy();
      
  
      
      if (desde=='null'&&hasta!='null') 
      {
          alert('DEDE INDICAR EL CAMPO DESDE');
          
      }
      else if (hasta=='null'&&desde!='null') 
      {
          alert('DEDE INDICAR EL CAMPO HASTA');
      } 
      else if (hasta<desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
      }else{
     listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio,personal,nombre_medico,nombre_especialidad,nombre_asistio);
      }
      
  });

     

 

  $(document).on('click','#btnlimpiar', function(e)
  {
    e.preventDefault();
    $('#desde').val('');
    $('#hasta').val('');
     $('#medico').val('0');
    $('#especialidad').val('0');
    $('#asistio').val('0');
    $('#control').val('0');
        
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val();
    if (desde==''&&hasta=='')
    {
    
        desde='null'
        hasta='null'     
    } 
  
    let id_medico   =$('#cmbmedicos').val();
    $('#medico').val('0')

    $('#especialidad').val('0')
    let especialidad   =0
    let asistio='Seleccione'  

    let control='Seleccione'   
   
    
    medico=0;
   
      $("#table_consulta_citas").dataTable().fnDestroy();
      listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio,personal);
        
  });



  function  llenar_combo_especialidad(e,id_especialidad)
  {
 
      e.preventDefault;
      
        url='/listar_especialidades_activas_sin_filtro';
         $.ajax
        ({
             url:url,
             method:'GET',
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {   
  if(data.length>=1)
  {
       $('#especialidad').empty();
       $('#especialidad').append('<option value=0>Seleccione</option>');     
       if(id_especialidad===undefined)
      {
           
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
           $.each(data, function(i, item)
           {
               
 
                if(item.id_especialidad===id_especialidad)
               
                {
                     $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
                }
                else
                {
                     $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
                }
           });
      }
  }      
  },
  error:function(xhr, status, errorThrown)
  {
      alert(xhr.status);
      alert(errorThrown);
  }
  });
  }

function  llenar_combo_MedicoTratante(e,id)
{

    e.preventDefault;
    
      url='/listar_medicos_activos';
       $.ajax
      ({
           url:url,
           method:'GET',
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {

if(data.length>=1)
{
     $('#cmbmedicotratante').empty();
     $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
     if(id===undefined)
    {
         
         $.each(data, function(i, item)
         {
              //console.log(data)
              $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

         });
    }
    else
    {
         
         $.each(data, function(i, item)
         {
             

              if(item.id===id)
             
              {
                   $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
              }
              else
              {
                   $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
              }
         });
    }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}
  function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#medico').empty();
      $('#medico').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#medico').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#medico').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("medico").disabled=false;
      id_especialidad=$('#especialidad option:selected').val(); 
      ///si es Tratamientos 
      if (id_especialidad == '12') {
          $('.t_personal').hide();
          $('.t_medico').show();
          llenar_combo_MedicoReferido(e, id_especialidad);
          
          // Cambiar el texto del label para Enfermera
          $('.labelmedicotratante.t_medico').text('Enfermera');
      } else {
          $('.t_personal').hide();
          $('.t_medico').show();
          llenar_combo_MedicoReferido(e, id_especialidad);
          
          // Cambiar el texto del label para Médico
          $('.labelmedicotratante.t_medico').text('Médico');
      }

   
 }); 




 function llenar_combo_personal(e) {
     e.preventDefault(); // Corrección aquí
 
     const url = '/listar_personal_tratamiento';
     $.ajax({
         url: url,
         method: 'GET',
         dataType: 'JSON',
         beforeSend: function() {
             // Puedes agregar un loader o algo similar aquí si lo deseas
         },
         success: function(data) {
            
 
             if (Array.isArray(data) && data.length >= 1) { // Verificación de que data es un array
                 $('#personal').empty();
                 $('#personal').append('<option value="0" selected disabled>Seleccione</option>');   
 
                 // Agregar las opciones al combo
                 data.forEach(item => {
                     $('#personal').append(`<option value="${item.id}">${item.nombre} ${item.apellido}</option>`);
                 });
             } else {
                 console.log("No hay datos disponibles o no es un array."); // Mensaje de depuración
             }
         },
         error: function(xhr, status, errorThrown) {
             alert(`Error: ${xhr.status} - ${errorThrown}`);
         }
     });
 }
 
















 $('#consulta_citas').on('click','.Bloquear', function(e)    

 {
   
     let control   =$(this).attr('control');
      Swal.fire({
           title: 'ESTÁ SEGURO QUE DESEA ELIMINAR LA CITA O CONSULTA?',
           icon:'warning',
           showCancelButton: true,
           cancelButtonColor:'#d33',
           confirmButtonText:'Confirmar',
                    
           }).then((result)=> {
                  if(result.isConfirmed){ 
      
                     Swal.fire(  
                                                  
                          'ELIMINAR!!',
                          'Registro Actualizado.',
                          'success',
                          {
                               
                          }
                        )
                        let id   =$(this).attr('id_consulta');
                        let nombre   =$(this).attr('nombre');
                        let cedula   =$(this).attr('cedula');
                        let control   =$(this).attr('control');
                        id = (isNaN(parseInt(id)))? 0 : parseInt(id);
                        let borrado    =true;
                      
 
                        var data=
                        {
                             id:id,
                             borrado:borrado,
                             nombre:nombre,
                             cedula:cedula,
                             control:control
                        }     
                       var url='/borrar_consulta';
                       $.ajax
                        ({
                             url:url,
                             method:'POST',
                             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                             dataType:'JSON',
                             beforeSend:function(data)
                             {
                                  //alert('Procesando Información ...');
                             },
                             success:function(data)
                             {
                             
                             
                                 if(data==0)
                                {
                                    alert('Error al Reversar el Registro');  
                                  
                                }
 
                                else  if(data==2)
                                {
                                    alert('Hubo un error durante el reverso ');  
                                }
                                setTimeout(function () {
                                    window.location = '/reporte_citas/';  
                                 }, 1500);
                                          
                             },
                             error:function(xhr, status, errorThrown)
                             {
                                alert(xhr.status);
                                alert(errorThrown);
                             }
                       });  
                       
                    }
                    
                  })  
      
         
  });
 
 


  $('#consulta_citas').on('click', '.editar', function(e) {
     e.preventDefault(); 
     let id_consulta = $(this).attr('id_consulta'); 
     let tipo_beneficiario = $(this).attr('tipo_beneficiario'); 
     let n_historial = $(this).attr('n_historial'); 
     let cedula = n_historial.match(/\d+/); 
     if (cedula) {
     cedula = cedula[0]; 
     } else {
     console.log("No se encontró un número de cédula.");
     }
     let nombre = $(this).attr('nombre'); 
 
     if (tipo_beneficiario == 'F' || tipo_beneficiario == 'C') {
         $("#beneficiarios").show();
     }
 
     // Realizar la primera llamada AJAX para buscar signos vitales
     $.ajax({
         type: "get",
         url: "buscar_signos_vitales_citas/" + n_historial + '/' + id_consulta,
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         success: function(response) {
             /* SIGNOS VITALES */
             $("#modal_consultas_citas").modal("show");

             $("#modal_consultas_citas").find("#id_consulta").val(id_consulta);
             $("#modal_consultas_citas").find("#n_historial").val(n_historial);



             $("#modal_consultas_citas").find("#cedula").val(cedula);
             $("#modal_consultas_citas").find("#motivo_consulta").val(response[0].motivo_consulta);
             $("#modal_consultas_citas").find("#peso").val(response[0].peso);
             $("#modal_consultas_citas").find("#talla").val(response[0].talla);
             $("#modal_consultas_citas").find("#spo2").val(response[0].spo2);
             $("#modal_consultas_citas").find("#frecuencia_c").val(response[0].frecuencia_c);
             $("#modal_consultas_citas").find("#frecuencia_r").val(response[0].frecuencia_r);
             $("#modal_consultas_citas").find("#temperatura").val(response[0].temperatura);
             $("#modal_consultas_citas").find("#ta_alta").val(response[0].tension_alta);
             $("#modal_consultas_citas").find("#ta_baja").val(response[0].tension_baja);
             $("#modal_consultas_citas").find("#imc").val(response[0].imc);
              /* DATOS DEL BENEFICIAIRIO */
             $.ajax({
               type: "GET", 
               url: "buscar_informacion_beneficiarios/"+cedula, 
               contentType: "application/json; charset=utf-8",
               dataType: "json", 
               success: function(response) 
               {
                    $("#table_traramiento").dataTable().fnDestroy();
                    listar_tratamientos();
                    $("#modal_consultas_citas").find("#n_historial").val(n_historial );
                    $("#modal_consultas_citas").find("#nombrepellido").val(response[0].nombre + ' ' + response[0].apellido);
                    $("#modal_consultas_citas").find("#departamento").val(response[0].ubicacion_administrativa );
                    $("#modal_consultas_citas").find("#edad").val(response[0].edad );
                    if (response[0].tipo=='F') 
                    {
                         tipo_beneficiario='FAMILIAR'+' '+'('+response[0].parentesco+')'
                         
                    }else  if (response[0].tipo=='C') 
                    {
                        tipo_beneficiario='CORTESÍA'
                         
                    }
                    else
                    {
                         tipo_beneficiario='TITULAR'

                    }
                    $("#modal_consultas_citas").find("#tipo_beneficiario").val(tipo_beneficiario);
                  
               },
               error: function(xhr, status, error) {
                   // Handle error response
                   console.error(xhr.responseText);
                   Swal.fire('Error!', "Error message here", "error");
               }
           });
         },
         error: function(xhr, status, error) {
             Swal.fire('Error!', "Error al actualizar el registro", "error");
             console.error(xhr.responseText);
         }
     });
 });




$(document).on('click','#btnActualizar', function(e)
{
let cedula      =$('#cedulaT').val(); 
let motivo_consulta    =$('#motivo_consulta').val(); 
let id_especialidad=$('#id_especialidad').val();
let id_historial_medico=$('#id_historial_medico').val()
let n_historial=$('#n_historial').val();
let id_consulta=$('#id_consulta').val();
let id_medico=$('#id_medico').val();//en sesion
let peso=$('#peso').val();
let talla=$('#talla').val();
let spo2=$('#spo2').val();
let frecuencia_c=$('#frecuencia_c').val();
let frecuencia_r=$('#frecuencia_r').val();
let temperatura=$('#temperatura').val();
let ta_alta=$('#ta_alta').val();
let ta_baja=$('#ta_baja').val(); 
let fecha_asistencia_normal=$('#fecha_del_dia_mi').val();
let fechaconvertida=moment(fecha_asistencia_normal,'DD-MM-YYYY'); 
fecha_asistencia=fechaconvertida.format('YYYY-MM-DD');
let imc=$('#imc').val();
let user_id=$('#id_user').val();
let tipo_beneficiario='T';
let url='/actualizar_signos_vitales_citas';
if (peso=='') 
{
peso=0; 
}
if (talla=='') 
{
talla=0; 
}
if (spo2=='') 
{
spo2=0; 
}if (frecuencia_c=='') 
{
frecuencia_c=0; 
} if (frecuencia_r=='')
{
frecuencia_r=0; 
}if (temperatura=='') 
{
temperatura=0; 
} if (ta_alta=='') 
{
ta_alta=0; 
} if (ta_baja=='')
{
ta_baja=0; 
}if (imc=='')
{
imc=0; 
}

var data=
{
fecha_asistencia:fecha_asistencia,
n_historial   :n_historial,
id_medico     :id_medico,
peso          :peso,               
talla         :talla,
spo2          :spo2,
frecuencia_c  :frecuencia_c,
frecuencia_r  :frecuencia_r,
temperatura   :temperatura,
ta_alta       :ta_alta,
ta_baja       :ta_baja,
imc:imc,
tipo_beneficiario:tipo_beneficiario,
motivo_consulta:motivo_consulta, 
user_id:user_id,
id_consulta:id_consulta,
}

//      //console.log(data);
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
},
success:function(data)
{

if(data=='1')
{
alert('SIGNOS VITALES ACTUALIZADOS ');

window.location = '/reporte_citas';

}
else if(data=='0')
{
alert('Error en la Incorporación del Registro');
}

else if(data=='2')
{
alert('La cedula posee Historial');
}


},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});
});


$(document).on('click','#btnagregar_tratamientos', function(e)
{
     $("#modal_tratamientos").modal("show");
}
);

$(document).on('click','#btnguargar_tratamientos', function(e)
{
     let n_historial=$('#n_historial').val();
     let id_consulta=$('#id_consulta').val();
     let enfermedad_actual=$('#enfermedad_actual').val().trim();
     if (enfermedad_actual==''||enfermedad_actual==null) 
          
     {
          alert('Debe ingresar el Tratamiento');
          
     }else
     {
          let data=
     {
          id_consulta:id_consulta,
          n_historial:n_historial,
          enfermedad_actual:enfermedad_actual

     }
    
     $.ajax
     ({
          url:'/agregar_enfermedad_actual',
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {

          if(data=='1')
          {
          alert('Registro Exitoso ');

          window.location = '/reporte_citas';

          }
          else if(data=='0')
          {
          alert('Error en la Incorporación del Registro');
          }

     
          },
          error:function(xhr, status, errorThrown)
          {
          alert(xhr.status);
          alert(errorThrown);
          }
     });

     }
     
});

     
  
$('#lista_traramiento').on('click', '.Editar_Tratamiento', function(e) {
     e.preventDefault(); 
     let id_tratamiento = $(this).attr('id'); 
     let enfermedad_actual = $(this).attr('enfermedad_actual'); 
     $("#edit_modal_tratamientos").modal("show");

     $("#edit_modal_tratamientos").find("#id_tratamiento").val(id_tratamiento);
     $("#edit_modal_tratamientos").find("#edit_enfermedad_actual").val(enfermedad_actual);

     
 });

 $(document).on('click','#edit_btnguargar_tratamientos', function(e)
{

     let id_tratamiento=$('#id_tratamiento').val();
     let enfermedad_actual=$('#edit_enfermedad_actual').val();
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= now.getFullYear()+"/"+month+"/"+day;
     let data=
     {
          id:id_tratamiento,
          observacion:enfermedad_actual,
          today:today

     }
      $.ajax({
     url:'/actualizar_enfermedad_actual',
     method:'POST',
     data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
     dataType:'JSON',
     success: function(response) 
     {
          
          if(response=='1')
          {
               alert('Registro Actualizado ');
     
               window.location = '/reporte_citas';
     
          }
          else if(response=='0')
           {
           alert('Error en la Incorporación del Registro');
           }
     
         
     },
     error: function(xhr, status, error) {
         Swal.fire('Error!', "Error al actualizar el registro", "error");
         console.error(xhr.responseText);
     }
 });
     
}
);

 



