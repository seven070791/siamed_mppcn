
//  Este es el document ready
 
 $(function()
 {
   
    
 });



 function  listar_Entradas_Medicamento(desde,hasta,id_medicamento)
 {
 
    let ruta_imagen =rootpath
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today= day+"/"+month+"/"+now.getFullYear(); 
    let encabezado='';
    data=new Date(desde);
    let dataFormatada_desde = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    data=new Date(hasta);
    let dataFormatada_hasta = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear();     
    if(dataFormatada_desde!='NaN/NaN/NaN' && dataFormatada_hasta!='NaN/NaN/NaN') 
        {
            encabezado= encabezado +'FECHA'+' '+ 'DESDE:'+' '+dataFormatada_desde+' '+' '+'HASTA'+' '+' '+dataFormatada_hasta+' '+' ';
        }
     
     
     var table = $('#table_movimientos_medicamentos').DataTable( {
         
         dom: "Bfrtip",
         buttons:{
             dom: {
                 button: {
                     className: 'btn-xs-xs',
                     header:true,
                     footer: true,
                 },
                 
             },
             
             buttons: [
             {
                 //definimos estilos del boton de pdf
                 extend: "pdf",
                 text:'PDF',
                 className:'btn-xs btn-dark',
                 title:'RELACION ENTRADAS DEL MEDICAMENTO',
                 download: 'open',
                 exportOptions: {
                             columns: [ 0,1,2,3],
                             
                 },   
                 alignment: 'center',                 
               customize:function(doc)
                    {                       
                    //Remove the title created by datatTables
                         doc.content.splice(0,1);
                       
                         doc.styles.title= 
                         { 
                              color: '#4c8aa0',
                              fontSize: '18',
                              alignment: 'center'                     
                         }
                         doc.styles['td:nth-child(2)'] = 
                         {
                              width: '100px',
                              'max-width': '100px'
                         },
                         doc.styles.tableHeader = 
                         {
                              fillColor:'#4c8aa0',
                              color:'white',
                              alignment:'center'
                         }, 
               // Create a header
               doc.pageMargins = [140,85,0,30];
               doc['header'] = (function (page, pages)
               {                      
                    doc.styles.title = 
                    {    
                         color: '#4c8aa0',
                         fontSize: '18',
                         alignment: 'center',  
                    } 
                    return {  
                              columns:
                              [
                                   {                              
                                        margin: [ 40, 3, 40, 40 ],                                 
                                        image:ruta_imagen,
                                        width: 500,  
                                                                 
                                   },                        
                                   {
                                        margin :[ -470, 40, -25, 0 ],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center' , 
                                        text: 'CONTROL DE INVENTARIO',
                                        fontSize: 18,                             
                                   },
                                   {
                                   margin :[ -408, 70, -25, 0 ],     
                                   text:encabezado+' '+'TIPO DE MOVIMIENTO:'+' '+'ENTRADAS',
                                   },
                              ], 
                         }                               
                }); 
               // Create a footer
               doc['footer'] = (function (page, pages)
               {    
                    return {
                              columns:
                              [
                                   {
                                        alignment: 'center',
                                        text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                   }
                              ],               
                              }                            
                    });
      
                    },
               },

             {    
                  //definimos estilos del boton de excel
                  extend: "excel",
                  text:'Excel',
                  className:'btn-xs btn-dark',
                  title:'NOTAS DE ENTREGAS', 
                 
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4]
                  },
                  excelStyles: {                                                
                    "template": [
                        "blue_medium",
                        "header_blue",
                        "title_medium"
                    ]                                  
                },

             }
             ]            
         }, 
        

         "order":[[0,"asc"]],					
         "paging": true,
         "lengthChange": true,
        
         dom: 'Blfrtip',
         "searching": true,
         "lengthMenu": [
              [ 10, 25, 50, -1 ],
              [ '10', '25', '50', 'Todos' ]
         ], 
        
         "ordering": true,
         "info": true,
         "autoWidth": true,
         //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
         "ajax":
         {
           
            "url":base_url+"/GenerarReportesEntradaPorFecha/"+desde+'/'+hasta+'/'+id_medicamento,
          "type":"GET",
          dataSrc:''
         },
         "columns":
         [
             
            //{data:'cedula_beneficiario'},
            {data:'descripcion'},
            {data:'fecha_entrada'},
            {data:'cantidad'},                 
            {data:'nombre'},   
           
         ],
         
   "language":
       {
           "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
              {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
              },
          ]
       
    },
});
 }
  

 function  listar_Salidas_Medicamento(desde,hasta,id_medicamento)
 {
    let ruta_imagen =rootpath
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today= day+"/"+month+"/"+now.getFullYear(); 
    let encabezado='';
    data=new Date(desde);
    let dataFormatada_desde = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    data=new Date(hasta);
    let dataFormatada_hasta = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear();     
    if(dataFormatada_desde!='NaN/NaN/NaN' && dataFormatada_hasta!='NaN/NaN/NaN') 
        {
            encabezado= encabezado +'FECHA'+' '+ 'DESDE:'+' '+dataFormatada_desde+' '+' '+'HASTA'+' '+' '+dataFormatada_hasta+' '+' ';
        }
     var table = $('#table_movimientos_medicamentos').DataTable( {
         
         dom: "Bfrtip",
         buttons:{
             dom: {
                 button: {
                     className: 'btn-xs-xs'
                 },
                 
             },
             
             buttons: [
             {
                 //definimos estilos del boton de pdf
                 extend: "pdf",
                 text:'PDF',
                 className:'btn-xs btn-dark',
                 title:'RELACION SALIDAS DEL MEDICAMENTO',
                 download: 'open',
                 exportOptions: {
                             columns: [ 0,1,2,3],
                             
                 },   
                 alignment: 'center',                 
               customize:function(doc)
                    {                       
                    //Remove the title created by datatTables
                         doc.content.splice(0,1);
                       
                         doc.styles.title= 
                         { 
                              color: '#4c8aa0',
                              fontSize: '18',
                              alignment: 'center'                     
                         }
                         doc.styles['td:nth-child(2)'] = 
                         {
                              width: '100px',
                              'max-width': '100px'
                         },
                         doc.styles.tableHeader = 
                         {
                              fillColor:'#4c8aa0',
                              color:'white',
                              alignment:'center'
                         }, 
               // Create a header
               doc.pageMargins = [140,85,0,30];
               doc['header'] = (function (page, pages)
               {                      
                    doc.styles.title = 
                    {    
                         color: '#4c8aa0',
                         fontSize: '18',
                         alignment: 'center',  
                    } 
                    return {  
                              columns:
                              [
                                   {                              
                                        margin: [ 40, 3, 40, 40 ],                                 
                                        image:ruta_imagen,
                                        width: 500,  
                                                                 
                                   },                        
                                   {
                                        margin :[ -470, 40, -25, 0 ],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center' , 
                                        text: 'CONTROL DE INVENTARIO',
                                        fontSize: 18,                             
                                   },
                                   {
                                   margin :[ -408, 70, -25, 0 ],     
                                   text:encabezado+' '+'TIPO DE MOVIMIENTO:'+' '+'SALIDAS',
                                   },
                              ], 
                         }                               
                }); 
               // Create a footer
               doc['footer'] = (function (page, pages)
               {    
                    return {
                              columns:
                              [
                                   {
                                        alignment: 'center',
                                        text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                   }
                              ],               
                              }                            
                    });
      
                    },
               },
             {    
                  //definimos estilos del boton de excel
                  extend: "excel",
                  text:'Excel',
                  className:'btn-xs btn-dark',
                  title:'NOTAS DE ENTREGAS', 
                 
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4]
                  },
                  excelStyles: {                                                
                    "template": [
                        "blue_medium",
                        "header_blue",
                        "title_medium"
                    ]                                  
                },

             }
             ]            
         }, 
        

         "order":[[0,"desc"]],					
         "paging": true,
         "lengthChange": true,
        
         dom: 'Blfrtip',
         "searching": true,
         "lengthMenu": [
              [ 10, 25, 50, -1 ],
              [ '10', '25', '50', 'Todos' ]
         ], 
        
         "ordering": true,
         "info": true,
         "autoWidth": true,
         //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
         "ajax":
         {
           
            "url":"/GenerarReportesSalidasPorFecha/"+desde+'/'+hasta+'/'+id_medicamento,
          "type":"GET",
          dataSrc:''
         },
         "columns":
         [
             
            //{data:'cedula_beneficiario'},
            {data:'descripcion'},
            {data:'fecha_salida'},
            {data:'cantidad'},                 
            {data:'nombre'},   
           
         ],
         
   "language":
       {
           "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
              {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
              },
          ]
       
    },
});
 }
// $(document).on('click','#btnExcelEntradas', function(e)
// {
//    e.preventDefault();
//     var desde =$('#desde').val();
//     var hasta =$('#hasta').val();
//     var id_medicamento =$('#id').val();
//     var todo='null';
//     var swfechaok =1;
//      if (document.getElementById('todo').checked)
//    {
//       todo='true'
//       desde=null;
//       hasta=null;
//    }else 
//    {
//       todo='false'
//       if (desde=='dd/mm/yy'||desde=='' )
//       {
//        swfechaok=0;
//       }
//    if (hasta=='dd/mm/yy'||hasta=='') 
//    {
//     swfechaok=0;
//    }
//   }    

//   if (swfechaok==1) {
//    window.open('../GenerarReportesEntradaPorFechaExcel/'+desde+'/'+hasta+'/'+id_medicamento+'/'+todo,'_blank');
// }else
// {
//    alert('Por favor Verificar Fechas ');
// }
    
// });






$(document).on('click','#btnregresar ', function(e)
{
    e.preventDefault();
    window.location = '/vistamedicamentos/';
});

$(document).on('click','#btnfiltrar ', function(e)
{
    e.preventDefault(); 
    $("#table_movimientos_medicamentos").dataTable().fnDestroy();
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val(); 
    let id_medicamento=$('#id_medicamento').val(); 
    if (desde=='')
   {
       desde='null'    
   } 
   if (hasta=='')
   {
       hasta='null'    
   } 

   if (desde=='null'&&hasta!='null') 
   {
       alert('DEDE INDICAR EL CAMPO DESDE');
       
   }
   else if (hasta=='null'&&desde!='null') 
   {
       alert('DEDE INDICAR EL CAMPO HASTA');
   } 
   else if (hasta<desde) {
     alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
   }

  else{
   let accion =$('#accion').val();


   
   if(accion==1){
     
      $(".fila1").css('display', 'none');
      listar_Entradas_Medicamento(desde,hasta,id_medicamento);
   }
   else  if(accion==2)
      {
         let desde          =$('#desde').val();
         let hasta          =$('#hasta').val(); 
         let id_medicamento=$('#id_medicamento').val(); 
         if (desde==''&& hasta=='')
         {
             desde='null'
            hasta='null'     
         } 
     
      $(".fila1").css('display', 'none');
      listar_Salidas_Medicamento(desde,hasta,id_medicamento);
      } 
      else  {
         alert('DEBE SELECCIONAR UNA OPCION ');
        
      }
  }

});
// $(document).on('click','#btnlimpiar', function(e)
// {
//      e.preventDefault();
//   $('input[type="date"]').val('');
//   let id_medicamento=$('#id_medicamento').val(); 
//   let desde          =$('#desde').val();
//   let hasta          =$('#hasta').val(); 
//   if (desde==''&& hasta=='')
//   {
//     desde='null'
//     hasta='null'   
//   } 
//   $('#accion').val(0);
 
//   $("#table_movimientos_medicamentos").dataTable().fnDestroy();
  
//     //istar_Salidas_Medicamento(desde,hasta,id_medicamento);
      
// });

 $(document).on('click','#btnlimpiar', function(e)
 {
   e.preventDefault();
});
