/*
 *Este es el document ready
 */
 $(function()
 {
     listarAutorizador();
 });
 /*
  * Función para definir datatable:
  */
 function listarAutorizador()
 {
      $('#table_autorizador').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_Autorizador",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'cedula'},
                {data:'nombre_apellido'},                
                {data:'fecha_desde'},
                {data:'fecha_hasta'},                
                {data:'estatus'},  
                {orderable: true,
                 render:function(data, type, row)
                 {
                  return '<a href="javascript:;" class="btn btn-xs btn-secondary editar" style=" font-size:1px" data-toggle="tooltip" title="Editar " id='+row.id+' nombre="'+row.nombre+'" apellido="'+row.apellido+'" cedula="'+row.cedula+'" fecha_desde='+row.fecha_desde+' fecha_hasta='+row.fecha_hasta+' estatus='+row.estatus+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();
      $('#modal').find('#activo').hide();
      $("#modal").modal("show");
     

 });
 $('#lista_autorizador').on('click','.editar', function(e)
 {
     var id              =$(this).attr('id');
     var cedula          =$(this).attr('cedula');
     var nombre          =$(this).attr('nombre');
     var apellido        =$(this).attr('apellido');
     var fecha_desde     =$(this).attr('fecha_desde');
     var estatus         =$(this).attr('estatus');
     $("#modal").modal("show");
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#vigencia').show();
     $('#modal').find('#activo').show();

     $('#id').val(id);
     $('#cedula').val(cedula)
     $('#nombre').val(nombre)
     $('#apellido').val(apellido)

     $('#cedula_anterior').val(cedula)
     $('#nombre_anterior').val(nombre)
     $('#apellido_anterior').val(apellido)


     $('#fecha').val(fecha_desde)
     if(estatus=='Activo')
     {
          $('#vigencia').attr('checked','checked');
          $('#vigencia').val('false');
          $('#vigencia_anterior').val('true');
          
     }
     if(estatus=='Inactivo')
     {
          $('#vigencia').removeAttr('checked')
          $('#vigencia').val('true')
          $('#vigencia_anterior').val('false');
     }
 });




 $(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();

      let cedula=$('#cedula').val();
      let fecha_desde=$('#fecha').val();
      let nombre=$('#nombre').val();
      let apellido=$('#apellido').val();
      if (cedula=='') {
          alert('El campo cedula es obligatorio');
      }
      else
      {   
          var url='/agregar_Autorizador';
          var data=
     {
          cedula:cedula,
          nombre:nombre,
          apellido:apellido,
          fecha_desde  :fecha_desde,
     }
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               //alert(data);
               if(data=='1')
               {
                    alert('Registro Incorporado');
                    window.location = '/vistaAutorizador';
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });
 
     }
});
$(document).on('click','#btnActualizar', function(e)
{
      e.preventDefault();
      let id=$('#id').val();
      let cedula=$('#cedula').val();
      let fecha_desde=$('#fecha').val();
      let nombre=$('#nombre').val();
      let apellido=$('#apellido').val();
      var vigencia='false';
      let nombre_anterior=$('#nombre_anterior').val();
      let apellido_anterior=$('#apellido_anterior').val();

     if (cedula=='') {
          alert('El campo cedula es obligatorio');
      }
      else
      {

     if($('#vigencia').is(':checked'))
     {
          vigencia='true';
     }
     else
     {
          vigencia='false';
     }

     var objeto_anterior = {
          "Cedula": $('#cedula_anterior ').val(),
          "Nombre": $('#nombre_anterior ').val(),
          "Apellido": $('#apellido_anterior ').val(),
          "Activo": $('#vigencia_anterior ').val(),
              
      }
      var objeto_actual= {
          "Cedula": $('#cedula ').val(),
          "Nombre": $('#nombre ').val(),
          "Apellido": $('#apellido ').val(),
          "Activo": vigencia,
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }

     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else 
      {
     var data=
     {
          id             :id,
          cedula:cedula,
          nombre:nombre,
          apellido:apellido,
          fecha_desde  :fecha_desde,
          vigencia        :vigencia,
          datos_modificados:datos_modificados,
          nombre_anterior:nombre_anterior,
          apellido_anterior:apellido_anterior,
     }

    var url='/actualizar_Autorizador';
    $.ajax
    ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
               alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/vistaAutorizador';             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });

      }
}
 });