/*
 *Este es el document ready
 */
 $(function()
 {
     var id_medicamento=$("#id_medicamento").val();
  
     listarcompuestos(id_medicamento);
   
     //Lleno la persiana de la unidad de medida
 });
 /*
  * Función para definir datatable:
  */
 function listarcompuestos(id_medicamento)
 {
      $('#table_compuestos').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_compuesto/"+id_medicamento,
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [{
                data:'id_compuesto'},
                {data:'tipo_medicamento'},                           
                {data:'cantidad'},
                {data:'unidad_medida'}, 
                

                {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.borrado=='Bloqueado')
                   
                      {
                         return '<button id="btnbloqueado"class=" btn-danger btnbloqueado "disabled="disabled">'+row.borrado+'</button>'
                      }
                      else
                      {
                         return '<button id="btnactivo"class=" btn-success btnactivo"disabled="disabled">'+row.borrado+'</button>'
                       
                      } 
                   
                    }
               },




                {orderable: true,
                 render:function(data, type, row)
                 {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary  editar" style=" font-size:1px" data-toggle="tooltip" title="Actualizar Descripción y Estatus del Tipo de Medicamento" tipo_medicamento= '+row.tipo_medicamento+' id_compuesto= '+row.id_compuesto+' descripcioncompuesta="'+row.descripcioncompuesta+'" unidad_medida='+row.unidad_medida+' cantidad='+row.cantidad+' id_unidad_medida='+row.id_unidad_medida+' borrado= '+row.borrado+ '> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 

    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

      $("#modal").modal("show");
      llenar_combo_unidad_medida(e);
      llenar_combo_tipo_medicamento(e);

 });
 
 /*
* Función para ...
*/
function llenar_combo_tipo_medicamento(e, id_tipo_medicamento)
{
    
     e.preventDefault
     url='/listar_medicamentos_activos';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {   
          /*
          console.log(data);
          alert(data[0].id)
          alert(data[0].descripcion)
          */
          if(data.length>=1)
          { 
               $('#cmbTipoMedicamento').empty();
               $('#cmbTipoMedicamento').append('<option value=0  selected disabled>Seleccione</option>');                
               if(id_tipo_medicamento===undefined)
               {      
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbTipoMedicamento').append('<option value='+item.id+'>'+item.descripcion+'</option>');
     
                    });
               }
               else
               {
                  
                    $.each(data, function(i, item)
                    {
                         if(item.id===id_tipo_medicamento)
                         {
                             $('#cmbTipoMedicamento').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                        
                         }
                         else
                         {
                             
                              $('#cmbTipoMedicamento').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}
 /*
* Función para llenar combo unidad de Medida ...
*/
function  llenar_combo_unidad_medida(e,id_unidad_medida)
{
     e.preventDefault
     
     url='/listar_unidadmedida';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
     
          /*
          console.log(data);
          alert(data[0].id)
          alert(data[0].descripcion)
          */
          if(data.length>=1)
          { 
               $('#cmbUnidadMedida').empty();
               $('#cmbUnidadMedida').append('<option value=0>Seleccione</option>');               
               if(id_unidad_medida===undefined)
               {
                    
                    $.each(data, function(i, item)
                    {
                         //console.log(data)
                         $('#cmbUnidadMedida').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                        
     
                    });
               }
               else
               {
                    $.each(data, function(i, item)
                    {
                         if(item.id===id_unidad_medida)
                         {
                              $('#cmbUnidadMedida').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                         }
                         else
                         {
                              $('#cmbUnidadMedida').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                         }
                    });
               }
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}



$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();

      var id_medicamento  =$('#id_medicamento').val();
      var id_tipo_medicamento      =$('#cmbTipoMedicamento').val();
      var descripcioncompuesta     =$('#descripcioncompuesta').val();
      var cantidad                 =$('#cantidad').val();
      var unidadmedida             =$('#cmbUnidadMedida').val();

      if (id_tipo_medicamento<=0)
      {
          alert('DEBE SELECCIONAR UN PRINCIPIO ACTIVO');
      } 
      else if (unidadmedida<0)
      {
          alert('DEBE SELECCIONAR UNA UNIDAD');
      } 

      else if (cantidad=='')
      {
          alert('DEBE INGRESAR UNA CANTIDAD');
      } 
      else
      {


     
     
      var url='/agregar_compuesto';
      var data=
      {
          id_medicamento:id_medicamento,
          id_tipo_medicamento:id_tipo_medicamento,
          descripcioncompuesta:descripcioncompuesta,
          cantidad:cantidad,
          unidadmedida :unidadmedida,
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/vistacompuestos/'+id_medicamento; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
     }
});

$(document).on('click','#btnActualizar', function(e)
{
     var id_tipo_medicamento=$("#cmbTipoMedicamento").val();
     var id_medicamento       =$("#id_medicamento").val();
     var id_compuesto=$('#id_compuesto').val();        
     var descripcioncompuesta     =$('#descripcion').val();
     var cantidad                 =$('#cantidad').val();
     var unidadmedida             =$('#cmbUnidadMedida').val();
     var borrado   =$('#borrado').val();
     var borrado='false';
    
     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }
     
     var data=
     {
          id_tipo_medicamento:id_tipo_medicamento,
          id_medicamento :id_medicamento,
          id_compuesto:id_compuesto,
          descripcioncompuesta:descripcioncompuesta,
          cantidad:cantidad,
          unidadmedida :unidadmedida,
          borrado        :borrado
     }
 
     var url='/actualizar_compuestos';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/vistacompuestos/'+id_medicamento;             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
    });
 });



         $('#lista_de_compuestos').on('click','.editar', function(e)
         {   
               var id_medicamento       =$("#id_medicamento").val();
               var descripcioncompuesta =$('#descripcioncompuesta').val();
               var cantidad             =$(this).attr('cantidad');   
               var id_compuesto         =$(this).attr('id_compuesto');  
               $('#id_compuesto').val(id_compuesto);      
               $('#id_medicamento').val(id_medicamento);
              
               //Para el Ajax:
                //La url:
                url='/listar_compuesto/+id_medicamento';
                //El Objeto con parameros:
              data=
              {
               id_medicamento:id_medicamento
              };
              $.ajax(
                   {
                        url:url,
                        method:'GET',
                        data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                        dataType:'JSON',
                        beforeSend:function(data)
                        {
                             //alert('Procesando');
                        },
                        success:function(data)
                        {
                         //console.log(data);
                       
                           
                             $("#modal").modal("show");
                             $('#modal').find('#btnGuardar').hide();
                             $('#modal').find('#btnActualizar').show();
                             $('#modal').find('#borrado').show();
                             $('#modal').find('#activo').show();
                             $('#cantidad').val(cantidad);
                              llenar_combo_tipo_medicamento(e, data[0].id_tipo_medicamento);
                               llenar_combo_unidad_medida(e,data[0].id_unidad_medida);
  
                             if(data[0].borrado=='Activo')
                             {
                                  $('#borrado').attr('checked','checked');
                                  $('#borrado').val('false');
                             }
                             if(data[0].borrado=='Eliminado')
                             {
                                  $('#borrado').removeAttr('checked')
                                  $('#borrado').val('true')
                             } 
         
                        }
                   });
         });


 $(document).on('click','#btnRegresar', function(e)
{
     window.location = '/vistamedicamentos/';
})
