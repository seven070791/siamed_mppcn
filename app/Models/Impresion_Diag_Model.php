<?php

namespace App\Models;

use CodeIgniter\Model;

class Impresion_Diag_Model extends BaseModel
{


	public function agregar($data)
	{
		$builder = $this->dbconn('historial_clinico.impresion_diag');
		$query = $builder->insert($data);
		return $query;
	}


	public function buscar_consulta($id_consulta)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " impre.id";
		$strQuery .= ",impre.descripcion ";
		$strQuery .= "FROM ";
		$strQuery .= " historial_clinico.impresion_diag as impre  ";
		$strQuery .= " where impre.id_consulta='$id_consulta'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function listar_impresion_diag($n_historial)
	{

		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " distinct hc.id_medico";
		$strQuery .= ",impre.id";
		$strQuery .= ",impre.descripcion ";
		$strQuery .= ",to_char(impre.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(impre.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= ",e.descripcion as especialidad ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.impresion_diag as impre ";
		$strQuery .= "  join historial_clinico.consultas  as hc on impre.id_consulta=hc.id";
		$strQuery .= "  join  historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery .= "  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery  = $strQuery . " where impre.n_historial='$n_historial'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function buscar_impresion_diag($n_historial)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " distinct hc.id_medico";
		$strQuery .= ",impre.id";
		$strQuery .= ",impre.descripcion ";
		$strQuery .= ",to_char(impre.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(impre.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= ",e.descripcion as especialidad ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.impresion_diag as impre ";
		$strQuery .= "  join historial_clinico.consultas  as hc on impre.id_consulta=hc.id";
		$strQuery .= "  join  historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery .= "  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery .= " where impre.n_historial='$n_historial'";
		$strQuery .= " AND psicologia";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function buscar_historia_impresion_diag($n_historial)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " distinct hc.id_medico";
		$strQuery .= ",impre.id";
		$strQuery .= ",impre.descripcion ";
		$strQuery .= ",to_char(impre.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(impre.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= ",e.descripcion as especialidad ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.impresion_diag as impre ";
		$strQuery .= "  join historial_clinico.consultas  as hc on impre.id_consulta=hc.id";
		$strQuery .= "  join  historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery .= "  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery .= " where impre.n_historial='$n_historial'";
		$strQuery .= " AND psicologia";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	

	public function listar_Impresion_Diag_Individual($n_historial, $id_consulta)
	{

		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " distinct hc.id_medico";
		$strQuery .= ",impre.id";
		$strQuery .= ",impre.descripcion ";
		$strQuery .= ",to_char(impre.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(impre.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= ",e.descripcion as especialidad ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.impresion_diag as impre ";
		$strQuery .= "  join historial_clinico.consultas  as hc on impre.id_consulta=hc.id";
		$strQuery .= "  join  historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery .= "  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery  = $strQuery . " where impre.n_historial='$n_historial'";
		$strQuery .= " and hc.id=$id_consulta";
		$strQuery .= " and impre.id_consulta=$id_consulta";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return  $strQuery;
	}






	public function actualizar_impresiondiagnostica($data)
	{
		$builder = $this->dbconn('historial_clinico.impresion_diag');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
		//return  $strQuery;
	}
}
