<?php namespace App\Models;
use CodeIgniter\Model;
class Antecendentes_Patologicosp_Model extends BaseModel
{

	
    public function agregar_antecedentes_patologicosp($data)
	{
		
		$builder = $this->dbconn('historial_clinico.antecedentes_patologicosp');
		$query = $builder->insert($data);  
	   return $query;
    }

	public function buscar_consulta($id_consulta)
	{
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" app.id";  
	   $strQuery .=",app.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .=" historial_clinico.antecedentes_patologicosp as app  ";	
	   $strQuery  =$strQuery . " where app.id_consulta='$id_consulta'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	 
	public function antecedentes_patologicosp($n_historial)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",app.id";  
	   $strQuery .=",app.descripcion "; 
	   $strQuery .=",to_char(app.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
	   $strQuery .=",to_char(app.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";  
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.antecedentes_patologicosp as app ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on app.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where app.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	    //return  $strQuery;
	   return $resultado;
	  
	}
	public function 	listar_Antecendentes_Patologicosp_Individual($n_historial,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",app.id";  
	   $strQuery .=",app.descripcion "; 
	   $strQuery .=",to_char(app.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
	   $strQuery .=",to_char(app.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";  
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.antecedentes_patologicosp as app ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on app.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where app.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	    //return  $strQuery;
	   return $resultado;
	  
	}





	public function actualizar_antecedentes_patologicosp($data)
	{
		$builder = $this->dbconn('historial_clinico.antecedentes_patologicosp');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}

}

