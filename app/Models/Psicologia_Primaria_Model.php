<?php

namespace App\Models;

use CodeIgniter\Model;

class Psicologia_Primaria_Model extends BaseModel
{
	public function Guardar_atencion_psicologia_p($data)
	{
		$builder = $this->dbconn('historial_clinico.psicologia_primaria');
		$query = $builder->insert($data);
		return $query;
	}

	public function listar_psicologia_primaria($numeroHistorial)
	{

		$builder = $this->dbconn('historial_clinico.psicologia_primaria as psico');
		$builder->select(
				"psico.id
		  ,psico.n_historial
		  ,evolucion
		  ,observacion
		  ,to_char(psico.fecha_seguimiento,'dd/mm/yyyy') as fecha_seguimiento
		  ,CONCAT(m.nombre,' ', m.apellido) AS nombre
		  ,m.id as id_medico_tabla"
			);
		$builder->join('historial_clinico.medicos as m ', 'psico.id_medico=m.id');
		$query = $builder->where('psico.n_historial', $numeroHistorial);
		$query = $builder->get();
		return $query;
	}

	public function listar_Psicologia_Primaria_Individual($n_historial)
	{

		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " distinct hc.id_medico";
		$strQuery .= ",psico.id";
		$strQuery .= ",psico.n_historial";
		$strQuery .= ",psico.evolucion";
		$strQuery .= ",to_char(psico.fecha_seguimiento,'dd/mm/yyyy') as fecha_seguimiento ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= ",e.descripcion as especialidad ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.psicologia_primaria as psico  ";
		$strQuery .= "  join historial_clinico.consultas  as hc on psico.id_consulta=hc.id";
		$strQuery .= "  join  historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery .= "  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery  = $strQuery . " where psico.n_historial='$n_historial'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function listar_Psicologia_Primaria_observacion($n_historial,$id_consulta)
	{
		
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " sp.observacion ";
		$strQuery .= "FROM ";
		$strQuery .= "  historial_clinico.psicologia_primaria as sp ";
		$strQuery  .= " WHERE sp.n_historial='$n_historial'";
		$strQuery .= "  AND sp.id_consulta='$id_consulta'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function actualizar_Psicologia_Primaria($data)
	{

		$builder = $this->dbconn('historial_clinico.psicologia_primaria');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
