<?php namespace App\Models;
use CodeIgniter\Model;
class Autorizador_Model extends BaseModel
{
     public function getAll($estatus=null)
     {
	  $builder =$this->dbconn('public.autorizador as a');
	  $builder->select
	  (
		"a.id,a.cedula,CONCAT(a.nombre,' ', a.apellido) AS nombre_apellido,a.nombre,a.apellido,
		,to_char(a.fecha_desde,'dd/mm/yyyy') as fecha_desde
		,to_char(a.fecha_hasta,'dd/mm/yyyy') as fecha_hasta
		,CASE WHEN a.vigencia='true' THEN 'Activo' ELSE 'Inactivo' END AS estatus"
	  );
	  $query = $builder->get();
	  return $query;	
     }

	 public function getAllActivos($fecha_salida=null)
     {

		$db      = \Config\Database::connect();
		$strQuery  = "";
		$strQuery .="SELECT a.id,CONCAT(a.nombre,' ', a.apellido) AS nombre_apellido ";
		$strQuery .="FROM public.autorizador as a ";
		$strQuery .= " where a.vigencia='true'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
     } 
	 public function buscar_autorizador_activo($id_autorizador=null)
     {
		$db      = \Config\Database::connect();
		$strQuery  = "";
		$strQuery .="SELECT a.id,CONCAT(a.nombre,' ', a.apellido) AS nombre_apellido ";
		$strQuery .="FROM public.autorizador as a ";
		$strQuery .= " where a.id=$id_autorizador";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
     } 

	 public function buscar_ultimo_id()
     {

		$db      = \Config\Database::connect();
		$strQuery  = "";
		$strQuery .=" SELECT MAX(a.id) ";
		$strQuery .="FROM public.autorizador as a ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
     } 

	


	 public function Agregar($data)
	 {
		$builder = $this->dbconn('public.autorizador');
		$query = $builder->insert($data);  
		return $query;
     }
    public function actualizar($data)
	{
		$builder = $this->dbconn('public.autorizador');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function cambiar_vigencia_autorizador()
	{
		$builder = $this->dbconn('public.autorizador');
		$query = $builder->update(['vigencia' => false]);
		return $query;
	}

	public function update_fecha_hasta($datos)
	{
		$builder = $this->dbconn('public.autorizador');
		$query = $builder->update(['fecha_hasta' => $datos['fecha_desde']]);
		$builder->where('id', $datos['id']);
		return $query;
	}
	

	
	

     public function getAllParaSistemas($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.rol as r');
	  $builder->select
	  (
	       "r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
	  );
	  $query = $builder->get();
	  return $query;
     }
     
     public function getDatosRol($id=null){
	  $builder = $this->dbconn('seguridad.rol r');
	  $builder->select
	       (
		    'r.id
		    ,r.rol
		    ,r.activo'
	       );
	  $builder->where('r.id', $id);
	  $query = $builder->get();
	  return $query;
     }
}
