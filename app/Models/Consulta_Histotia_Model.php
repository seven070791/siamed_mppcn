<?php

namespace App\Models;

use CodeIgniter\Model;

class Consulta_Histotia_Model extends BaseModel
{
	public function agregar_consulta_historial($data)
	{
		$builder = $this->dbconn('historial_clinico.consultas');
		$query = $builder->insert($data);
		return $query;
	}



	public function listar_Historial_consultas($n_historial)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->select(
	    "hc.id
		,hc.n_historial
		,hc.id_medico
		,hc.borrado
		,hc.tipo_beneficiario
		,hm.cedula
		,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia, hc.fecha_asistencia as fecha
		,CONCAT(m.nombre,' ', m.apellido) AS nombre
		 ,e.descripcion as especialidad"
		);
		$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
		$builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
		$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		$builder->where(['hc.consulta' => true]);
		$builder->where(['hc.borrado' => false]);
		$builder->where(['hm.borrado' => false]);
		$builder->where(['hc.n_historial' => $n_historial]);
		$builder->orderBy('fecha', 'DESC');



		$query = $builder->get();
		return $query;
	}


	public function listar_citas_medicos($medico_id)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "c.fecha_creacion as fecha_orden";
		$strQuery .= ",c.id";
		$strQuery .= ",c.n_historial";
		$strQuery .= ",hm.cedula";
		$strQuery .= ",hm.id as id_historial_medico";
		$strQuery .= ",CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= ",c.id_medico";
		$strQuery .= ",c.tipo_beneficiario";
		$strQuery .= ",c.fecha_creacion as fecha_creacion";
		$strQuery .= ",c.fecha_asistencia as fecha_asistencia";
		$strQuery .= ",CASE WHEN b.telefono='' THEN '___ ___ ___' ELSE b.telefono END AS telefono";
		$strQuery .= ",to_char(c.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",CASE WHEN ea.id is null THEN 'NO_ATENDIDO' else 'ATENDIDO' end as  atendido ";
		$strQuery .= "FROM  ";
		$strQuery .= "historial_clinico.consultas AS c ";
		$strQuery .= "JOIN  ";
		$strQuery .= "historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "JOIN ";
		$strQuery .= "vista_beneficiarios AS b ON hm.cedula=b.cedula ";
		$strQuery .= "LEFT JOIN ";
		$strQuery .= "historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta ";
		$strQuery .= "WHERE ";
		$strQuery .= "hm.borrado=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.consulta=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.borrado=false ";
		$strQuery .= "AND ";
		$strQuery .= "c.id_medico='$medico_id'";
		$strQuery .= "order by atendido desc ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_consultas_medicos($medico_id)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery = "SELECT ";
		$strQuery .= "c.fecha_creacion as fecha_orden";
		$strQuery .= ",c.id";
		$strQuery .= ",c.n_historial";
		$strQuery .= ",hm.cedula";
		$strQuery .= ",hm.id as id_historial_medico";
		$strQuery .= ",CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= ",c.id_medico";
		$strQuery .= ",c.tipo_beneficiario";
		$strQuery .= ",c.fecha_creacion as fecha_creacion";
		$strQuery .= ",to_char(c.fecha_asistencia,'dd-mm-yyyy') as fecha_asistencia";
		$strQuery .= ",CASE WHEN b.telefono='' THEN '___ ___ ___' ELSE b.telefono END AS telefono";
		$strQuery .= ",to_char(c.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",CASE WHEN ea.id is null THEN 'NO_ATENDIDO' else 'ATENDIDO' end as  atendido ";
		$strQuery .= "FROM  ";
		$strQuery .= "historial_clinico.consultas AS c ";
		$strQuery .= "JOIN  ";
		$strQuery .= "historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "JOIN ";
		$strQuery .= "vista_beneficiarios AS b ON hm.cedula=b.cedula ";
		$strQuery .= "LEFT JOIN ";
		$strQuery .= "historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta ";
		$strQuery .= "WHERE ";
		$strQuery .= "hm.borrado=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.consulta=true  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.borrado=false ";
		$strQuery .= "AND ";
		$strQuery .= "c.id_medico='$medico_id'";
		$strQuery .= "order by atendido desc ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}







	public function listar_Historial_citas($n_historial)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->select(
		"hc.id
		,hc.n_historial
		,hc.id_medico
		,hc.borrado as borrado
		,hc.consulta
		,hc.asistencia
		,hc.tipo_beneficiario
		,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia
		,hm.cedula
		,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,CONCAT(m.nombre,' ', m.apellido) AS nombre
		 ,e.descripcion as especialidad"
		);
		$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
		$builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
		$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		//$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		$builder->where(['hc.consulta' => false]);
		$builder->where(['hc.borrado' => false]);
		$builder->where(['hm.borrado' => false]);
		$builder->where(['hc.n_historial' => $n_historial]);
		$query = $builder->get();
		return $query;
	}





	public function borrar_consulta($data)
	{
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->where('hc.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function actualizar_asistencia($data)
	{

		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->where('hc.id', $data['id'], 'hc.borrado', 'false');
		$query = $builder->update($data);
		return $query;
	}




	public function listar_signos_vitales($n_historial, $id_consulta)
	{
		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " hc.id";
		$strQuery .= ",hc.peso";
		$strQuery .= ",hc.talla";
		$strQuery .= ",hc.spo2";
		$strQuery .= ",to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
		$strQuery .= ",hc.frecuencia_c";
		$strQuery .= ",hc.frecuencia_r";
		$strQuery .= ",hc.temperatura ";
		$strQuery .= ",hc.tension_alta";
		$strQuery .= ",hc.tension_baja ";
		$strQuery .= ",hc.imc";
		$strQuery .= ",hc.motivo_consulta ";
		$strQuery .= ",hc.user_id ";
		$strQuery .= ",CONCAT(usuarios.nombre,' ', usuarios.apellido) AS user_nombre ";
		$strQuery .= "FROM ";
		$strQuery .= "historial_clinico.consultas as hc ";
		$strQuery .= "join usuarios on hc.user_id= usuarios.id ";
		$strQuery .= " where n_historial='$n_historial'";
		$strQuery .= " AND  hc.id=$id_consulta";
		// $strQuery .=" AND ";
		//$strQuery .=" hc.borrado=false";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		//return  $strQuery;
		return $resultado;
	}


	public function actualizar_signos_vitales_citas($data)
	{

		$builder = $this->dbconn('historial_clinico.consultas ');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function listar_reporte_citas($desde = null, $hasta = null, $medico = 0, $especialidad, $control = 0, $asistio = 0, $personal = 0,$today)
	{

		$db = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT DISTINCT ";
		$strQuery .= "c.user_id, c.id_consulta, c.id_medico, c.n_historial, ";
		$strQuery .= "c.peso, c.talla, c.spo2, c.frecuencia_c, c.frecuencia_r, c.temperatura, c.tension_alta, c.tension_baja, c.imc, ";
		$strQuery .= "CONCAT(b.nombre, ' ', b.apellido) AS nombre, ";
		$strQuery .= "CONCAT(nombremedico, ' ', apellidomedico) AS nombremedico, b.cedula, ";
		$strQuery .= "descripcion AS especialidad, id_especialidad, ";
		$strQuery .= "c.tipo_beneficiario, c.fecha_creacion AS fecha_creacion, c.fecha_asistencia, c.format_fecha_asistencia, ";
		$strQuery .= "c.asistencia, c.controlasistencia, c.borrado, c.consulta, ";
		$strQuery .= "CASE WHEN b.telefono = '' THEN '0' ELSE b.telefono END AS telefono, ";
		$strQuery .= "CASE WHEN c.diagnostico IS NOT NULL AND c.diagnostico <> 0 THEN true ELSE false END AS diagnostico_activo, ";
		$strQuery .= "CASE WHEN c.peso <> 0 OR c.talla <> 0 OR c.spo2 <> 0 OR c.frecuencia_c <> 0 OR ";
		$strQuery .= "c.frecuencia_r <> 0 OR c.temperatura <> 0 OR c.tension_alta <> 0 OR ";
		$strQuery .= "c.tension_baja <> 0 OR c.imc <> 0 THEN true ELSE false END AS signos_activos, ";
		$strQuery .= "CASE WHEN (CASE WHEN c.peso <> 0 OR c.talla <> 0 OR c.spo2 <> 0 OR c.frecuencia_c <> 0 OR ";
		$strQuery .= "c.frecuencia_r <> 0 OR c.temperatura <> 0 OR c.tension_alta <> 0 OR ";
		$strQuery .= "c.tension_baja <> 0 OR c.imc <> 0 THEN true ELSE false END) = false ";
		$strQuery .= "AND (CASE WHEN c.diagnostico IS NOT NULL AND c.diagnostico <> 0 THEN true ELSE false END) = false THEN false ";
		$strQuery .= "ELSE true END AS atencion ";
		$strQuery .= "FROM ( ";
		$strQuery .= "SELECT c.id AS id_consulta, c.n_historial, c.peso, c.talla, c.spo2, c.frecuencia_c, c.frecuencia_r, ";
		$strQuery .= "c.temperatura, c.tension_alta, c.tension_baja, c.imc, c.id_medico, c.tipo_beneficiario, ";
		$strQuery .= "ea.id_consulta AS diagnostico, ";
		$strQuery .= "CASE WHEN c.consulta = 't' THEN 'Consulta' ELSE 'Cita' END AS consulta, ";
		$strQuery .= "c.borrado, c.user_id, c.asistencia, ";
		$strQuery .= "TO_CHAR(c.fecha_creacion, 'dd-mm-yyyy') AS fecha_creacion, ";
		$strQuery .= "TO_CHAR(c.fecha_asistencia, 'dd-mm-yyyy') AS fecha_asistencia, ";
		$strQuery .= "c.fecha_asistencia AS format_fecha_asistencia, ";
		$strQuery .= "CASE WHEN fecha_asistencia >= CURRENT_DATE   AND asistencia = 'f' THEN 'En espera' ";
		$strQuery .= " WHEN fecha_asistencia < CURRENT_DATE   AND asistencia = 'f' THEN 'NO' ";
		$strQuery .= " WHEN fecha_asistencia >= CURRENT_DATE AND asistencia = 't' THEN 'Si' ";
		$strQuery .= " WHEN fecha_asistencia = CURRENT_DATE  AND consulta = 't' THEN 'Si' ";
		$strQuery .= "ELSE 'SI' END AS controlasistencia, ";
		$strQuery .= "med.nombre AS nombremedico, med.apellido AS apellidomedico, e.descripcion, ";
		$strQuery .= "e.id_especialidad AS id_especialidad ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.medicos AS med ON c.id_medico = med.id ";
		$strQuery .= "JOIN historial_clinico.especialidades AS e ON med.especialidad = e.id_especialidad ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id = ea.id_consulta ";
		$strQuery .= ") AS c, ";
		$strQuery .= "( SELECT cedula, id AS id_historial_medico, n_historial, borrado ";
		$strQuery .= "FROM historial_clinico.historial_medico ) AS hm, ";
		$strQuery .= "( ";
		$strQuery .= "SELECT cedula_trabajador::text AS cedula, nombre, apellido, telefono ";
		$strQuery .= "FROM titulares ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT cedula AS cedula, nombre, apellido, telefono ";
		$strQuery .= "FROM familiares ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT cedula AS cedula, nombre, apellido, telefono ";
		$strQuery .= "FROM cortesia ";
		$strQuery .= ") AS b ";
		$strWhere ="";
		$strQuery .= "WHERE c.n_historial = hm.n_historial ";
		$strQuery .= "AND hm.cedula = b.cedula ";
		$strQuery .= "AND hm.borrado = false ";
		$strQuery .= "AND c.borrado = false ";

		
		if ($especialidad != '0') {

			$strWhere .= " AND id_especialidad='" . $especialidad . "'";
		}

		if ($personal != '0' and $personal != 'null') {

			$strWhere .= " AND c.user_id='" . $personal . "'";
		}

		if ($control != 'Seleccione') {

			$strWhere .= " AND c.consulta='" . $control . "'";
		}

		if ($asistio != 'Seleccione') {
			if ($asistio == 't') {
				$strWhere .= " AND c.asistencia='t'";
			} else if ($asistio == 'f') {
				$strWhere .= "AND c.asistencia='f' and c.format_fecha_asistencia < CURRENT_DATE ";
			} else if ($asistio == 'En espera') {
				$strWhere .= " AND c.asistencia='f' and c.format_fecha_asistencia>=CURRENT_DATE";
			}
		}
		if ($medico != '0') {

			$strWhere .= " AND c.id_medico='" . $medico . "'";
		}

		if ($desde != 'null' and $hasta != 'null') {

			$strWhere .= " AND format_fecha_asistencia BETWEEN '$desde'AND '$hasta'";
		}
		$strQuery = $strQuery . $strWhere;
		$strQuery .= "ORDER BY atencion ASC, c.format_fecha_asistencia DESC;";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function listar_consultas_atendidas($medico_id)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=true ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is not null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}


	
	public function listar_consultas_not_atendidas($medico_id)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_no_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=true ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is  null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
	public function listar_citas_atendidas($medico_id)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=false ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is not null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
	public function listar_citas_not_atendidas($medico_id)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_no_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=false ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is  null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function buscar_informacion_beneficiarios($cedula)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " * ";
		$strQuery .= "FROM vista_beneficiarios_con_ubicacion b ";
		$strQuery .= "WHERE b.cedula='$cedula'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
}
