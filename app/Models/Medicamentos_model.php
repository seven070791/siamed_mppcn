<?php namespace App\Models;
use CodeIgniter\Model;
class Medicamentos_model extends BaseModel
{
	
    public function getAll($id_categoria=0,$cronicos=null)
    {

	$db      = \Config\Database::connect();
	$strQuery ="";
	$strQuery .="SELECT ";
	$strQuery .="i.id";
	$strQuery .=",substring(i.descripcion,0,75) as descripcion";
	$strQuery .=",i.control";
	$strQuery .=",i.stock_minimo";
	$strQuery .=",COALESCE(e.entradas,0)as entradas";
	$strQuery .=",COALESCE(s.salidas,0) as salidas";
	$strQuery .=",CASE WHEN e.entradas IS NOT NULL AND s.salidas IS NOT NULL THEN e.entradas-s.salidas";
	$strQuery .=" WHEN e.entradas IS NULL AND s.salidas IS NOT NULL THEN 0-s.salidas ";
	$strQuery .=" WHEN e.entradas IS NOT NULL AND s.salidas IS NULL THEN e.entradas-0 ";
	$strQuery .=" ELSE 0 ";
	$strQuery .=" END AS stock ";
	//$strQuery .=",i.med_cronico";
	$strQuery.=",case when i.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
	$strQuery .=",i.fecha_creacion";
	$strQuery .=",i.estatus";
	$strQuery .=",ci.descripcion as categoria";
	$strQuery .=" FROM ";
	$strQuery .="(";
	$strQuery .="SELECT ";
	$strQuery .="m.id";
	$strQuery .=",m.descripcion";
	$strQuery .=",m.stock_minimo";
	$strQuery .=",m.med_cronico";
	$strQuery .=",c.descripcion control";
	$strQuery .=",to_char(m.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
	$strQuery .=",CASE WHEN m.borrado='t' THEN  'Eliminado' ELSE 'Activo' END AS estatus";
	$strQuery .=",m.id_tipo_medicamento";
	$strQuery .=" FROM ";
	$strQuery .="medicamentos as m";
	$strQuery .=" JOIN ";
	$strQuery .="control as c ON (m.id_control=c.id) ";
	$strQuery .="ORDER BY id ";
	$strQuery .=")";
	$strQuery .=" AS i";
	$strQuery .=" LEFT JOIN ";
	$strQuery .="(";
	$strQuery .="select id_medicamento, sum(cantidad) as entradas from entradas WHERE borrado='f' group by id_medicamento ORDER BY id_medicamento";
	$strQuery .=") ";
	$strQuery .="AS e ON i.id=e.id_medicamento";
	$strQuery .=" LEFT JOIN ";
	$strQuery .="(";
	$strQuery .="SELECT ";
	$strQuery .="e.id_medicamento";
	$strQuery .=",SUM(s.cantidad) as salidas";
	$strQuery .=" FROM ";
	$strQuery .="salidas s";
	$strQuery .=" JOIN ";
	$strQuery .="entradas e ON s.id_entrada=e.id";
	$strQuery .=" WHERE s.borrado='f'";	  
	$strQuery .=" GROUP BY ";
	$strQuery .="e.id_medicamento";
	$strQuery .=" ORDER BY ";
	$strQuery .="e.id_medicamento";
	$strQuery .=")";
	$strQuery .="AS s ON i.id=s.id_medicamento";
	$strQuery .=" LEFT JOIN ";
	$strQuery .="(";
	$strQuery .="SELECT ";
	$strQuery .="tm.id";
	$strQuery .=",tm.categoria_id";
	$strQuery .=" FROM ";
	$strQuery .="tipo_medicamento tm";
	$strQuery .=")";
	$strQuery .="AS tm ON i.id_tipo_medicamento=tm.id";
	$strQuery .=" LEFT JOIN ";
	$strQuery .="(";
	$strQuery .="SELECT ";
	$strQuery .="ci.id";
	$strQuery .=",ci.descripcion ";
	$strQuery .=" FROM ";
	$strQuery .="categoria_inventario ci";
	$strQuery .=")";
	$strQuery .="AS ci ON  tm.categoria_id=ci.id";

	if($id_categoria!=0)
	{
	   $strQuery .=" where ci.id=$id_categoria";
	}


	if($cronicos=='true')
	{
	   $strQuery .=" and i.med_cronico=$cronicos";
	}
	
	else if($cronicos=='false')
	{
	   $strQuery .="  and i.med_cronico=$cronicos";
	}
	 


	




	 

	$query = $db->query($strQuery);
	$resultado=$query->getResult();
	return $resultado;
	  //return $strQuery;
     }


	 public function listar_stock_minimo($id_categoria=0,$cronicos=null)
	 {
	
	 $db      = \Config\Database::connect();
	 $strQuery ="";
	 $strQuery .="SELECT ";
	 $strQuery .="i.id";
	 $strQuery .=",substring(i.descripcion,0,75) as descripcion";
	 $strQuery .=",i.control";
	 $strQuery .=",i.stock_minimo";
	 $strQuery .=",CASE WHEN e.entradas IS NOT NULL AND s.salidas IS NOT NULL THEN e.entradas-s.salidas";
	 $strQuery .=" WHEN e.entradas IS NULL AND s.salidas IS NOT NULL THEN 0-s.salidas ";
	 $strQuery .=" WHEN e.entradas IS NOT NULL AND s.salidas IS NULL THEN e.entradas-0 ";
	 $strQuery .=" ELSE 0 ";
	 $strQuery .=" END AS stock ";
	 $strQuery .=",i.med_cronico";
	 $strQuery .=",i.cronico";
	 $strQuery .=",i.fecha_creacion";
	 $strQuery .=",i.estatus";
	 $strQuery .=",ci.descripcion as categoria";
	 $strQuery .=",ci.id as id_categoria ";
	 $strQuery .=" FROM ";
	 $strQuery .="(";
	 $strQuery .="SELECT ";
	 $strQuery .="m.id";
	 $strQuery .=",m.descripcion";
	 $strQuery .=",m.stock_minimo";
	 $strQuery .=",m.med_cronico";
	 $strQuery.=",case when m.med_cronico='t' then 'SI'  else 'NO' end as cronico";
	 $strQuery .=",c.descripcion control";
	 $strQuery .=",to_char(m.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
	 $strQuery .=",CASE WHEN m.borrado='t' THEN  'Eliminado' ELSE 'Activo' END AS estatus";
	 $strQuery .=",m.id_tipo_medicamento";
	 $strQuery .=" FROM ";
	 $strQuery .="medicamentos as m";
	 $strQuery .=" JOIN ";
	 $strQuery .="control as c ON (m.id_control=c.id) ";
	 $strQuery .="ORDER BY id ";
	 $strQuery .=")";
	 $strQuery .=" AS i";
	 $strQuery .=" LEFT JOIN ";
	 $strQuery .="(";
	 $strQuery .="select id_medicamento, sum(cantidad) as entradas from entradas WHERE borrado='f' group by id_medicamento ORDER BY id_medicamento";
	 $strQuery .=") ";
	 $strQuery .="AS e ON i.id=e.id_medicamento";
	 $strQuery .=" LEFT JOIN ";
	 $strQuery .="(";
	 $strQuery .="SELECT ";
	 $strQuery .="e.id_medicamento";
	 $strQuery .=",SUM(s.cantidad) as salidas";
	 $strQuery .=" FROM ";
	 $strQuery .="salidas s";
	 $strQuery .=" JOIN ";
	 $strQuery .="entradas e ON s.id_entrada=e.id";
	 $strQuery .=" WHERE s.borrado='f'";	  
	 $strQuery .=" GROUP BY ";
	 $strQuery .="e.id_medicamento";
	 $strQuery .=" ORDER BY ";
	 $strQuery .="e.id_medicamento";
	 $strQuery .=")";
	 $strQuery .="AS s ON i.id=s.id_medicamento";
	 $strQuery .=" LEFT JOIN ";
	 $strQuery .="(";
	 $strQuery .="SELECT ";
	 $strQuery .="tm.id";
	 $strQuery .=",tm.categoria_id";
	 $strQuery .=" FROM ";
	 $strQuery .="tipo_medicamento tm";
	 $strQuery .=")";
	 $strQuery .="AS tm ON i.id_tipo_medicamento=tm.id";
	 $strQuery .=" LEFT JOIN ";
	 $strQuery .="(";
	 $strQuery .="SELECT ";
	 $strQuery .="ci.id";
	 $strQuery .=",ci.descripcion ";
	 $strQuery .=" FROM ";
	 $strQuery .="categoria_inventario ci";
	 $strQuery .=")";
	 $strQuery .="AS ci ON  tm.categoria_id=ci.id";
	 $strQuery .=" WHERE";
	 $strQuery .="(";
	 $strQuery .=" COALESCE(e.entradas,NULL,0)-COALESCE(s.salidas,NULL,0))<=(COALESCE(i.stock_minimo,NULL,0)";
	 $strQuery .=")";
	 
	 if($cronicos=='true')
	 {
		$strQuery .=" AND i.med_cronico=$cronicos";
	 }
	 else if($cronicos=='false')
	 {
		$strQuery .=" AND i.med_cronico=$cronicos";
	 }
	  if($id_categoria!=0 )
		 {
			$strQuery .=" AND ci.id=$id_categoria";
		 }
			   $query = $db->query($strQuery);
			  $resultado=$query->getResult(); 
			  return $resultado;
		
		
	
	
	  //return $strQuery;
	  }




	 public function getRangoFechas(string $desde,string $hasta)
	 {
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT ";
	   $strQuery .="i.id";
	   $strQuery .=",i.descripcion";
	   $strQuery .=",i.control";
	   $strQuery .=",CASE WHEN e.entradas IS NOT NULL AND s.salidas IS NOT NULL THEN e.entradas-s.salidas";
	   $strQuery .=" WHEN e.entradas IS NULL AND s.salidas IS NOT NULL THEN 0-s.salidas ";
	   $strQuery .=" WHEN e.entradas IS NOT NULL AND s.salidas IS NULL THEN e.entradas-0 ";
	   $strQuery .=" ELSE 0 ";
	   $strQuery .=" END AS stock ";
	   $strQuery .=",i.fecha_creacion";
	   $strQuery .=",i.estatus";
	   $strQuery .=" FROM ";
	   $strQuery .="(";
	   $strQuery .="SELECT ";
	   $strQuery .="m.id";
	   $strQuery .=",m.descripcion";
	   $strQuery .=",c.descripcion control";
	   $strQuery .=",to_char(m.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
	   $strQuery .=",CASE WHEN m.borrado='t' THEN  'Eliminado' ELSE 'Activo' END AS estatus";
	   $strQuery .=" FROM ";
	   $strQuery .="medicamentos as m";
	   $strQuery .=" JOIN ";
	   $strQuery .="control as c ON (m.id_control=c.id) ";
	   $strQuery .="ORDER BY id ";
	   $strQuery .=")";
	   $strQuery .=" AS i";
	   $strQuery .=" LEFT JOIN ";
	   $strQuery .="(";
	   $strQuery .="select id_medicamento, sum(cantidad) as entradas from entradas WHERE borrado='f' group by id_medicamento ORDER BY id_medicamento";
	   $strQuery .=") ";
	   $strQuery .="AS e ON i.id=e.id_medicamento";
	   $strQuery .=" LEFT JOIN ";
	   $strQuery .="(";
	   $strQuery .="SELECT ";
	   $strQuery .="e.id_medicamento";
	   $strQuery .=",SUM(s.cantidad) as salidas";
	   $strQuery .=" FROM ";
	   $strQuery .="salidas s";
	   $strQuery .=" JOIN ";
	   $strQuery .="entradas e ON s.id_entrada=e.id";
	   $strQuery .=" WHERE s.borrado='f'";
	   $strQuery .=" GROUP BY ";
	   $strQuery .="e.id_medicamento";
	   $strQuery .=" ORDER BY ";
	   $strQuery .="e.id_medicamento";
	   $strQuery .=")";
	   $strQuery .="AS s ON i.id=s.id_medicamento";
	   $strQuery .=" WHERE i.fecha_creacion BETWEEN '$desde' AND '$hasta'";

	   //return $strQuery;

	    $query = $db->query($strQuery);
	    $resultado=$query->getResult();
	    return $resultado;
	   
	  }
	public function Agregar($data)
	{	
		$builder = $this->dbconn('public.medicamentos');
		$query = $builder->insert($data);  
		return $query;
    }
	/* --------------------------------------------------------------------
 *ESTE METODO SE USA PARA PASAR LOS DATOS DEL MEDICAMENTO EN LAS ENTRADAS Y SALIDAS**
 * --------------------------------------------------------------------
 */
	public function getDatosMedicamento($id_medicamento=null)
	{

		$builder =$this->dbconn('public.medicamentos as m');
		$builder->select
		(
			"m.id,
			,m.id_presentacion
			,m.id_tipo_medicamento
			,m.fecha_creacion
			,m.descripcion as medicamento
			,c.id as control
			,c.descripcion as descripcion
			,p.id as presentacion
			,t.id as tipo_medicamento
			,m.borrado
			,m.med_cronico
			,CASE WHEN m.med_cronico='t' THEN 'SI' ELSE 'NO' END AS estatus_med_cronico
			,m.stock_minimo"
		);

		$builder->join('public.control as c','m.id_control=c.id');
		$builder->join('public.presentacion as p','m.id_presentacion=p.id');
		$builder->join('public.tipo_medicamento as t','m.id_tipo_medicamento=t.id');
		$builder->where('m.id',$id_medicamento);
		$query = $builder->get();
		return $query; 



	}

	//Obtener Totales del Medicamento: Stock, Entradas, Salidas
	public function getTotalesParaVistaEntradas($id_medicamento=null)
	{

		//$builder =$this->dbconn('public.medicamentos as m');
		$db      = \Config\Database::connect();

		$strQuery ="";
		$strQuery .="SELECT ";
		$strQuery .="e.total_entradas";
		$strQuery .=",s.total_salidas";
		$strQuery .=",e.total_entradas-s.total_salidas as total_stock";
		$strQuery .=" FROM";
		$strQuery .="(";
		$strQuery .="SELECT";
		$strQuery .=" CASE";
		$strQuery .=" WHEN sum(cantidad) IS NOT NULL THEN";
		$strQuery .=" sum(cantidad)";
		$strQuery .=" WHEN sum(cantidad) IS NULL THEN";
		$strQuery .=" 0";
		$strQuery .=" END AS";
		$strQuery .=" total_entradas";
		$strQuery .=" FROM";
		$strQuery .=" entradas";
		$strQuery .=" WHERE";
		$strQuery .=" id_medicamento=$id_medicamento AND NOT borrado";
		$strQuery .=")";
		$strQuery .=" AS e,";
		$strQuery .="(";
		$strQuery .="SELECT";
		$strQuery .=" CASE";
		$strQuery .=" WHEN sum(cantidad) IS NOT NULL THEN";
		$strQuery .=" sum(cantidad)";
		$strQuery .=" WHEN sum(cantidad) IS NULL THEN";
		$strQuery .=" 0";
		$strQuery .=" END AS";
		$strQuery .=" total_salidas";
		$strQuery .=" FROM";
		$strQuery .=" salidas";
		$strQuery .=" WHERE";
		$strQuery .=" id_medicamento=$id_medicamento AND NOT borrado";
		$strQuery .=")";
		$strQuery .=" AS s";

		$query = $db->query($strQuery);
		$totales=[];
		foreach($query->getResult() as $fila)
		{
			//echo($fila->total_entradas;);
			$totales['total_entradas']=$fila->total_entradas;
			$totales['total_salidas']=$fila->total_salidas;
			$totales['total_stock']=$fila->total_stock;
		}
		////$resultado=$query->getResult();
		return $totales;
	}

    public function actualizar($data)
	{
		$builder = $this->dbconn('public.medicamentos');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
