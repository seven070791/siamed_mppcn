<?php namespace App\Models;
use CodeIgniter\Model;
class Historial_info_Model extends BaseModel
{
	public function especialidad_hist_informativo($id_especialidad=null)
	{
 

		$db      = \Config\Database::connect();
		$strQuery ="";
		$strQuery .="SELECT ";
		$strQuery .="h_info.id ";  
		$strQuery .=",h_info.descripcion "; 
		$strQuery .="FROM ";
		$strQuery .="  historial_clinico.especialidad_hist_informativo as e  ";	
		$strQuery .="  join  historial_clinico.historial_informativo as h_info on e.histo_info_id= h_info.id   ";	
		$strQuery .="  where e.especialidad_id ='$id_especialidad' ";	
		$strQuery .="  and e.borrado ='false' ";	
		$query = $db->query($strQuery);
		$resultado=$query->getResult(); 
		return $resultado;
	 
	}

	public function histo_info_existentes($data)
	{

	$id_especialidad=$data["especialidad_id"];
	$histo_info_id=$data["histo_info_id"];
	

		$db      = \Config\Database::connect();
		$strQuery ="";
		$strQuery .="SELECT ";
		$strQuery .=" e.id, ";  
		$strQuery .=" e.especialidad_id "; 
		$strQuery .=",e.borrado ,e.histo_info_id "; 
		$strQuery .="FROM ";
		$strQuery .="  historial_clinico.especialidad_hist_informativo as e  ";		
		$strQuery .="  where e.especialidad_id ='$id_especialidad' ";		
		$strQuery .="  and e.histo_info_id ='$histo_info_id' ";
		$query = $db->query($strQuery);
		$resultado=$query->getResult(); 
		return $resultado;
	 
	}
	

	 
	public function Listar_Historial_Informativo()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT ";
	   $strQuery .="h_info.id ";  
	   $strQuery .=",h_info.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.historial_informativo as h_info ";	
	   $strQuery .="  WHERE h_info.borrado='false' ";	
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	}






	// METODO QUE  ACTIVA LOS HISTORIALES INFORMATIVOS  EXISTENTES 
	public function Activar_historial_infor($historial)
	{
		
	
		$builder = $this->dbconn(' historial_clinico.especialidad_hist_informativo ');
		
			$builder->where('especialidad_id',$historial['especialidad_id'] );
			$builder->where('histo_info_id',$historial['histo_info_id']);
			$query = $builder->update([
				'borrado' => false,
			]);
	
		return $query;
	}



	// METODO QUE ELIMINA LOS HISTORIALES INFORMATIVOS  EXISTENTES 
	public function Eliminar_historial_infor($check_borrados,$especialidad_id)
	{
		$builder = $this->dbconn(' historial_clinico.especialidad_hist_informativo ');
		foreach ($check_borrados as $Row)
		{	
			$builder->where('especialidad_id',$especialidad_id);
			$builder->where('histo_info_id',$Row);
			$query = $builder->update([
				'borrado' => true,
			]);
		}
		return $query;
	}

	public function agregar($datos)
	{
		
		$builder = $this->dbconn('historial_clinico.especialidad_hist_informativo');
		$query = $builder->insert($datos);
		return $query ? true : false;
	
	}

}

