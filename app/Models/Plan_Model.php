<?php namespace App\Models;
use CodeIgniter\Model;
class Plan_Model extends BaseModel
{



	public function agregar($data)
	{
		 $builder = $this->dbconn('historial_clinico.plan');
		 $query = $builder->insert($data);  
		return $query;
    }

	public function listar_plan($n_historial)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",plan.id";  
	   $strQuery .=",plan.descripcion "; 
	   $strQuery .=",to_char(plan.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(plan.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.plan as plan ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on plan.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where plan.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	 
	public function listar_Plan_Model_Individual($n_historial, $id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",plan.id";  
	   $strQuery .=",plan.descripcion "; 
	   $strQuery .=",to_char(plan.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(plan.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.plan as plan ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on plan.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where plan.n_historial='$n_historial'";
	   $strQuery .=" and hc.id=$id_consulta";
	   $strQuery .=" and plan.id_consulta=$id_consulta";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}

	



	public function actualizar_plan($data)
	{
		$builder = $this->dbconn('historial_clinico.plan');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}


}

