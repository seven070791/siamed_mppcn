<?php namespace App\Models;
use CodeIgniter\Model;
class Categoria_Model extends BaseModel
{
public function getAll($estatus=null)
     {
		$db      = \Config\Database::connect();
		$strQuery ="SELECT c.id,c.descripcion,to_char(c.fecha_creacion,'dd/mm/yyyy') as fecha_creacion,CASE WHEN C.borrado='t' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM public.categoria_inventario as c ";

		
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;	
     }

     public function getAllActivos()
     {
        $db      = \Config\Database::connect();
		$strQuery ="SELECT c.id,c.descripcion,to_char(c.fecha_creacion,'dd/mm/yyyy') as fecha_creacion,CASE WHEN C.borrado='t' THEN 'Bloqueado' ELSE 'Activo' END AS borrado
		FROM public.categoria_inventario as c WHERE C.borrado='f' ";

	    //return $strQuery;	
		   $query = $db->query($strQuery);
		    $resultado=$query->getResult(); 
		   return $resultado;
     }

	
     public function agregar_categoria($data)
	 {
		 $builder = $this->dbconn('public.categoria_inventario');
		 $query = $builder->insert($data);  
		return $query;
     }

     public function actualizar_categoria($data)
     {
         
         $builder = $this->dbconn('public.categoria_inventario');
         $builder->where('id', $data['id']);
         $query = $builder->update($data);
         return $query;
     }

    }