<?php

namespace App\Models;

use CodeIgniter\Model;

class Auditoria_sistema_Model extends BaseModel
{


	public function listar_auditoria_sistema($direccion_ip = null, $dispositivo = null)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " s.id ";
		$strQuery .= ", '$direccion_ip' as direccion_ip ";
		$strQuery .= ", '$dispositivo' as dispositivo ";
		$strQuery .= ",CONCAT(usuarios.nombre,' ',usuarios.apellido) as nombre ";
		$strQuery .= ",s.accion ";
		$strQuery .= ",s.hora ";
		$strQuery .= ",to_char(s.fecha,'dd/mm/yyyy') as fecha , s.fecha as fecha_normal ";
		$strQuery .= "FROM ";
		$strQuery .= "  public.auditoria_de_sistema as s";
		$strQuery .= "  JOIN usuarios on  s.user_id=usuarios.id";
		$strQuery .= "  order by s.id desc"; // Ordenar por el último id ingresado
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}



	public function agregar($auditoria)
	{
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$auditoria['hora'] = $hora;
		$auditoria['user_id']           = session('id_user');
		$auditoria['accion']   = $auditoria['accion'];
		$builder = $this->dbconn('public.auditoria_de_sistema');
		$query = $builder->insert($auditoria);
		return $query;
	}
}
