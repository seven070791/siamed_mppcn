<?php namespace App\Models;
use CodeIgniter\Model;
class Partos_Model extends BaseModel
{
	
	public function agregar($data)
	{
		
		$builder = $this->dbconn('historial_clinico.partos');
		$query = $builder->insert($data);  
	   return $query;
	}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.partos');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}

	public function listar_partos($n_historial,$id_consulta)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" partos.id";  
	   $strQuery .=",partos.id_partos_numeros_romanos "; 
	   $strQuery .=",nr.descripcion as descri_partos ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.partos ";	
	   $strQuery .="  JOIN historial_clinico.numeros_romanos as nr on partos.id_partos_numeros_romanos=nr.id";	
	   $strQuery .=" where partos.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	}
}

	