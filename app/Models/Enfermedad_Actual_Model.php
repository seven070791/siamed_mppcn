<?php

namespace App\Models;

use CodeIgniter\Model;

class Enfermedad_Actual_Model extends BaseModel
{


	public function agregar($data)
	{
		$builder = $this->dbconn('historial_clinico.enfermedad_actual');
		$query = $builder->insert($data);
		return $query;
	}

	public function listar_enfermedad_actual($n_historial, $id_consulta)

	{


		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " hc.id_medico";
		$strQuery .= ",ea.id";
		$strQuery .= ",ea.descripcion ";
		$strQuery .= ",to_char(ea.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(ea.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= "FROM ";
		$strQuery .= " historial_clinico.enfermedad_actual as ea ";
		$strQuery .= " join historial_clinico.consultas as hc on ea.n_historial=hc.n_historial";
		$strQuery .= " join historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery  = $strQuery . " where ea.n_historial='$n_historial'";
		$strQuery .= " and hc.id=$id_consulta";
		$strQuery .= " and ea.id_consulta=$id_consulta";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function listar_enfermedad_actual_Individual($n_historial, $id_consulta)

	{


		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " hc.id_medico";
		$strQuery .= ",ea.id";
		$strQuery .= ",ea.descripcion ";
		$strQuery .= ",to_char(ea.fecha_creacion,'dd/mm/yyyy') as fecha_creacion ";
		$strQuery .= ",to_char(ea.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion ";
		$strQuery .= ",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
		$strQuery .= "FROM ";
		$strQuery .= " historial_clinico.enfermedad_actual as ea ";
		$strQuery .= " join historial_clinico.consultas as hc on ea.n_historial=hc.n_historial";
		$strQuery .= " join historial_clinico.medicos as m on hc.id_medico=m.id";
		$strQuery  = $strQuery . " where ea.n_historial='$n_historial'";
		$strQuery .= " and hc.id=$id_consulta";
		$strQuery .= " and ea.id_consulta=$id_consulta";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}




	public function listar_psicologia_impre_diag($n_historial, $id_especialidad)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " ea.descripcion ";
		$strQuery .= "FROM ";
		$strQuery .= " historial_clinico.enfermedad_actual as ea  ";
		$strQuery .= " JOIN historial_clinico.consultas as hc on ea.id_consulta=hc.id  ";
		$strQuery .= " JOIN historial_clinico.medicos as m on hc.id_medico=m.id  ";
		$strQuery .= " JOIN historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
		$strQuery  = $strQuery . " where ea.n_historial='$n_historial'";
		$strQuery .= " AND e.id_especialidad=$id_especialidad ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function buscar_consulta($id_consulta)
	{
		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " ea.id";
		$strQuery .= ",ea.descripcion ";
		$strQuery .= "FROM ";
		$strQuery .= " historial_clinico.enfermedad_actual as ea ";
		$strQuery  = $strQuery . " where ea.id_consulta=$id_consulta";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function actualizar_enfermedad_actual($data)
	{
		$builder = $this->dbconn('historial_clinico.enfermedad_actual');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
		//return  $strQuery;
	}
}
