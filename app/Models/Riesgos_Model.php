<?php namespace App\Models;
use CodeIgniter\Model;
class Riesgos_Model extends BaseModel
{
	

/*
      * METODO QUE INSETAR UNA NUEVA RIESGOS EN LA TABLA PRINCIPAL RIESGOS
 */
public function agregar_riesgos_master($data)
{

   $builder = $this->dbconn('historial_clinico.riesgos');
   $query = $builder->insert($data);  
   return $query;
}
/*
  * METODO QUE ACTUALIZA RIESGOS EN LA TABLA PRINCIPAL RIESGOS
*/
public function actualizar_riesgos_master($data)
{
	$builder = $this->dbconn('historial_clinico.riesgos as h');
	$builder->where('h.id', $data['id']);
	$query = $builder->update($data);
	return $query;
}

/*
  * METODO QUE AGREGA LAS RIEGOS EN FUNCION DE LA ESPECIALIDAD 
*/
	public function agregar($data)
{
    $builder = $this->dbconn('historial_clinico.riesgos_historial');
    $query = $builder->insert($data);
    return $query ? true : false;

}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.abortos');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}
	public function Listar_riesgos()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id";  
	   $strQuery .=",riesgos.descripcion "; 
	   $strQuery .=",case when riesgos.borrado='f' then 'Activo ' ELSE 'Inactivo' end as borrado  "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos ";	
	   $strQuery .=" where riesgos.borrado='f' ";	
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}

	public function Listar_riesgos_activos()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id";  
	   $strQuery .=",riesgos.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos ";	
	   $strQuery .="  where riesgos.borrado='false' ";	
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}


	public function listar_riesgos_Historial($numeroHistorial=null)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id_riesgo ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos_historial as riesgos ";	
	   $strQuery .="  where riesgos.n_historial='$numeroHistorial' ";
	   $strQuery .="  and riesgos.borrado='false'";	
	   //return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	 return $resultado;
	}

	public function buscar_riesgos_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_riesgo = $datos['id_riesgo'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id_riesgo,riesgos.n_historial,riesgos.borrado ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos_historial as riesgos ";	
	   $strQuery .="  where riesgos.n_historial='$n_historial' ";	
	   $strQuery .="  and riesgos.id_riesgo=$id_riesgo ";	
	   $strQuery .="  and riesgos.borrado='false'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}

	public function riesgos_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_riesgo = $datos['id_riesgo'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id_riesgo,riesgos.n_historial,riesgos.borrado ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos_historial as riesgos ";	
	   $strQuery .="  where riesgos.n_historial='$n_historial' ";	
	   $strQuery .="  and riesgos.id_riesgo=$id_riesgo ";	
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}

// METODO  ACTIVA  LOS RIESGOS EXISTENTES 
public function Activar_Riesgos_existentes($riesgos)
{
	
	$builder = $this->dbconn(' historial_clinico.riesgos_historial as riesgos');
	
		$builder->where('n_historial',$riesgos['n_historial'] );
		$builder->where('id_riesgo',$riesgos['id_riesgo']);
		$query = $builder->update([
			'borrado' => false,
		]);

	return $query;
}


	public function Eliminar_Riesgos_existentes($check_borrados,$n_historia)
	{
		
		
		$builder = $this->dbconn(' historial_clinico.riesgos_historial');
		foreach ($check_borrados as $Row) {
			$builder->where('n_historial', $n_historia);
			$builder->where('id_riesgo', $Row);
			$query = $builder->update([
				'borrado' => true,
			]);
		}

		return $query;
	}
}

	