<?php namespace App\Models;
use CodeIgniter\Model;
class Antecedentes_Quirurgicos_Model extends BaseModel
{

	
    public function agregar($data)
	{
		 $builder = $this->dbconn('historial_clinico.antecedentes_quirurgicos');
		 $query = $builder->insert($data);  
		return $query;
    }

	public function listar_antecedentes_quirurgicos($n_historial)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",atcq.id";  
	   $strQuery .=",atcq.descripcion "; 
	   $strQuery .=",to_char(atcq.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(atcq.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.antecedentes_quirurgicos as atcq ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on atcq.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where atcq.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	 
	public function listar_Antecedentes_Quirurgicos_Individual($n_historial,$id_consulta)
	{
 
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" distinct hc.id_medico"; 
	   $strQuery .=",atcq.id";  
	   $strQuery .=",atcq.descripcion "; 
	   $strQuery .=",to_char(atcq.fecha_creacion,'dd/mm/yyyy') as fecha_creacion "; 
	   $strQuery .=",to_char(atcq.fecha_actualizacion,'dd/mm/yyyy') as fecha_actualizacion "; 
	   $strQuery .=",CONCAT(m.nombre,' ', m.apellido) AS nombre ";
	   $strQuery .=",e.descripcion as especialidad ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.antecedentes_quirurgicos as atcq ";	
	   $strQuery .="  join historial_clinico.consultas  as hc on atcq.id_consulta=hc.id";
	   $strQuery .="  join  historial_clinico.medicos as m on hc.id_medico=m.id";
	   $strQuery .="  join historial_clinico.especialidades as e on m.especialidad=e.id_especialidad ";
	   $strQuery  =$strQuery . " where atcq.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}

	public function buscar_consulta($id_consulta)
	{
	   //$builder = $this->dbconn('historial_clinico.consultas as hc');
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" atcq.id";  
	   $strQuery .=",atcq.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .=" historial_clinico.antecedentes_quirurgicos as atcq  ";	
	   $strQuery  =$strQuery . " where atcq.id_consulta='$id_consulta'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	   //return  $strQuery;
	}
	 
	public function actualizar_antecedentes_quirurgicos($data)
	{
		$builder = $this->dbconn('historial_clinico.antecedentes_quirurgicos');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}


}

