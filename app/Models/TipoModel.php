<?php namespace App\Models;
use CodeIgniter\Model;
class TipoModel extends BaseModel
{
    protected $table = 'grupo_usuario';
    protected $table2 ='usuarios';
    
    protected $allowedFields = [ 'id','nivel_usuario'];
    public function getTipo($slug = false)
    {
        if ($slug === false) 


        {
            // return $this->where(['borrado'=>false])->findAll(); 

            return $this->findAll(); 
        }
        return $this->where(['id'=>$slug])->first();
        
    }

    public function savegrupo2($datos)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('grupo_usuario');
       
        $data = 
        [
        
         'nivel_usuario'=>$datos['nivel_usuario'],
         'id'=>$datos['id'],
         'borrado'=>$datos['borrado'],
         
        ];
        $builder->where('id',$datos['id']);
        $builder->update($data);
             
    }

  




}
