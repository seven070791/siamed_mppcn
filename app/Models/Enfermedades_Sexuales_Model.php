<?php namespace App\Models;
use CodeIgniter\Model;
class Enfermedades_Sexuales_Model extends BaseModel
{
	
	 public function ListarEnfermedadesSexuales()
     {
	  $builder = $this->dbconn('historial_clinico.enfermedades_sexuales as es');
	  $builder->select
	  (
		" es.id
		,es.descripcion"
	  );
	  $builder->where(['es.borrado'=>false]);
	  $query = $builder->get();
	  return $query;	
     }
	

	}

	