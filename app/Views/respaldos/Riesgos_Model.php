<?php namespace App\Models;
use CodeIgniter\Model;
class Riesgos_Model extends BaseModel
{
	
	public function agregar($data)
{
    $builder = $this->dbconn('historial_clinico.riesgos_historial');
    $query = $builder->insert($data);
    return $query ? true : false;

}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.abortos');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}
	public function Listar_riesgos()
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id";  
	   $strQuery .=",riesgos.descripcion "; 
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos ";	
	  // return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	  return $resultado;
	}

	public function listar_riesgos_Historial($numeroHistorial=null)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id_riesgo ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos_historial as riesgos ";	
	   $strQuery .="  where riesgos.n_historial='$numeroHistorial' ";
	   $strQuery .="  and riesgos.borrado='false'";	
	   //return  $strQuery;
	  $query = $db->query($strQuery);
	  $resultado=$query->getResult(); 
	 return $resultado;
	}

	public function buscar_riesgos_existentes($datos=null)
	{
 
		$n_historial = $datos['n_historial'];
		$id_riesgo = $datos['id_riesgo'];
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" riesgos.id_riesgo ";  
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.riesgos_historial as riesgos ";	
	   $strQuery .="  where riesgos.n_historial='$n_historial' ";	
	   $strQuery .="  and riesgos.id_riesgo=$id_riesgo ";	
	   $strQuery .="  and riesgos.borrado='false'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	 return $resultado;
	}

	public function actualizar_riesgos_existentes($check_borrados,$n_historia)
	{
		
		
		$builder = $this->dbconn(' historial_clinico.riesgos_historial');
		foreach ($check_borrados as $Row) {
			$builder->where('n_historial', $n_historia);
			$builder->where('id_riesgo', $Row);
			$query = $builder->update([
				'borrado' => true,
			]);
		}

		return $query;
	}
}

	