/*
 *Este es el document ready
 */
 $(function()
 {
  
     //$( "#fecha" ).datepicker({dateFormat:'dd/mm/yy',changeMonth:true, changeYear:true});	
     //$( "#fecha1" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth:true, changeYear:true});	
     listarentradas();
     //alert($('#id_medicamento').val());
 });
 /*
  * Función para definir datatable:
  */
 function listarentradas()
 {
      $('#table_entradas').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_entradas/"+$('#id_medicamento').val(),
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'fecha_entrada'},
                {data:'fecha_vencimiento'},                
                {data:'entradas'},     
                {data:'salidas'},
                {data:'stock'},
                {orderable: true,
                    render:function(data, type, row)
                    {
                     return '<a href="javascript:;" class="btn btn-xs reversar" style=" font-size:1px" data-toggle="tooltip" title="Reversar" id='+row.id+' fecha_entrada="'+row.fecha_entrada+'" fecha_vencimiento='+row.fecha_vencimiento+' entradas='+row.entradas+'> <i class="material-icons " >autorenew</i></a>'
                       }
                   }
               
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();
      $('#modal').find('#activo').hide();
      $("#modal").modal("show");
      $('#fecha1').val('');
      $('.modal_reversar').show();
      $('.observacion').hide();
      $('#cantidad').val('');

 });
 



 $(document).on('click','#btnGuardar', function(e)
 {
     e.preventDefault();
     //var accion='REALIZO UNA ENTRADA'
     var id_medicamento=$('#id_medicamento').val();
     var fecha_entrada=$('#fecha').val();
     var fecha_vencimiento=$('#fecha1').val();
     var cantidad=$('#cantidad').val();

  


     if (fecha_vencimiento=='') {
          alert('DEBE INGRESAR LA FECHA DE VENCIMIENTO');  
     }

     else if ( cantidad==''  ) {
        alert('DEBE INGRESAR UNA CANTIDAD');  
     }else
     {

    
     var url='/agregar_entrada';
     var ruta_regreso='/vistaEntradas/'+id_medicamento;
     var data=
    {
         id_medicamento    :id_medicamento,
         fecha_entrada     :fecha_entrada,
         fecha_vencimiento :fecha_vencimiento,
         cantidad          :cantidad,
       //  accion          :accion,
    }
        $.ajax
     ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
             
         // alert(data);
              if(data=='1')
              {
                   alert('Registro Incorporado ');
               
                   
                   window.location = ruta_regreso;
              }
              else
              {
                   alert('Error en la Incorporación del Registro');
              }
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}
});



 $(document).on('click','#btnRegresar ', function(e)
 {
     e.preventDefault();
     window.location = '/vistamedicamentos/';
     
 });

 $('#lista_medicamento').on('click','.reversar', function(e)
 {
     var id_entrada   =$(this).attr('id');
     var fecha_vencimiento   =$(this).attr('fecha_vencimiento');
     var fecha_entrada   =$(this).attr('fecha_entrada');
     var cantidad   =$(this).attr('entradas');
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#borrado').show();
     $('#modal').find('#activo').show();
     $("#modal").modal("show");
     $('#id_entrada').val(id_entrada);
     $('#fecha').val(fecha_entrada);
     $('#fecha1').val(fecha_vencimiento);
     $('#cantidad').val(cantidad);

     $('.modal_reversar').hide();
     $('.observacion').show();
     
   

     var borrado='f';

     if(borrado=='f')
     {
          $('#borrado').removeAttr('checked')
     }
    else if(borrado=='t')
     {
          $('#borrado').attr('checked','checked');
     }


     var borrado='f';

     if(borrado=='f')
     {
          $('#borrado').removeAttr('checked')
     }
    else if(borrado=='t')
     {
          $('#borrado').attr('checked','checked');
     }
 

 });



 $(document).on('click','#borrado ', function(e)
 {
    
     $("#btnActualizar").prop('disabled', false);
     
 });
// ************************METODO PARA ACTULIZAR************************
$(document).on('click','#btnActualizar ', function(e)
{
     e.preventDefault();
     var fecha_entrada=$('#fecha').val();
     var fecha_vencimiento=$('#fecha1').val();
     var cantidad=$('#cantidad').val();
     var id_entrada =$('#id_entrada ').val();
     var id_medicamento =$('#id_medicamento').val();
     var estatus ='false';

     if($('#borrado').is(':checked'))
     {
          estatus='true';
     }
     else
     {
          estatus='false';
     }
   
    var data=
    {
         id_entrada         :id_entrada,
         estatus           :estatus,   
         id_medicamento    :id_medicamento,
         fecha_entrada     :fecha_entrada,
         fecha_vencimiento :fecha_vencimiento,
         cantidad          :cantidad,
  
    }
    //alert(estatus);
     // console.log(data);
   var url='/actualizar_entrada';
   $.ajax
   ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
              //alert('Procesando Información ...');
         },
         success:function(data)
         {
          //console.log(data);
            //alert(data);
            if(data===1)
            {
              alert('Registro Actualizado');
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/vistaEntradas/'+id_medicamento;             
         },
         error:function(xhr, status, errorThrown)
         {
            alert(xhr.status);
            alert(errorThrown);
         }
    });
    
});





 