<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Plan_Model;
use App\Models\Patologias_Model;
use CodeIgniter\RESTful\ResourceController;

class Patologias_Controler extends BaseController
{
	use ResponseTrait;

	public function agregar_Patologias()
	{

		$model = new Patologias_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$nHistorials = $data->n_historials;
		$n_historia= $data->n_historia;
		$check_borrados=[];
		$check_borrados=$data->valores_borrados;
		$selectedValues = $data->selectedValues;

		if (empty($selectedValues)) 
		{
			$query_actualizar_patologias_existentes=$model->actualizar_patologias_existentes($check_borrados,$n_historia);		
			$mensaje = 1;
			return json_encode($mensaje);
		}else
		{
			// Inicialice una matriz vacía para almacenar los resultados de cada inserción
			$results = [];
			// Recorra el arreglo nHistorials e inserte una nueva fila para cada uno
			foreach ($nHistorials as $index => $nHistorial)
			{
				$datos = array(
					'n_historial' => $nHistorial,
					'id_patologia' => $selectedValues[$index]
				);
				// BUSCAMOS SI YA EXISTE ESA PATOLOGIA EN EL HISTORIAL ACTUAL
				//$query_buscar_patologias_existentes = $model->buscar_patologias_existentes($datos);
				$query_patologias_existentes = $model->patologias_existentes($datos);
				// SI , LA PATOLOGIA NO EXISTE LA AGREGAMOS 
				if (empty($query_patologias_existentes)) {
					$results[] = $model->agregar($datos);
				}else
				{
					// VERIFICAMOS SI HAY PATOLOGIAS BORRADAS
					if (!empty($check_borrados)) 
					{
						$query_actualizar_patologias_existentes=$model->actualizar_patologias_existentes($check_borrados,$n_historia);	
					}

				}
				
			}
			// Si todas las inserciones fueron exitosas, establezca el mensaje en 1
			if (count(array_filter($results)) === count($results)) {
				$mensaje = 1;
			} else {
				// Si alguna inserción falló, establezca el mensaje en 0
				$mensaje = 0;
			}
		}
		return json_encode($mensaje);
	
	
	
	}


	public function Listar_patologias()
    {
        $modelo_Patologias = new  Patologias_Model();
        $query_patologias = $modelo_Patologias->listar_patologias();

        if (empty($query_patologias)) {
            $patologias = [];
        } else {
            $patologias = $query_patologias;
        }
        echo json_encode($patologias);
    }


	public function listar_patologias_Historial($numeroHistorial=null)
    {
        $modelo_Patologias = new  Patologias_Model();
        $query_patologias = $modelo_Patologias->listar_patologias_Historial($numeroHistorial);

        if (empty($query_patologias)) {
            $patologias = [];
        } else {
            $patologias = $query_patologias;
        }
        echo json_encode($patologias);
    }
	


}
