<link rel="stylesheet" href="<?php echo base_url(); ?>/css/menusecretaria.css">
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu2">
  <li class="nav-item ">
    <a href="../supermenu" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
      <p> Secretaria</p>
    </a>
  </li>

  <li class="nav-item">
    
    
  </li>

  <li class="nav-item">
    <a href="<?php echo base_url(); ?>/titulares" class="nav-link"><i class="nav-icon fas fa-copy" style='font-size:20px'></i>
      <p> Beneficiarios </p>

    </a>
    <li class="nav-item">
              <a href="<?php echo base_url(); ?>/reporte_citas" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Consultas / Citas</p>
              </a>
    </li>

  </li>


    </ul>
</ul>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<input type="hidden" id="master" disabled="disabled" value='<?= session('master'); ?>' />
<script>
  $(document).ready(function() {
    $('.usuarios').prop('disabled', true);
    $('.auditoria').prop('disabled', true);
    $('.reportes').prop('disabled', true);
    $('.inventario').prop('disabled', true);
    $('.historial').prop('disabled', true);

  });
</script>

<!-- background-color: rgba(00,00,255,0.5); -->