<!-- jQuery -->
<script src="<?=base_url();?>/js/jquery.js"></script>
<!-- jQuery-UI -->
<script src="<?=base_url();?>/js/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?=base_url();?>/js/bootstrap4.3.1.min.js"></script>
<script src="<?php  echo base_url();?>/plugins/moment/moment.min.js"></script>
<!-- Date Picker -->
<script src="<?php echo base_url();?>/js/fecha_datapicker.js"></script>
<!-- DataTables -->
<script src="<?=base_url();?>/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url();?>/js/dataTables.bootstrap4.min.js"></script>
<!-- Bootstrap Responsive -->
<script src="<?=base_url();?>/js/responsive.bootstrap4.min.js"></script>
<!-- Para el Módulo -->
<script type="text/javascript" src="<?php echo base_url();?>/js/notas_entregas/notas_entregas.js"></script>
<script src="<?php echo base_url(); ?>/datatable_responsive/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo base_url(); ?>/datatable_responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>/datatable_responsive/js/responsive.bootstrap.min.js"></script>
<!-- ******ESTO ES PARA INSERTAR LA IMAGEN EN EL PDF,******* -->
<?php
    $path = ROOTPATH.'public/img/cintillo2022.jpg';//this is the image path
    $type = pathinfo($path, PATHINFO_EXTENSION);
    $data = file_get_contents($path);
    $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
?>
<script src="<?=base_url();?>/js/cdn/buttons.html5.styles.min.js"></script>
<script src="<?=base_url();?>/js/cdn/buttons.html5.min.js"></script>
<script src="<?=base_url();?>/js/cdn/buttons.print.min.js"></script>
<script src="<?=base_url();?>/js/cdn/dataTables.buttons.min.js"></script>
<script>
    var rootpath='<?php echo($base64);?>'  
</script>
</body>
</html>


