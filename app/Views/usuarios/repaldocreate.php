 <!-- **Boton para Regresar**** -->
 <form action="/User_controllers/create" method="post">
              <?= session()->getFlashdata('error') ?>
              <?= service('validation')->listErrors() ?>
              <?= csrf_field() ?>
              <h3> <p class="login-box-msg"><i class="fa fa-user icon-title"></i>Registro de  usuario</p></h3>   
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="cedula" name="cedula" style="width: 400px;" placeholder=" Cedula: 99999999" autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" />
                  <!-- <span class="input-label">Cedula:</span> -->
                  <i class="		fas fa-shield-alt"></i>   
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="nombre" name="nombre" style="width: 400px;" placeholder=" Nombre:" required pattern="[aA-Za-z]+" autocomplete="off"  />
                  <!-- <span class="input-label">Nombre</span> -->
                  <i class="	fas fa-portrait"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="apellido" name="apellido"style="width: 400px;"placeholder=" Apellido:" required pattern="[aA-Za-z]+" autocomplete="off" />
                  <!-- <span class="input-label">Apellido</span> -->
                  <i class="far fa-id-badge"></i>   
                  </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="username" name="usuario"style="width: 400px;" placeholder=" Usuario:" required pattern="[aA-Za-z0-9]+"autocomplete="off" />
                  <!-- <span class="input-label">Username</span> -->
                  <i class="	fas fa-user-circle"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="text" id="password" name="password"style="width: 400px;" placeholder=" Clave:" required autocomplete="off" />
                  <!-- <span class="input-label">Password</span> -->
                  <i class="	fas fa-lock"></i>   
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                  <input type="email" id="correo" name="correo"style="width: 400px;" pattern=".+@sapi.gob\.ve" placeholder=" Correo Electronico: aaaaa@sapi.gob.ve"autocomplete="off" required  />
                  <!-- <span class="input-label">Correo Electronico</span> -->
                  <i class="fas fa-envelope"></i>   
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                <label for="nombre0" class="control-label">Nivel de Usuario</label>
                <select class="form-control tipousuario " name="tipousuario" id="tipousuario" style="width: 279px;"required>
                  <?php foreach($tipos as $tipo): ?>
                    <option value="<?= $tipo['id']; ?>"><?= $tipo['nivel_usuario']; ?></option>
                  <?php endforeach; ?>   
              </div>
              <H3><span class="input-label ">Nivel de Usuario</span></H3> 
              </select>
              </div>
        
              &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              <input type="submit" value="Registrar" class="btn-login" />
              </form>