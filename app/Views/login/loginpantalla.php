<title>SIAMED</title>
<link rel="icon" type="image/png" href="<?= base_url() ?>/img/siamedcorazon.jpg" style="width: 50px; height: 50px;">
<link href="/css/login2.css" rel="stylesheet" type="text/css" />



<body>
  <form action="<?php echo base_url(); ?>/Login/validarIngreso" method="post">
    <section class="form-login">
      <img class="img_center" src="<?php echo base_url(); ?>/img/LOGOCOMERCIO.png" style="width: 260px; max-height: 5rem;" alt="Avatar">
      <h3>Inicie sesion para usar la aplicacion</h3>
      <input class="controls" type="text" name="usuario" value="" placeholder="Usuario" autocomplete="off" required>
      <input class="controls" type="password" name="contrasena" value="" placeholder="Contraseña" autocomplete="off" required>
      <input class="buttons" type="submit" name="" value="Ingresar">
      <p></p>
    </section>
  </form>

  <div class="row">
    <div class="col-12" style="text-align: center;">
      <br>
      <?php if (isset($mensaje)) { ?>
        <div class="alert alert-<?= $tipo; ?> ">
          <?= $mensaje; ?>
        </div>
      <?php } ?>
    </div>
  </div>

  <script src="<?php echo base_url(); ?>/plugins/jQuery/jQuery-2.1.3.min.js"></script>
  <script src="<?php echo base_url(); ?>/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>