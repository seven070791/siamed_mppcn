<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- Required meta tags -->
<!-- Required meta tags -->


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="/css/vista_consultas_citas.css" rel="stylesheet" type="text/css" />

<input type="hidden" id="rol" value="<?php echo session('nivel_usuario');?>">
<input type="hidden" id="id_user" value="<?php echo session('id_user');?>">
<input type="hidden" id="cedula_usuario" value="<?php echo session('cedula_usuario');?>">
<input type="hidden" id="id_medico" >
<input type="hidden" id="idespecialidad" >




<div class="container">
	<div class="row ">
		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
		</div>
		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
			<h3 class="center">Consultas /Citas </h3>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xl-3">
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
			<label for="min">Desde</label>&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
			<label for="hasta">Hasta</label>&nbsp;&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;
			&nbsp;&nbsp;
			<label class="labelcita" class="control-label">Control &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="control" autocomplete="off" required>
				<option value="0">Seleccione</option>
				<option value="1">Cita</option>
				<option value="2">Consulta</option>
			</select>&nbsp;&nbsp;

			<label class="labelasitio" class="control-label">Asistío &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="asistio" autocomplete="off" required>
				<option value="0">Seleccione</option>
				<option value="1">Si</option>
				<option value="2">No</option>
				<option value="3">En espera</option>
			</select>


		</div>
	</div>
	<br>

	<div class="row">
		<div class="col-md-10 col-sm-10 col-lg-10 col-xl-10">
			<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:250px;" id="especialidad" autocomplete="off" required>
				<option value="Seleccione">Seleccione</option>
				</h5>
			</select>&nbsp;&nbsp;
			<label class="labelmedicotratante t_medico" class="control-label">Médico &nbsp;&nbsp; </label>
			<select class="custom-select vm t_medico" style="width:200px;" id="medico" autocomplete="off" required>
				<option value="0">Seleccione</option>
			</select>

			<label class="labelmedicotratante t_personal" style="display: none;" class="control-label">Personal &nbsp;&nbsp; </label>
			<select class="custom-select vm t_personal" style="width:200px; display: none;" id="personal" >
				<option value="0">Seleccione</option>
			</select>
			&nbsp;&nbsp;
			<button id="btnfiltrar" style="visibility:visible;" class="btn btn-primary btn-sm">Filtrar </button>&nbsp;
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>&nbsp;
			<!-- <button id="btnfiltrocortesia" class="btn btn-outline-dark btn-sm">Filtrar por Entes</button>&nbsp; -->
			<button id="filtrocortesia" style="visibility:hidden;" class="btn btn-primary btn-sm">Filtrar</button>&nbsp;
		</div>


		<div class="col-md-3 col-sm-3 col-lg-3 col-xl-3">
			
		</div>
	</div>




	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

<div class="row">
	<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
	

		<table class="display" id="table_consulta_citas" style="width:100%">


			<thead>
				<tr>
				    <td class="text-center" style="width: 20px;">Atendido</td>
					<td class="text-center" style="width: 50px;">Control</td>
					<td class="text-center" style="width: 80px;">Nº Historial</td>
					<td class="text-center" style="width: 290px;">Beneficiario</td>
					<td class="text-center" style="width: 220px;">Medico</td>
					<td class="text-center" style="width: 220px;">Especialidad</td>
					<td class="text-center" style="width: 100px;">Fecha </td>
					<td class="text-center" style="width: 7%;">Asistio</td>
					<td class="text-center" style="width: 30px;">Control</td>

				</tr>
			</thead>
			<tbody id="consulta_citas">
			</tbody>
		</table>
	</div>

</div>






<!-- Ventana Modal -->
<input type="hidden" id="tipo_beneficiario" disabled="disabled" value='' />
<div class=" modal fade" id="modal_consultas_citas" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<h5 class="modal-title">Informacion  </h5>
			<div class="modal-header">


				<div class="row ">

					<div class="col-md-12">


	                 <input type="hidden" id="id_consulta">


						<label class="labelfecha">Fecha de Asistencia</label>
						<input type="text" class="fecha" style="width:100px;" autocomplete="off" name="fecha" id="fecha_del_dia" style=" z-index: 1050 !important;">
						<label class="labeldepartamento">Nº Historial</label>
						<input type="text" id="n_historial" disabled="disabled" style="width:300px;" value=''>	

						<div class="form-group has-feedback" id="cajas1">
							<input type="hidden" id="apellido" disabled="disabled" value='' />
							<input type="hidden" id="nombre" disabled="disabled" value='' />
							<br>
							<fieldset>
								<legend class="legend"><b>Datos del Beneficiario</b></legend>

								
								<div>
									<label class="labelnombreApellido">Nombre y Apellido</label>
									<input type="text" id="nombrepellido" disabled="disabled" style="width:300px;" value=''>
									<label class="labelCedula">Cedula</label>
									<input type="text" id="cedula" disabled="disabled" style="width:100px;" value=''>
									<label class="labelCedula">Edad</label>
									<input type="text" id="edad" disabled="disabled" style="width:100px;" value=''>
									
								</div>
								<div>
									<label class="labeldepartamento">Departamento</label>
									<input type="text" id="departamento" disabled="disabled" style="width:330px;" value=''>	

									<label class="labeldepartamento">Tipo de Beneficiario</label>
									<input type="text" id="tipo_beneficiario" disabled="disabled" style="width:200px;" value=''>	

								</div>
							</fieldset>

							<br>


							&nbsp;&nbsp;<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span> &nbsp;&nbsp;
							<input type="text" disabled id="motivo_consulta" onkeyup="mayus(this);"   autocomplete="off">
							<br>
							<br>
							<div>
								<fieldset>
									<legend class="legend"><b>Signos Vitales</b></legend>
									<div>
										<label class="labelpeso">PESO:</label>
										<input type="number" id="peso" class="bodersueve" min="0" value="0" name="peso" style="width:50px;">
										<label class="labelkg">(KG)</label>&nbsp;&nbsp;
										<label class="labeltalla">TALLA:</label>
										<input type="number" id="talla" class="bodersueve" min="0" value="0" name="talla" style="width:50px;">
										<label class="labelcm">(CM)</label>&nbsp;&nbsp;
										<label class="labelspo2">SPO2:</label>
										<input type="number" id="spo2" class="bodersueve" min="0" value="0" name="spo2" style="width:50px;">&nbsp;&nbsp;
										<label class="labelfc">FC:</label>
										<input type="number" id="frecuencia_c" class="bodersueve" min="0" value="0" name="fc" style="width:50px;">
										<label class="labelfr">(X) &nbsp;&nbsp;FR:</label>&nbsp;&nbsp;
										<input type="number" id="frecuencia_r" class="bodersueve" min="0" value="0" name="fr" style="width:50px;">
										<label class="labelx">(X)</label>


										<label class="labeltemperatura">TEMPERATURA:</label>
										<input type="number" id="temperatura" class="bodersueve" min="0" value="0" name="temperatura" style="width:50px;">&nbsp;&nbsp;
										<label class="labelc">(ºC)</label>&nbsp;&nbsp;
										<label class="labelta">T/A:</label>
										<input type="number" id="ta_alta" class="bodersueve" min="0" placeholder="t.a" name="ta" style="width:50px;">
										<input type="number" id="ta_baja" class="bodersueve" min="0" placeholder="t.b" name="ta" style="width:50px;">
										<label class="labelmmhg">(MMHg)</label>
										<label class="labeltipodesangre">Imc</label>
										<input type="number" class="bodersueve" id="imc" class="bodersueve" min="0" value="0" style="width:50px;">
								</fieldset>
								<div class="modal-footer">
									<button id="btnActualizar" class="btn-primary btn-sm">&nbsp;&nbsp;Actualizar Signos</button>&nbsp;&nbsp;
									<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" name="btnCerrar">Cerrar</button>
								</div>
							 </div>

							 <!-- <fieldset>
									<legend class="legend"><b> Tratamiento &nbsp;&nbsp;<button id="btnagregar_tratamientos" class="btn-primary btn-sm">&nbsp;&nbsp;Agregar</button>&nbsp;&nbsp;</b></legend>
									
									
									<div class="row">
									<br>
										<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
											<table ALIGN="right" class="display" id="table_traramiento" cellspacing="2" style="width:100%">
												<thead>
													<tr>
														<td style="width:25%">Descripcion</td>
														<td style="width:25%">Nombre</td>
														<td style="width:25%">Fecha_actualizacion</td>
														<td style="width:5%">Acciones</td>
													</tr>
												</thead>
												<tbody id="lista_traramiento">
												</tbody>
											</table>
										</div>
									</div>
									
							</fieldset> -->

							<br>
<!-- 
							<div style="text-align: right;">
								
							</div> -->





							</div>

							</div>
							</div>
							</div>
							</div>
							</div>
</div>



<!-- Ventana Modal  Agregar tratamientos-->
<div class=" modal fade" id="modal_tratamientos" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<h5 class="modal-title">Informacion  </h5>
			<div class="modal-header">
				<div class="row ">
					<div class="col-md-12">
							<div>
							<fieldset>
								<legend class="legend"><b></b></legend>

								
								<div>
									<label class="labelnombreApellido">Descripcion</label>
									<input type="text"  id=""  style="width:300px;" value=''>
									<textarea name="" onkeyup="mayus(this);" class="bodersueve" id="enfermedad_actual"></textarea>								
								</div>
								
							</fieldset>
								
							</div>
							<br>
							<div style="text-align: right;">
							<button id="btnguargar_tratamientos" class="btn-primary btn-sm">&nbsp;&nbsp;Guardar</button>&nbsp;&nbsp;
							<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" name="btnCerrar">Cerrar</button>
							</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- Ventana Modal  Editar tratamientos-->
<div class=" modal fade" id="edit_modal_tratamientos" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<h5 class="modal-title">Informacion  </h5>
			<div class="modal-header">
				<div class="row ">
					<div class="col-md-12">
							<div>
							<fieldset>
								<legend class="legend"><b></b></legend>

								
								<div>
									<label class="labelnombreApellido">Descripcion</label>
									<input type="text"  id=""  style="width:300px;" value=''>
									<input type="hidden"  id="id_tratamiento"  style="width:300px;" value=''>
									<textarea name="" onkeyup="mayus(this);" class="bodersueve" id="edit_enfermedad_actual"></textarea>								
								</div>
								
							</fieldset>
								
							</div>
							<br>
							<div style="text-align: right;">
							<button id="edit_btnguargar_tratamientos" class="btn-primary btn-sm">&nbsp;&nbsp;Actualizar</button>&nbsp;&nbsp;
							<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" name="btnCerrar">Cerrar</button>
							</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>

<style type="text/css">
	option {
		font-family: verdana, arial, helvetica, sans-serif;
		font-size: 14px;
		border: 3;
		border-radius: 20%;
	}

	option {
		border-color: blueviolet;
	}
</style>


</body>

</html>




<script>
	var botones = document.querySelectorAll('.btn-expandir');
	var texto_expandir = document.querySelectorAll('.texto_expandir');
	botones.forEach((elemento, clave) => {
		elemento.addEventListener('click', () => {
			texto_expandir[clave].classList.toggle("abrir_cerrar")
		});

	});
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
 -->





<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>



<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		// var today= (day)+"-"+(month)+"-"+now.getFullYear();
		// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		$("#fecha_del_dia").val(today);
	});
</script>
<?= $this->endSection(); ?>