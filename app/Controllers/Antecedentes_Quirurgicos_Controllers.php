<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Antecedentes_Quirurgicos_Model;
use CodeIgniter\RESTful\ResourceController;

class Antecedentes_Quirurgicos_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_antecedentes_quirurgicos()
	{
		$model = new Antecedentes_Quirurgicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->antecedentes_quirurgicos;
		$datos['id_consulta']   = $data->id_consulta;
		$query2 = $model->buscar_consulta($datos['id_consulta']);
		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}
		return json_encode($mensaje);
	}

	public function listar_antecedentesquirurgicos($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Antecedentes_Quirurgicos_Model();

		$query = $model->listar_antecedentes_quirurgicos($n_historial);
		if (empty($query)) {
			$antecedentes_quirurgicos = [];
		} else {
			$antecedentes_quirurgicos = $query;
		}
		echo json_encode($antecedentes_quirurgicos);
	}


	public function actualizar_antecedentes_quirurgicos()
	{
		$model = new Antecedentes_Quirurgicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->antecedentes_quirurgicos;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_antecedentes_quirurgicos($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
