<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Paraclinicos_Model;
use CodeIgniter\RESTful\ResourceController;

class Paraclinicos_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_paraclinicos()
	{
		$model = new Paraclinicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->paraclinicos;
		$datos['id_consulta']   = $data->id_consulta;

		$query2 = $model->buscar_consulta($datos['id_consulta']);
		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar_paraclinicos($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}

		return json_encode($mensaje);
	}



	public function listar_paraclinicos($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Paraclinicos_Model();

		$query = $model->listar_paraclinicos($n_historial);
		if (empty($query)) {
			$examen_fisico = [];
		} else {
			$examen_fisico = $query;
		}
		echo json_encode($examen_fisico);
	}



	public function actualizar_paraclinicos()
	{
		$model = new Paraclinicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->paraclinicos;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_paraclinicos($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
