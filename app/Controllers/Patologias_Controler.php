<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Plan_Model;
use App\Models\Patologias_Model;
use CodeIgniter\RESTful\ResourceController;
use App\Models\Auditoria_sistema_Model;
class Patologias_Controler extends BaseController
{
	use ResponseTrait;
	public function patologias_personales()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/patologias_personales/patologias.php');
		echo view('/patologias_personales/footer_patologias.php');
	}

/*
      * METODO QUE INSETAR UNA NUEVA PATOLOGIA EN LA TABLA PRINCIPAL PATOLOGIAS
 */
	public function agregar_patologia_master()
	{
		$model = new Patologias_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->descripcion;
		$query = $model->agregar_patologia_master($datos);
		if (isset($query)) {

			$mensaje = 1;
            $auditoria['accion'] = 'REGISTRÓ LA PATOLOGÍA DE   '.' '.$datos['descripcion'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

/*
      * METODO QUE ACTUALIZA LA PATOLOGIA EN LA TABLA PRINCIPAL PATOLOGIAS
 */
	public function actualizar_patologias_master()
	{
		$model = new Patologias_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_patologia;
		$datos['descripcion']   = $data->descripcion;
		$datos['borrado'] = $data->borrado;
		$auditoria['accion'] = strtoupper($data->datos_modificados);
		$query = $model->actualizar_patologias_master($datos);
		if (isset($query)) {
			$mensaje = 1;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
/*
      * METODO QUE AGREGA LAS PATOLOGIAS EN FUNCION DE LA ESPECIALIDAD 
 */
	public function agregar_Patologias()
	{

		$model = new Patologias_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$nHistorials = $data->n_historials;
		$n_historia= $data->n_historia;
		$check_borrados=[];
		$check_borrados=$data->valores_borrados;
		$selectedValues = $data->selectedValues;
		/// SI NO SELECCIONO NINGUNO , ENTONCES ELIMINAMOS LOS REGISTROS DE LA TABLA HISTORIAL_PATOLOGIAS
		if (empty($selectedValues)) 
		{
			//VERIFICO SI TIENE PATOLOGIAS ASOCIADAS
			$query_Patologias_Asociadas=$model->listar_patologias_Historial($n_historia);		
			if (empty($query_Patologias_Asociadas)) {
				$mensaje = 2;
				
			}else
			{
				$query_actualizar_patologias_existentes=$model->Eliminar_patologias_existentes($check_borrados,$n_historia);		
				$mensaje = 1;
			}
			return json_encode($mensaje);
		}else
		{
		  // SI HAY REGISTROS SELECCIONADOS ENTONCES RECORREMOS ($selectedValues) 
			// Inicialice una matriz vacía para almacenar los resultados de cada inserción
			$results = [];
			// Recorra el arreglo nHistorials e inserte una nueva fila para cada uno
			foreach ($nHistorials as $index => $nHistorial)
			{
				$datos = array(
					'n_historial' => $nHistorial,
					'id_patologia' => $selectedValues[$index]
				);
				 // BUSCAMOS SI YA EXISTE ESA PATOLOGIA EN EL HISTORIAL ACTUAL
				$query_patologias_existentes = $model->patologias_existentes($datos);
				 // SI ,NO HAY UN REGISTRO PARA ESA HISTORIA Y ESA PATOLOGIA , AGREGAMOS EL INSERT 
				 if (empty($query_patologias_existentes)) 
				{	
					if (!empty($check_borrados)) 
					{
						$query_actualizar_patologias_existentes=$model->Eliminar_patologias_existentes($check_borrados,$n_historia);	
					}
					// AGREGAMOS EL INSERT 
					$results[] = $model->agregar($datos);
				}
				else 

				{					
				// BUSCAMOS LAS PATOLOGIAS CON SU ESTATUS DE BORRADO
					foreach ($query_patologias_existentes as $existentes)
					 {
						$patologias['n_historial']     =   $existentes->n_historial;
						$patologias['id_patologia']    = $existentes->id_patologia;
                        $patologias['borrado']         = $existentes->borrado;
					
					 }
				// VERIFICAMOS SI HAY PATOLOGIAS BORRADAS
					if ($patologias['borrado']=='t') 
				 	{
						$query_actualizar_patologias_existentes=$model->Activar_patologias_existentes($patologias);
					}
					if (!empty($check_borrados)) 
					{
						$query_actualizar_patologias_existentes=$model->Eliminar_patologias_existentes($check_borrados,$n_historia);	
					}
				 }
			}

			$mensaje = 1;
			// // Si todas las inserciones fueron exitosas, establezca el mensaje en 1
			// if (count(array_filter($results)) === count($results)) {
			// 	$mensaje = 1;
			// } else {
			// 	// Si alguna inserción falló, establezca el mensaje en 0
			// 	$mensaje = 0;
			// }
		}
		return json_encode($mensaje);
	}

	public function Listar_patologias()
    {
        $modelo_Patologias = new  Patologias_Model();
        $query_patologias = $modelo_Patologias->listar_patologias();

        if (empty($query_patologias)) {
            $patologias = [];
        } else {
            $patologias = $query_patologias;
        }
        echo json_encode($patologias);
    }

	public function Listar_patologias_activas()
    {
        $modelo_Patologias = new  Patologias_Model();
        $query_patologias = $modelo_Patologias->Listar_patologias_activas();

        if (empty($query_patologias)) {
            $patologias = [];
        } else {
            $patologias = $query_patologias;
        }
        echo json_encode($patologias);
    }
	public function listar_patologias_Historial($numeroHistorial=null)
    {
        $modelo_Patologias = new  Patologias_Model();
        $query_patologias = $modelo_Patologias->listar_patologias_Historial($numeroHistorial);

        if (empty($query_patologias)) {
            $patologias = [];
        } else {
            $patologias = $query_patologias;
        }
        echo json_encode($patologias);
    }
	


}
