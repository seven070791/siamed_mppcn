<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Control_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Control_Controllers extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	public function vistacontrol()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarControl/content_C');
		echo view('/listarControl/footer_C');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Control_model();
		$query = $model->getAll();
		if (empty($query->getResult())) {
			$control = [];
		} else {
			$control = $query->getResultArray();
		}
		echo json_encode($control);
	}
	public function getAllActivos()
	{
		$model = new Control_model();
		$query = $model->getAllActivos();

		if (empty($query->getResult())) {
			$control = [];
		} else {
			$control = $query->getResultArray();
		}
		echo json_encode($control);
	}
	/*
      * Método que guarda el registro nuevo
      */
	//public function save()
	public function agregar()
	{
		$model = new Control_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->control;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$query = $model->agregar($datos);

		//echo($query);
		if (isset($query)) {
				$mensaje = 1;
				$auditoria['accion'] = 'REGISTRÓ EL TIPO DE CONTROL   '.' '.$datos['descripcion'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	/*
      * Función para obtener los datos de un Rol
      */
	public function getDatosRol()
	{
		if ($this->request->isAJAX()) {
			$data = json_decode(base64_decode($this->request->getGet('data')));
			$datos['id'] = $data->aide;
			$modelo = new Control_model();
			$query = $modelo->getDatosRol($datos['id']);
			$respuesta = [];
			if (empty($query->getResult())) {
				$respuesta[] = '0';
			} else {
				foreach ($query->getResult() as $fila) {
					$respuesta['id']      = $fila->id;
					$respuesta['rol']     = $fila->rol;
					$respuesta['activo']  = $fila->activo;
				}
			}
		} else {
			redirect()->to('/403');
		}
		return json_encode($respuesta);
	}
	/*
      * Método que actualiza el registro
      */
	public function actualizar()
	{
		$modelo = new Control_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']            = $data->id;
		$datos['descripcion']   = $data->control;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$datos['borrado']       = $data->borrado;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_descr_anterior['decripcion_anterior']= $data->descripcion_anterior;
		$query = $modelo->actualizar($datos);
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL TIPO DE CONTROL '.' '.$datos_descr_anterior['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
