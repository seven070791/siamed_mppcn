<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Antecendentes_Patologicosf_Model;
use CodeIgniter\RESTful\ResourceController;

class Antecedentes_Patologicosf_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_antecedentes_patologicosf()
	{
		$model = new Antecendentes_Patologicosf_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->antecedentes_patologicosf;
		$datos['id_consulta']   = $data->id_consulta;

		$query2 = $model->buscar_consulta($datos['id_consulta']);

		if ($query2) {
			$mensaje = 2;
		} else if (empty($query2)) {
			$query = $model->agregar_antecedentes_patologicosf($datos);

			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		}

		return json_encode($mensaje);
	}

	public function listar_antecedentes_patologicosf($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Antecendentes_Patologicosf_Model();

		$query = $model->antecedentes_patologicosf($n_historial);

		if (empty($query)) {
			$antecedentes_familiares = [];
		} else {
			$antecedentes_familiares = $query;
		}
		echo json_encode($antecedentes_familiares);
	}


	public function actualizar_antecedentes_patologicosf()
	{
		$model = new Antecendentes_Patologicosf_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->antecedentes_patologicosf;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_antecedentes_patologicosf($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
