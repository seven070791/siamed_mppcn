<?php

namespace App\Controllers;

use App\Models\AuditoriaModel;




class AuditoriaController extends BaseController
{
    public function index()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }

        echo view('auditoria_user/ultima_fecha');
        echo view('auditoria_user/footer_auditoria.php');

    }

    public function listar_auditoria($direccion_ip = null, $dispositivo = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new AuditoriaModel();

		$query = $model->listar_auditoria($direccion_ip, $dispositivo);

		if (empty($query)) {
			$auditoria = [];
		} else {
			$auditoria = $query;
		}
		echo json_encode($auditoria);
	}
}


