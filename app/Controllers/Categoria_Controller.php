<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
use App\Models\Medicamentos_model;


class Categoria_Controller extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	public function vistacategoria()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarCategoria/content_Categoria');
		echo view('/listarCategoria/footer_Categoria');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Categoria_Model();
		$query = $model->getAll();
		if (empty($query)) {
			$categoria = [];
		} else {
			$categoria = $query;
		}
		echo json_encode($categoria);
	}

	public function getAllActivos()
	{
		$model = new Categoria_Model();
		$query = $model->getAllActivos();
		if (empty($query)) {
			$categoria = [];
		} else {
			$categoria = $query;
		}
		echo json_encode($categoria);
	}






	/*
      * Método que guarda el registro nuevo
      */
	//public function save()
	public function agregar_categoria()
	{
		$model = new Categoria_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->categoria;
		$query = $model->agregar_categoria($datos);
		if (isset($query)) {
				$mensaje = 1;
				$auditoria['accion'] = 'REGISTRÓ EL TIPO DE CATEGORIA   '.' '.$datos['descripcion'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar_categoria()
	{
		$modelo = new Categoria_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_categoria;
		$datos['descripcion']   = $data->categoria;
		$datos['borrado']   = $data->borrado;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_descr_anterior['decripcion_anterior']= $data->descripcion_anterior;
		$query = $modelo->actualizar_categoria($datos);
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL TIPO DE CATEGORIA '.' '.$datos_descr_anterior['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
