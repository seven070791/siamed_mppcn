<?php

namespace App\Controllers;

use App\Models\Beneficiarios_Model;
use App\Models\Psicologia_Primaria_Model;
use App\Models\Enfermedad_Actual_Model;
use App\Models\Impresion_Diag_Model;
use App\Models\HistorialModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class Psicologia_Primaria_Controler extends BaseController
{
  public function listar_psicologia_primaria($numeroHistorial)
  {
    $model = new Psicologia_Primaria_Model();
    $query = $model->listar_psicologia_primaria($numeroHistorial);
    if (empty($query->getResult())) {
      $psicologia = [];
    } else {
      $psicologia = $query->getResultArray();
    }
    echo json_encode($psicologia);
  }

  public function Guardar_atencion_psicologia_p()
  {

    $model_psicologia_primaria = new Psicologia_Primaria_Model();
    $model_impresion_diagnostica = new Impresion_Diag_Model();

    $data = json_decode(base64_decode($this->request->getPost('data')));
    $datos_psicologia_p['n_historial']  = $data->numeroHistorial;
    $datos_psicologia_p['evolucion']   = $data->evolucion;
    $datos_psicologia_p['id_medico']   = $data->medico_id;
    $datos_psicologia_p['observacion']   = $data->observacion;
    $datos_psicologia_p['id_consulta']  = $data->id_consulta;
    
    $datos_imprecion_diag['descripcion']   = $data->impresiondiagnostica;
    $datos_imprecion_diag['n_historial']  = $data->numeroHistorial;
    $datos_imprecion_diag['id_consulta']  = $data->id_consulta;
    $datos_imprecion_diag['psicologia']='true';

    $n_historial=$datos_imprecion_diag['n_historial'];
    //Verifico si Existe ya una impresión diagnóstica para Psicologia Primaria
   $query_buscar_consulta = $model_impresion_diagnostica->buscar_historia_impresion_diag($n_historial);
    if(empty($query_buscar_consulta))
    {
      //Si no existe Registro, entonces guardo la impresión diagnóstica en la tabla impresion_diagnostica para psicologia  
      $query2 = $model_impresion_diagnostica->agregar($datos_imprecion_diag);   
       if (isset($query2)) 
       {
          /// Guardo un registro de los seguimientos en l a tabla de psicologia primaria  
          $query = $model_psicologia_primaria->Guardar_atencion_psicologia_p($datos_psicologia_p);
          //Se guardó el Registro de ID y el seguimiento en psicologia_primaria
          if (isset($query))
          {
            //Registro exitoso de la ID y del Seguimiento
            $mensaje = 1;
          }
          else 
          {
            //Registro exitoso de la ID pero no se registró el Seguimiento
            $mensaje = 2;
          }
        }
        else 
        {
          //Problemas en el registro de la ID
          $mensaje = 3;
        }
    }
    else
    {
      //YA EXISTE UN REGISTRO EN IMPRESION DIAGNOSTICA//   
      $query = $model_psicologia_primaria->Guardar_atencion_psicologia_p($datos_psicologia_p);
      if (isset($query)) 
        {
          //Se registró el seguimiento a la ID Existente
          $mensaje = 4;
        }
        else  
        {
          //No se pudo realizar el registro del seguimiento a la ID Existente
          $mensaje = 5;
        }
    }
    

  



    
    return json_encode($mensaje);
  }



  public function actualizar_Psicologia_Primaria()
  {
    $modelo = new Psicologia_Primaria_Model();
    $data = json_decode(base64_decode($this->request->getPost('data')));
    $datos['evolucion']   = $data->evolucion;
    $datos['observacion']   = $data->observacion;
    $datos['id']   = $data->id_psicologiap;
    $query = $modelo->actualizar_Psicologia_Primaria($datos);
    if (isset($query)) {
      $mensaje = 1;
    } else {
      $mensaje = 0;
    }
    //$mensaje=$datos;
    return json_encode($mensaje);
  }
}
