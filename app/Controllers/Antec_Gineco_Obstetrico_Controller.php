<?php

namespace App\Controllers;

use App\Models\Antece_Gineco_Obstetrico_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Medicamentos_model;
use App\Models\Partos_Model;
use App\Models\Cesarias_Model;
use App\Models\Abortos_Model;
use App\Models\Gestacion_Model;


use CodeIgniter\RESTful\ResourceController;

class Antec_Gineco_Obstetrico_Controller extends BaseController
{
	use ResponseTrait;

	public function agregar()
	{
		$model = new Antece_Gineco_Obstetrico_Model();
		$model_partos = new Partos_Model();
		$model_cesarias = new Cesarias_Model();
		$model_abortos = new Abortos_Model();
		$model_gestacion = new Gestacion_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['id_consulta']   = $data->id_consulta;
		$datos['menarquia']   = $data->menarquia;
		$datos['sexarquia'] = $data->sexarquia;
		$datos['nps'] = $data->nps;
		if ($data->f_u_r != '') {
			$datos['f_u_r'] = $data->f_u_r;
		}
		$datos['edad_gestacional'] = $data->eg;
		$datos['dismenorrea'] = $data->dismenorrea;
		$datos['eumenorrea'] = $data->eumenorrea;
		$datos['anticonceptivo'] = $data->anticon;
		$datos['detalles_anticon'] = $data->detalles_anticon;
		if ($data->t_colocacion != '') {
			$datos['t_colocacion'] = $data->t_colocacion;
		}
		$datos['ets'] = $data->ets;
		$datos['tipo_ets'] = $data->tipo_ets;
		$datos['detalle_ets'] = $data->detalles_ets;
		$datos['citologia'] = $data->citologia;
		$datos['eco_mamario'] = $data->eco_mamario;
		$datos['mamografia'] = $data->mamografia;
		$datos['desintometria'] = $data->desintometria;
		$datos['detalle_historia_obst'] = $data->detalle_historia;	
		$datos['diu'] = $data->diu;	
		$datos['t_cobre'] = $data->t_cobre;	
		$datos['mirena'] = $data->mirena;	
		$datos['aspiral'] = $data->aspiral;	
		$datos['i_subdermico'] = $data->i_subdermico;	
		$datos['otro'] = $data->otro;
		$datos_gestacion['id_gestacion_numeros_romanos'] = $data->gestacion;
		$datos_partos['id_partos_numeros_romanos'] = $data->partos;
		$datos_cesarias['id_cesarias_numeros_romanos'] = $data->cesarias;
		$datos_abortos['id_abortos_numeros_romanos'] = $data->abortos;
		$datos_gestacion['n_historial']   = $data->n_historial;
		$datos_partos['n_historial']   = $data->n_historial;
		$datos_cesarias['n_historial']   = $data->n_historial;
		$datos_abortos['n_historial']   = $data->n_historial;
		$query = $model->agregar($datos);
		$query_partos = $model_partos->agregar($datos_partos);
		$query_cesarias = $model_cesarias->agregar($datos_cesarias);
		$query_abortos = $model_abortos->agregar($datos_abortos);
		$query_gestacion = $model_gestacion->agregar($datos_gestacion);	
		
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		 return json_encode($mensaje);
	}

	public function actualizar_Antecedentes_gineco_obstetricos()
	{
		$model = new Antece_Gineco_Obstetrico_Model();
		$model_partos = new Partos_Model();
		$model_cesarias = new Cesarias_Model();
		$model_abortos = new Abortos_Model();
		$model_gestacion = new Gestacion_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['id_consulta']   = $data->id_consulta;
		$datos['menarquia']   = $data->menarquia;
		$datos['sexarquia'] = $data->sexarquia;
		$datos['nps'] = $data->nps;
		$datos['ciclo_mestrual'] = $data->ciclo_mestrual;
		if ($data->f_u_r != '') {
			$datos['f_u_r'] = $data->f_u_r;
		}
		$datos['edad_gestacional'] = $data->eg;
		$datos['dismenorrea'] = $data->dismenorrea;
		$datos['eumenorrea'] = $data->eumenorrea;
		$datos['anticonceptivo'] = $data->anticon;
		$datos['detalles_anticon'] = $data->detalles_anticon;
		if ($data->t_colocacion != '') {
			$datos['t_colocacion'] = $data->t_colocacion;
		}
		$datos['ets'] = $data->ets;
		$datos['tipo_ets'] = $data->tipo_ets;
		$datos['detalle_ets'] = $data->detalles_ets;
		$datos['citologia'] = $data->citologia;
		$datos['eco_mamario'] = $data->eco_mamario;
		$datos['mamografia'] = $data->mamografia;
		$datos['desintometria'] = $data->desintometria;
		$datos['detalle_historia_obst'] = $data->detalle_historia;
		$datos['diu'] = $data->diu;	
		$datos['t_cobre'] = $data->t_cobre;	
		$datos['mirena'] = $data->mirena;	
		$datos['aspiral'] = $data->aspiral;	
		$datos['i_subdermico'] = $data->i_subdermico;	
		$datos['otro'] = $data->otro;
		$datos_gestacion['id_gestacion_numeros_romanos'] = $data->gestacion;
		$datos_partos['id_partos_numeros_romanos'] = $data->partos;
		$datos_cesarias['id_cesarias_numeros_romanos'] = $data->cesarias;
		$datos_abortos['id_abortos_numeros_romanos'] = $data->abortos;
		$datos_gestacion['n_historial']   = $data->n_historial;
		$datos_partos['n_historial']   = $data->n_historial;
		$datos_cesarias['n_historial']   = $data->n_historial;
		$datos_abortos['n_historial']   = $data->n_historial;

		$query = $model->actualizar_Antecedentes_gineco_obstetricos($datos);
		$query_partos = $model_partos->actualizar($datos_partos);
		$query_cesarias = $model_cesarias->actualizar($datos_cesarias);
		$query_abortos = $model_abortos->actualizar($datos_abortos);
		$query_gestacion = $model_gestacion->actualizar($datos_gestacion);	
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
