<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Plan_Model;
use App\Models\Riesgos_Model;
use CodeIgniter\RESTful\ResourceController;
use App\Models\Auditoria_sistema_Model;
use function PHPSTORM_META\type;

class Riesgos_Controler extends BaseController
{
	use ResponseTrait;


	public function habitos_psicobiologicos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/habitos_psicobiologicos/habitos.php');
		echo view('/habitos_psicobiologicos/footer_habitos.php');
	}

/*
      * METODO QUE INSETAR UNA NUEVA RIESGOS EN LA TABLA PRINCIPAL RIESGOS
 */
	public function agregar_riesgos_master()
	{
		$model = new Riesgos_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->descripcion;
		$query = $model->agregar_riesgos_master($datos);
		if (isset($query)) {

			$mensaje = 1;
            $auditoria['accion'] = 'REGISTRÓ EL RIESO  DE   '.' '.$datos['descripcion'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

/*
      * METODO QUE ACTUALIZA LA RIESGOS EN LA TABLA PRINCIPAL RIESGOS
 */
	public function actualizar_riesgos_master()
	{
		$model = new Riesgos_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_habitos;
		$datos['descripcion']   = $data->descripcion;
		$datos['borrado'] = $data->borrado;
		$auditoria['accion'] = strtoupper($data->datos_modificados);
		$query = $model->actualizar_riesgos_master($datos);
		if (isset($query)) {
			$mensaje = 1;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}


















	public function agregar_Riesgos()

	{
	

		$model = new Riesgos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$nHistorials = $data->n_historials;
		$n_historia= $data->n_historia;
		$check_borrados=[];
		$check_borrados=$data->valores_borrados;
		$selectedValues = $data->selectedValues;

		/// SI NO SELECCIONO NINGUNO , ENTONCES ELIMINAMOS LOS REGISTROS DE LA TABLA HISTORIAL_Riesgos
		if (empty($selectedValues)) 
		{
			//VERIFICO SI TIENE Riesgos ASOCIADAS
			$query_Riesgos_Asociadas=$model->listar_riesgos_Historial($n_historia);		
			if (empty($query_Riesgos_Asociadas)) {
				$mensaje = 2;
				
			}else
			{
				$query_actualizar_Riesgos_existentes=$model->Eliminar_Riesgos_existentes($check_borrados,$n_historia);		
				$mensaje = 1;
			}
			
			return json_encode($mensaje);
		}else
		{
			// SI HAY REGISTROS SELECCIONADOS ENTONCES RECORREMOS ($selectedValues) 
			// Inicialice una matriz vacía para almacenar los resultados de cada inserción
			$results = [];
			// Recorra el arreglo nHistorials e inserte una nueva fila para cada uno
			foreach ($nHistorials as $index => $nHistorial)
			{
				$datos = array(
					'n_historial' => $nHistorial,
					'id_riesgo' => $selectedValues[$index]
				);
				
				// BUSCAMOS SI YA EXISTE ESE RIESDO EN EL HISTORIAL ACTUAL
				$query_buscar_riesgos_existentes = $model->riesgos_existentes($datos);
				 // SI ,NO HAY UN REGISTRO PARA ESA HISTORIA Y ESE RIESGO , AGREGAMOS EL INSERT 
				 if (empty($query_buscar_riesgos_existentes)) {
					if (!empty($check_borrados)) 
						{
							$query_Eliminar_riesdos_existentes=$model->Eliminar_Riesgos_existentes($check_borrados,$n_historia);	
						}
					$results[] = $model->agregar($datos);
				}
				else 
				{	
				// BUSCAMOS LAS RIESGOS CON SU ESTATUS DE BORRADO
					foreach ($query_buscar_riesgos_existentes as $existentes)
					 {
						$riesgos['n_historial']     =   $existentes->n_historial;
						$riesgos['id_riesgo']    = $existentes->id_riesgo;
                        $riesgos['borrado']         = $existentes->borrado;
					 }

				// VERIFICAMOS SI HAY RIESGOS BORRADAS
					if ($riesgos['borrado']=='t') 
				 	{
						$query_Activar_riesgos_existentes=$model->Activar_Riesgos_existentes($riesgos);
					}
					if (!empty($check_borrados)) 
					{
						$query_Eliminar_riesdos_existentes=$model->Eliminar_Riesgos_existentes($check_borrados,$n_historia);	
					}

				 }
			}
			$mensaje = 1;
				/* // Si todas las inserciones fueron exitosas, establezca el mensaje en 1
				if (count(array_filter($results)) === count($results)) {
					$mensaje = 1;
				} else {
					// Si alguna inserción falló, establezca el mensaje en 0
					$mensaje = 0;
				} */

		}

		return json_encode($mensaje);
	}


	public function Listar_riesgos()
    {
		
        $modelo_Riesgos = new  Riesgos_Model();
        $query_Riesgos = $modelo_Riesgos->Listar_riesgos();

        if (empty($query_Riesgos)) {
            $Riesgos = [];
        } else {
            $Riesgos = $query_Riesgos;
        }
        echo json_encode($Riesgos);
    }

	public function Listar_riesgos_activos()
    {
		
        $modelo_Riesgos = new  Riesgos_Model();
        $query_Riesgos = $modelo_Riesgos->Listar_riesgos_activos();

        if (empty($query_Riesgos)) {
            $Riesgos = [];
        } else {
            $Riesgos = $query_Riesgos;
        }
        echo json_encode($Riesgos);
    }

	


	public function listar_riesgos_Historial($numeroHistorial=null)
    {
        $modelo_riesgos = new  Riesgos_Model();
        $query_riesgos = $modelo_riesgos->listar_riesgos_Historial($numeroHistorial);

        if (empty($query_riesgos)) {
            $riesgos = [];
        } else {
            $riesgos = $query_riesgos;
        }
        echo json_encode($riesgos);
    }
	

	public function actualizar_plan()
	{
		$model = new Plan_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->plan;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_plan($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
