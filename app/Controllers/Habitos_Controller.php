<?php

namespace App\Controllers;

use App\Models\Habitos_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Beneficiarios_Model;
use App\Models\HistorialModel;
use CodeIgniter\RESTful\ResourceController;

class Habitos_Controller extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	public function vistacategoria()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarCategoria/content_Categoria');
		echo view('/listarCategoria/footer_Categoria');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function listar_habitos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Habitos_Model();
		$query = $model->getAll();
		if (empty($query)) {
			$categoria = [];
		} else {
			$categoria = $query;
		}
		echo json_encode($categoria);
	}


	public function agregar_habito_historial()
	{
		$modelo = new  Habitos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['historial_id']   = $data->historial_id;
		$datos['habito_id'] = $data->habitos_psicosociales;
		$query = $modelo->agregar_habito_historial($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}


	public function  listar_habitos_personales($historial_medico)
	{
		$model = new Habitos_Model();
		$query = $model->listar_habitos_personales($historial_medico);
		if (empty($query)) {
			$habitospersonales = [];
		} else {
			$habitospersonales = $query;
		}
		echo json_encode($habitospersonales);
	}


	public function borrar_habitos()
	{

		$modelo = new  Habitos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']     = $data->habito_id;
		$datos['borrado'] = $data->borrado;
		$query = $modelo->borrar_habitos($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
