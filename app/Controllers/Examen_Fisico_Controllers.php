<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Examen_Fisico_Model;
use CodeIgniter\RESTful\ResourceController;

class Examen_Fisico_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_examen_fisico()
	{
		$model = new Examen_Fisico_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->examen_fisico;
		$datos['id_consulta']   = $data->id_consulta;

		$query = $model->agregar($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function listar_examen_fisico($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Examen_Fisico_Model();

		$query = $model->listar_examen_fisico($n_historial);
		if (empty($query)) {
			$examen_fisico = [];
		} else {
			$examen_fisico = $query;
		}
		echo json_encode($examen_fisico);
	}



	public function actualizar_examen_fisico()
	{
		$model = new Examen_Fisico_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->examen_fisico;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_examen_fisico($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
