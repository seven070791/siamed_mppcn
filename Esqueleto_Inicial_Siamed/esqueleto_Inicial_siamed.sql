--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Ubuntu 14.4-1.pgdg18.04+1)
-- Dumped by pg_dump version 14.4 (Ubuntu 14.4-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: historial_clinico; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA historial_clinico;


ALTER SCHEMA historial_clinico OWNER TO postgres;

--
-- Name: migracion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA migracion;


ALTER SCHEMA migracion OWNER TO postgres;

--
-- Name: migracion_2; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA migracion_2;


ALTER SCHEMA migracion_2 OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: abortos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.abortos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_abortos_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.abortos OWNER TO postgres;

--
-- Name: abortos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.abortos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.abortos_id_seq OWNER TO postgres;

--
-- Name: abortos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.abortos_id_seq OWNED BY historial_clinico.abortos.id;


--
-- Name: alergias_medicamentos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.alergias_medicamentos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.alergias_medicamentos OWNER TO postgres;

--
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.alergias_medicamentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.alergias_medicamentos_id_seq OWNER TO postgres;

--
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.alergias_medicamentos_id_seq OWNED BY historial_clinico.alergias_medicamentos.id;


--
-- Name: antecedentes_gineco_osbtetrico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_gineco_osbtetrico (
    id integer NOT NULL,
    n_historial text,
    id_consulta integer NOT NULL,
    menarquia integer,
    sexarquia integer,
    nps integer,
    ciclo_mestrual text,
    f_u_r date,
    edad_gestacional text,
    dismenorrea boolean DEFAULT false NOT NULL,
    eumenorrea boolean DEFAULT false NOT NULL,
    anticonceptivo boolean DEFAULT false NOT NULL,
    detalles_anticon text,
    diu boolean DEFAULT false NOT NULL,
    t_cobre boolean DEFAULT false NOT NULL,
    mirena boolean DEFAULT false NOT NULL,
    aspiral boolean DEFAULT false NOT NULL,
    i_subdermico boolean DEFAULT false NOT NULL,
    otro text,
    t_colocacion date,
    ets boolean DEFAULT false NOT NULL,
    tipo_ets integer DEFAULT 0,
    detalle_ets text,
    citologia text,
    eco_mamario text,
    mamografia text,
    desintometria text,
    detalle_historia_obst text
);


ALTER TABLE historial_clinico.antecedentes_gineco_osbtetrico OWNER TO postgres;

--
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_gineco_osbtetrico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.antecedentes_gineco_osbtetrico_id_seq OWNER TO postgres;

--
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_gineco_osbtetrico_id_seq OWNED BY historial_clinico.antecedentes_gineco_osbtetrico.id;


--
-- Name: antecedentes_patologicosf; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_patologicosf (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_patologicosf OWNER TO postgres;

--
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_patologicosf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.antecedentes_patologicosf_id_seq OWNER TO postgres;

--
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_patologicosf_id_seq OWNED BY historial_clinico.antecedentes_patologicosf.id;


--
-- Name: antecedentes_patologicosp; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_patologicosp (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_patologicosp OWNER TO postgres;

--
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_patologicosp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.antecedentes_patologicosp_id_seq OWNER TO postgres;

--
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_patologicosp_id_seq OWNED BY historial_clinico.antecedentes_patologicosp.id;


--
-- Name: antecedentes_quirurgicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_quirurgicos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_quirurgicos OWNER TO postgres;

--
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_quirurgicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.antecedentes_quirurgicos_id_seq OWNER TO postgres;

--
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_quirurgicos_id_seq OWNED BY historial_clinico.antecedentes_quirurgicos.id;


--
-- Name: cesarias; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.cesarias (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_cesarias_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.cesarias OWNER TO postgres;

--
-- Name: cesarias_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.cesarias_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.cesarias_id_seq OWNER TO postgres;

--
-- Name: cesarias_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.cesarias_id_seq OWNED BY historial_clinico.cesarias.id;


--
-- Name: consultas; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.consultas (
    id integer NOT NULL,
    n_historial text,
    id_medico integer NOT NULL,
    peso integer,
    talla integer,
    spo2 integer,
    frecuencia_c integer,
    frecuencia_r integer,
    temperatura integer,
    tension_alta integer,
    tension_baja integer,
    imc integer,
    fecha_asistencia date,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    asistencia boolean DEFAULT false,
    consulta boolean DEFAULT true NOT NULL,
    tipo_beneficiario text,
    borrado boolean DEFAULT false,
    motivo_consulta text,
    user_id integer
);


ALTER TABLE historial_clinico.consultas OWNER TO postgres;

--
-- Name: consultas_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.consultas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.consultas_id_seq OWNER TO postgres;

--
-- Name: consultas_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.consultas_id_seq OWNED BY historial_clinico.consultas.id;


--
-- Name: detalle_notas_entrega; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.detalle_notas_entrega (
    id integer NOT NULL,
    nota_entrega_id integer,
    salida_id integer,
    fecha date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.detalle_notas_entrega OWNER TO postgres;

--
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.detalle_notas_entrega_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.detalle_notas_entrega_id_seq OWNER TO postgres;

--
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.detalle_notas_entrega_id_seq OWNED BY historial_clinico.detalle_notas_entrega.id;


--
-- Name: enfermedad_actual; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.enfermedad_actual (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.enfermedad_actual OWNER TO postgres;

--
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.enfermedad_actual_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.enfermedad_actual_id_seq OWNER TO postgres;

--
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.enfermedad_actual_id_seq OWNED BY historial_clinico.enfermedad_actual.id;


--
-- Name: enfermedades_sexuales; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.enfermedades_sexuales (
    id bigint NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.enfermedades_sexuales OWNER TO postgres;

--
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.enfermedades_sexuales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.enfermedades_sexuales_id_seq OWNER TO postgres;

--
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.enfermedades_sexuales_id_seq OWNED BY historial_clinico.enfermedades_sexuales.id;


--
-- Name: especialidades; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.especialidades (
    id_especialidad integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false,
    acceso_citas boolean DEFAULT false,
    caja_chica boolean DEFAULT false
);


ALTER TABLE historial_clinico.especialidades OWNER TO postgres;

--
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.especialidades_id_especialidad_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.especialidades_id_especialidad_seq OWNER TO postgres;

--
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.especialidades_id_especialidad_seq OWNED BY historial_clinico.especialidades.id_especialidad;


--
-- Name: examen_fisico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.examen_fisico (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.examen_fisico OWNER TO postgres;

--
-- Name: examen_fisico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.examen_fisico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.examen_fisico_id_seq OWNER TO postgres;

--
-- Name: examen_fisico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.examen_fisico_id_seq OWNED BY historial_clinico.examen_fisico.id;


--
-- Name: gestacion; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.gestacion (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_gestacion_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.gestacion OWNER TO postgres;

--
-- Name: gestacion_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.gestacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.gestacion_id_seq OWNER TO postgres;

--
-- Name: gestacion_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.gestacion_id_seq OWNED BY historial_clinico.gestacion.id;


--
-- Name: habito_historial; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.habito_historial (
    id integer NOT NULL,
    historial_id integer NOT NULL,
    habito_id integer NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.habito_historial OWNER TO postgres;

--
-- Name: habito_historial_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.habito_historial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.habito_historial_id_seq OWNER TO postgres;

--
-- Name: habito_historial_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.habito_historial_id_seq OWNED BY historial_clinico.habito_historial.id;


--
-- Name: habitos_psicosociales; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.habitos_psicosociales (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.habitos_psicosociales OWNER TO postgres;

--
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.habitos_psicosociales_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.habitos_psicosociales_id_seq OWNER TO postgres;

--
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.habitos_psicosociales_id_seq OWNED BY historial_clinico.habitos_psicosociales.id;


--
-- Name: historial_informativo; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.historial_informativo (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.historial_informativo OWNER TO postgres;

--
-- Name: historial_informativo_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.historial_informativo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.historial_informativo_id_seq OWNER TO postgres;

--
-- Name: historial_informativo_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.historial_informativo_id_seq OWNED BY historial_clinico.historial_informativo.id;


--
-- Name: historial_medico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.historial_medico (
    id integer NOT NULL,
    n_historial text NOT NULL,
    cedula text NOT NULL,
    fecha_regist date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.historial_medico OWNER TO postgres;

--
-- Name: historial_medico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.historial_medico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.historial_medico_id_seq OWNER TO postgres;

--
-- Name: historial_medico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.historial_medico_id_seq OWNED BY historial_clinico.historial_medico.id;


--
-- Name: impresion_diag; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.impresion_diag (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false,
    psicologia boolean DEFAULT false
);


ALTER TABLE historial_clinico.impresion_diag OWNER TO postgres;

--
-- Name: impresion_diag_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.impresion_diag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.impresion_diag_id_seq OWNER TO postgres;

--
-- Name: impresion_diag_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.impresion_diag_id_seq OWNED BY historial_clinico.impresion_diag.id;


--
-- Name: medicamentos_patologias; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.medicamentos_patologias (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false,
    id_especialidad integer
);


ALTER TABLE historial_clinico.medicamentos_patologias OWNER TO postgres;

--
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.medicamentos_medicina_interna_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.medicamentos_medicina_interna_id_seq OWNER TO postgres;

--
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.medicamentos_medicina_interna_id_seq OWNED BY historial_clinico.medicamentos_patologias.id;


--
-- Name: medicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.medicos (
    id integer NOT NULL,
    cedula integer NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    telefono text,
    especialidad integer NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.medicos OWNER TO postgres;

--
-- Name: medicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.medicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.medicos_id_seq OWNER TO postgres;

--
-- Name: medicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.medicos_id_seq OWNED BY historial_clinico.medicos.id;


--
-- Name: metodos_anticonceptibos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.metodos_anticonceptibos (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.metodos_anticonceptibos OWNER TO postgres;

--
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.metodos_anticonceptibos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.metodos_anticonceptibos_id_seq OWNER TO postgres;

--
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.metodos_anticonceptibos_id_seq OWNED BY historial_clinico.metodos_anticonceptibos.id;


--
-- Name: notas_entregas; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.notas_entregas (
    id integer NOT NULL,
    n_historial text NOT NULL,
    fecha_registro date NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    usuario text,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.notas_entregas OWNER TO postgres;

--
-- Name: notas_entregas_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.notas_entregas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.notas_entregas_id_seq OWNER TO postgres;

--
-- Name: notas_entregas_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.notas_entregas_id_seq OWNED BY historial_clinico.notas_entregas.id;


--
-- Name: numeros_romanos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.numeros_romanos (
    id integer NOT NULL,
    descripcion character varying(4) NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.numeros_romanos OWNER TO postgres;

--
-- Name: numeros_romanos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.numeros_romanos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.numeros_romanos_id_seq OWNER TO postgres;

--
-- Name: numeros_romanos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.numeros_romanos_id_seq OWNED BY historial_clinico.numeros_romanos.id;


--
-- Name: paraclinicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.paraclinicos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.paraclinicos OWNER TO postgres;

--
-- Name: paraclinicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.paraclinicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.paraclinicos_id_seq OWNER TO postgres;

--
-- Name: paraclinicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.paraclinicos_id_seq OWNED BY historial_clinico.paraclinicos.id;


--
-- Name: partos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.partos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_partos_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.partos OWNER TO postgres;

--
-- Name: partos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.partos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.partos_id_seq OWNER TO postgres;

--
-- Name: partos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.partos_id_seq OWNED BY historial_clinico.partos.id;


--
-- Name: plan; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.plan (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.plan OWNER TO postgres;

--
-- Name: plan_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.plan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.plan_id_seq OWNER TO postgres;

--
-- Name: plan_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.plan_id_seq OWNED BY historial_clinico.plan.id;


--
-- Name: psicologia_primaria; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.psicologia_primaria (
    id integer NOT NULL,
    n_historial text NOT NULL,
    fecha_seguimiento date DEFAULT CURRENT_DATE NOT NULL,
    evolucion text,
    borrado boolean DEFAULT false NOT NULL,
    id_medico integer,
    observacion text,
    id_consulta integer
);


ALTER TABLE historial_clinico.psicologia_primaria OWNER TO postgres;

--
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.psicologia_primaria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.psicologia_primaria_id_seq OWNER TO postgres;

--
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.psicologia_primaria_id_seq OWNED BY historial_clinico.psicologia_primaria.id;


--
-- Name: reposos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.reposos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_medico integer NOT NULL,
    fecha_desde date NOT NULL,
    fecha_hasta date NOT NULL,
    motivo text NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE,
    horas integer
);


ALTER TABLE historial_clinico.reposos OWNER TO postgres;

--
-- Name: reposos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.reposos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE historial_clinico.reposos_id_seq OWNER TO postgres;

--
-- Name: reposos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.reposos_id_seq OWNED BY historial_clinico.reposos.id;


--
-- Name: categoria_inventario; Type: TABLE; Schema: migracion; Owner: postgres
--

CREATE TABLE migracion.categoria_inventario (
    id integer NOT NULL,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE migracion.categoria_inventario OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE; Schema: migracion; Owner: postgres
--

CREATE SEQUENCE migracion.categoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion.categoria_id_seq OWNER TO postgres;

--
-- Name: categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion; Owner: postgres
--

ALTER SEQUENCE migracion.categoria_id_seq OWNED BY migracion.categoria_inventario.id;


--
-- Name: medicamentos; Type: TABLE; Schema: migracion; Owner: postgres
--

CREATE TABLE migracion.medicamentos (
    id bigint,
    id_tipo_medicamento integer,
    id_control integer,
    id_presentacion integer,
    descripcion text,
    fecha_creacion date,
    borrado boolean
);


ALTER TABLE migracion.medicamentos OWNER TO postgres;

--
-- Name: medicamentos_xls; Type: TABLE; Schema: migracion; Owner: postgres
--

CREATE TABLE migracion.medicamentos_xls (
    nro text,
    medicamento text,
    mg text,
    nombre_medicamento text,
    id integer NOT NULL,
    fecha_creadion date DEFAULT CURRENT_DATE,
    id_tipo_medicamento integer,
    id_control integer,
    id_presentacion integer,
    descripcion text
);


ALTER TABLE migracion.medicamentos_xls OWNER TO postgres;

--
-- Name: medicamentos_xls_id_seq; Type: SEQUENCE; Schema: migracion; Owner: postgres
--

CREATE SEQUENCE migracion.medicamentos_xls_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion.medicamentos_xls_id_seq OWNER TO postgres;

--
-- Name: medicamentos_xls_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion; Owner: postgres
--

ALTER SEQUENCE migracion.medicamentos_xls_id_seq OWNED BY migracion.medicamentos_xls.id;


--
-- Name: para_medicamentos; Type: TABLE; Schema: migracion; Owner: postgres
--

CREATE TABLE migracion.para_medicamentos (
    id_tipo_medicamento_txt text,
    id_control_txt text,
    id_presentacion_txt text,
    descripcion_txt text,
    id bigint NOT NULL,
    id_tipo_medicamento integer,
    id_control integer,
    id_presentacion integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE migracion.para_medicamentos OWNER TO postgres;

--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE; Schema: migracion; Owner: postgres
--

CREATE SEQUENCE migracion.para_medicamentos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion.para_medicamentos_id_seq OWNER TO postgres;

--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion; Owner: postgres
--

ALTER SEQUENCE migracion.para_medicamentos_id_seq OWNED BY migracion.para_medicamentos.id;


--
-- Name: tipo_medicamento; Type: TABLE; Schema: migracion; Owner: postgres
--

CREATE TABLE migracion.tipo_medicamento (
    id bigint NOT NULL,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false,
    categoria_id integer
);


ALTER TABLE migracion.tipo_medicamento OWNER TO postgres;

--
-- Name: tipo_medicamento_por_categoria_id_seq; Type: SEQUENCE; Schema: migracion; Owner: postgres
--

CREATE SEQUENCE migracion.tipo_medicamento_por_categoria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion.tipo_medicamento_por_categoria_id_seq OWNER TO postgres;

--
-- Name: tipo_medicamento_por_categoria_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion; Owner: postgres
--

ALTER SEQUENCE migracion.tipo_medicamento_por_categoria_id_seq OWNED BY migracion.tipo_medicamento.id;


--
-- Name: desde_excel; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.desde_excel (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text
);


ALTER TABLE migracion_2.desde_excel OWNER TO postgres;

--
-- Name: desde_excel_2; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.desde_excel_2 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text
);


ALTER TABLE migracion_2.desde_excel_2 OWNER TO postgres;

--
-- Name: entradas_def; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.entradas_def (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fecha_entrada date DEFAULT CURRENT_DATE,
    fecha_vencimiento date DEFAULT CURRENT_DATE,
    id_medicamento integer NOT NULL,
    cantidad integer NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE migracion_2.entradas_def OWNER TO postgres;

--
-- Name: entradas_def_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.entradas_def_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.entradas_def_id_seq OWNER TO postgres;

--
-- Name: entradas_def_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.entradas_def_id_seq OWNED BY migracion_2.entradas_def.id;


--
-- Name: medicamentos_def; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.medicamentos_def (
    id integer NOT NULL,
    id_tipo_medicamento integer NOT NULL,
    id_control integer NOT NULL,
    id_presentacion integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    stock_minimo integer DEFAULT 10,
    med_cronico boolean DEFAULT false
);


ALTER TABLE migracion_2.medicamentos_def OWNER TO postgres;

--
-- Name: medicamentos_def_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.medicamentos_def_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.medicamentos_def_id_seq OWNER TO postgres;

--
-- Name: medicamentos_def_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.medicamentos_def_id_seq OWNED BY migracion_2.medicamentos_def.id;


--
-- Name: para_entradas; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas (
    fecha_vencimiento date,
    id_medicamento integer,
    cantidad integer,
    user_id integer,
    id integer
);


ALTER TABLE migracion_2.para_entradas OWNER TO postgres;

--
-- Name: para_entradas_prev; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas_prev (
    fecha_vencimiento date,
    id_medicamento integer,
    cantidad integer,
    user_id integer,
    id integer
);


ALTER TABLE migracion_2.para_entradas_prev OWNER TO postgres;

--
-- Name: para_entradas_prev_1; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas_prev_1 (
    campo1 text,
    descripcion text,
    presentacion text,
    cantidad_num bigint,
    campo4 text,
    descripcion_full text,
    id_medicamento integer,
    vencimiento text
);


ALTER TABLE migracion_2.para_entradas_prev_1 OWNER TO postgres;

--
-- Name: para_entradas_prev_2; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas_prev_2 (
    campo1 text,
    descripcion text,
    presentacion text,
    cantidad_txt text,
    campo4 text,
    descripcion_full text,
    cantidad integer,
    id_medicamento integer,
    vencimiento text,
    vencimiento_2 text,
    vencimiento_3 text,
    vencimiento_4 text,
    vencimiento_5 text,
    vencimiento_6 date,
    vencimiento_7 text,
    vencimiento_8 date,
    fecha_vencimiento date
);


ALTER TABLE migracion_2.para_entradas_prev_2 OWNER TO postgres;

--
-- Name: para_entradas_prev_3; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas_prev_3 (
    fecha_vencimiento date,
    id_medicamento integer,
    cantidad integer
);


ALTER TABLE migracion_2.para_entradas_prev_3 OWNER TO postgres;

--
-- Name: para_entradas_prev_4; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_entradas_prev_4 (
    fecha_vencimiento date,
    id_medicamento integer,
    cantidad integer,
    user_id integer,
    id integer NOT NULL
);


ALTER TABLE migracion_2.para_entradas_prev_4 OWNER TO postgres;

--
-- Name: para_entradas_prev_4_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.para_entradas_prev_4_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.para_entradas_prev_4_id_seq OWNER TO postgres;

--
-- Name: para_entradas_prev_4_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.para_entradas_prev_4_id_seq OWNED BY migracion_2.para_entradas_prev_4.id;


--
-- Name: para_medicamentos; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos (
    descripcion text,
    id_tipo_medicamento integer,
    id_presentacion integer,
    id_control integer,
    id integer NOT NULL
);


ALTER TABLE migracion_2.para_medicamentos OWNER TO postgres;

--
-- Name: para_medicamentos_distinct; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_distinct (
    descripcion text,
    id_tipo_medicamento integer,
    id_presentacion integer,
    id_control integer
);


ALTER TABLE migracion_2.para_medicamentos_distinct OWNER TO postgres;

--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.para_medicamentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.para_medicamentos_id_seq OWNER TO postgres;

--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.para_medicamentos_id_seq OWNED BY migracion_2.para_medicamentos.id;


--
-- Name: para_medicamentos_prev; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    cantidad_txt text,
    presentacion_prev text,
    tipo_medicamento_id integer,
    presentacion text,
    control text,
    cantidad_txt_2 text,
    cantidad_num bigint,
    descripcion text,
    control_id integer,
    presentacion_id integer
);


ALTER TABLE migracion_2.para_medicamentos_prev OWNER TO postgres;

--
-- Name: para_medicamentos_prev_1; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev_1 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    cantidad_txt text,
    presentacion_prev text,
    tipo_medicamento_id integer
);


ALTER TABLE migracion_2.para_medicamentos_prev_1 OWNER TO postgres;

--
-- Name: para_medicamentos_prev_1_bak; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev_1_bak (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    campo7 text,
    campo8 text,
    tipo_medicamento_id integer,
    presentacion text,
    control text
);


ALTER TABLE migracion_2.para_medicamentos_prev_1_bak OWNER TO postgres;

--
-- Name: para_medicamentos_prev_2; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev_2 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    cantidad_txt text,
    presentacion_prev text,
    tipo_medicamento_id integer,
    presentacion text,
    control text,
    cantidad_txt_2 text
);


ALTER TABLE migracion_2.para_medicamentos_prev_2 OWNER TO postgres;

--
-- Name: para_medicamentos_prev_3; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev_3 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    cantidad_txt text,
    presentacion_prev text,
    tipo_medicamento_id integer,
    presentacion text,
    control text,
    cantidad_txt_2 text,
    cantidad_num bigint,
    descripcion text,
    control_id integer,
    presentacion_id integer
);


ALTER TABLE migracion_2.para_medicamentos_prev_3 OWNER TO postgres;

--
-- Name: para_medicamentos_prev_4; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_prev_4 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    cantidad_txt text,
    presentacion_prev text,
    tipo_medicamento_id integer,
    presentacion text,
    control text,
    cantidad_txt_2 text,
    cantidad_num bigint,
    descripcion text,
    control_id integer,
    presentacion_id integer
);


ALTER TABLE migracion_2.para_medicamentos_prev_4 OWNER TO postgres;

--
-- Name: para_medicamentos_todos; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_medicamentos_todos (
    descripcion text,
    id_tipo_medicamento integer,
    id_control integer,
    id_presentacion integer,
    descripcion_sin_presentacion text
);


ALTER TABLE migracion_2.para_medicamentos_todos OWNER TO postgres;

--
-- Name: para_tipo_medicamento; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_tipo_medicamento (
    id integer NOT NULL,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    categoria_id integer DEFAULT 1
);


ALTER TABLE migracion_2.para_tipo_medicamento OWNER TO postgres;

--
-- Name: para_tipo_medicamento_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.para_tipo_medicamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.para_tipo_medicamento_id_seq OWNER TO postgres;

--
-- Name: para_tipo_medicamento_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.para_tipo_medicamento_id_seq OWNED BY migracion_2.para_tipo_medicamento.id;


--
-- Name: para_tipo_medicamento_prev_1; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.para_tipo_medicamento_prev_1 (
    tipo_medicamento text,
    id integer,
    borrar smallint,
    categoria_id smallint
);


ALTER TABLE migracion_2.para_tipo_medicamento_prev_1 OWNER TO postgres;

--
-- Name: tipo_medicamento_1; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.tipo_medicamento_1 (
    tipo_medicamento text,
    id integer NOT NULL,
    borrar smallint DEFAULT 0,
    categoria_id smallint DEFAULT 1
);


ALTER TABLE migracion_2.tipo_medicamento_1 OWNER TO postgres;

--
-- Name: tipo_medicamento_1_depurado; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.tipo_medicamento_1_depurado (
    tipo_medicamento text,
    id integer,
    borrar smallint,
    categoria_id smallint
);


ALTER TABLE migracion_2.tipo_medicamento_1_depurado OWNER TO postgres;

--
-- Name: tipo_medicamento_1_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.tipo_medicamento_1_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.tipo_medicamento_1_id_seq OWNER TO postgres;

--
-- Name: tipo_medicamento_1_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.tipo_medicamento_1_id_seq OWNED BY migracion_2.tipo_medicamento_1.id;


--
-- Name: tipo_medicamento_def; Type: TABLE; Schema: migracion_2; Owner: postgres
--

CREATE TABLE migracion_2.tipo_medicamento_def (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    categoria_id integer DEFAULT 1
);


ALTER TABLE migracion_2.tipo_medicamento_def OWNER TO postgres;

--
-- Name: tipo_medicamento_def_id_seq; Type: SEQUENCE; Schema: migracion_2; Owner: postgres
--

CREATE SEQUENCE migracion_2.tipo_medicamento_def_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE migracion_2.tipo_medicamento_def_id_seq OWNER TO postgres;

--
-- Name: tipo_medicamento_def_id_seq; Type: SEQUENCE OWNED BY; Schema: migracion_2; Owner: postgres
--

ALTER SEQUENCE migracion_2.tipo_medicamento_def_id_seq OWNED BY migracion_2.tipo_medicamento_def.id;


--
-- Name: a1; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.a1 (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    campo7 text,
    id integer NOT NULL,
    huerfano boolean DEFAULT false
);


ALTER TABLE public.a1 OWNER TO postgres;

--
-- Name: a1_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.a1_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.a1_id_seq OWNER TO postgres;

--
-- Name: a1_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.a1_id_seq OWNED BY public.a1.id;


--
-- Name: auditoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auditoria (
    id integer NOT NULL,
    fecha_sesion date DEFAULT CURRENT_DATE NOT NULL,
    id_usuario integer,
    accion text,
    id_medicamento integer,
    cantidad integer,
    hora character(11)
);


ALTER TABLE public.auditoria OWNER TO postgres;

--
-- Name: auditoria_de_sistema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auditoria_de_sistema (
    id integer NOT NULL,
    user_id integer NOT NULL,
    accion text NOT NULL,
    fecha date DEFAULT CURRENT_DATE NOT NULL,
    hora character(11)
);


ALTER TABLE public.auditoria_de_sistema OWNER TO postgres;

--
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auditoria_de_sistema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auditoria_de_sistema_id_seq OWNER TO postgres;

--
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auditoria_de_sistema_id_seq OWNED BY public.auditoria_de_sistema.id;


--
-- Name: auditoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auditoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auditoria_id_seq OWNER TO postgres;

--
-- Name: auditoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auditoria_id_seq OWNED BY public.auditoria.id;


--
-- Name: categoria_inventario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria_inventario (
    id integer NOT NULL,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE public.categoria_inventario OWNER TO postgres;

--
-- Name: categoria_inventario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoria_inventario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categoria_inventario_id_seq OWNER TO postgres;

--
-- Name: categoria_inventario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoria_inventario_id_seq OWNED BY public.categoria_inventario.id;


--
-- Name: compuestos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compuestos (
    id_compuesto integer NOT NULL,
    descripcioncompuesta text,
    id_tipo_medicamento integer,
    id_unidad_medida integer,
    cantidad integer,
    borrado boolean DEFAULT false NOT NULL,
    id_medicamento integer NOT NULL
);


ALTER TABLE public.compuestos OWNER TO postgres;

--
-- Name: compuestos_id_compuesto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compuestos_id_compuesto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compuestos_id_compuesto_seq OWNER TO postgres;

--
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compuestos_id_compuesto_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compuestos_id_compuesto_seq1 OWNER TO postgres;

--
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compuestos_id_compuesto_seq1 OWNED BY public.compuestos.id_compuesto;


--
-- Name: control; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.control (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.control OWNER TO postgres;

--
-- Name: control_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.control_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.control_id_seq OWNER TO postgres;

--
-- Name: control_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.control_id_seq OWNED BY public.control.id;


--
-- Name: cortesia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cortesia (
    id integer NOT NULL,
    cedula_trabajador integer,
    nacionalidad character varying(1),
    nombre text,
    apellido text,
    telefono text,
    sexo integer,
    fecha_nac_cortesia date DEFAULT CURRENT_DATE,
    cedula text NOT NULL,
    observacion text,
    borrado boolean DEFAULT false NOT NULL,
    tipo_de_sangre integer,
    estado_civil integer,
    ocupacion_id integer,
    grado_intruccion_id integer,
    adscrito boolean DEFAULT false
);


ALTER TABLE public.cortesia OWNER TO postgres;

--
-- Name: cortesia_adscrito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cortesia_adscrito_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cortesia_adscrito_id_seq OWNER TO postgres;

--
-- Name: cortesia_adscrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cortesia_adscrito (
    id bigint DEFAULT nextval('public.cortesia_adscrito_id_seq'::regclass) NOT NULL,
    id_cortesia integer NOT NULL,
    id_adscrito integer NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cortesia_adscrito OWNER TO postgres;

--
-- Name: cortesia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cortesia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cortesia_id_seq OWNER TO postgres;

--
-- Name: cortesia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cortesia_id_seq OWNED BY public.cortesia.id;


--
-- Name: datos_titulares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.datos_titulares (
    cedula_trabajador text NOT NULL,
    nacionalidad text NOT NULL,
    nombre text,
    apellido text,
    telefono text,
    sexo text NOT NULL,
    fecha_nacimiento date NOT NULL,
    tipo_de_personal text,
    ubicacion_administrativa text,
    borrado boolean DEFAULT false,
    id integer NOT NULL
);


ALTER TABLE public.datos_titulares OWNER TO postgres;

--
-- Name: datos_titulares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.datos_titulares_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.datos_titulares_id_seq OWNER TO postgres;

--
-- Name: datos_titulares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.datos_titulares_id_seq OWNED BY public.datos_titulares.id;


--
-- Name: ente_adscrito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ente_adscrito_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ente_adscrito_id_seq OWNER TO postgres;

--
-- Name: ente_adscrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ente_adscrito (
    id integer DEFAULT nextval('public.ente_adscrito_id_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    rif text
);


ALTER TABLE public.ente_adscrito OWNER TO postgres;

--
-- Name: entradas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradas (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fecha_entrada date DEFAULT CURRENT_DATE,
    fecha_vencimiento date DEFAULT CURRENT_DATE,
    id_medicamento integer NOT NULL,
    cantidad integer NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.entradas OWNER TO postgres;

--
-- Name: entradas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entradas_id_seq OWNER TO postgres;

--
-- Name: entradas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradas_id_seq OWNED BY public.entradas.id;


--
-- Name: estado_civil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado_civil (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.estado_civil OWNER TO postgres;

--
-- Name: estado_civil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_civil_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_civil_id_seq OWNER TO postgres;

--
-- Name: estado_civil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_civil_id_seq OWNED BY public.estado_civil.id;


--
-- Name: exceltipomedicamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.exceltipomedicamento (
    medicamento text,
    mg text,
    disponible text
);


ALTER TABLE public.exceltipomedicamento OWNER TO postgres;

--
-- Name: familiares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.familiares (
    id bigint NOT NULL,
    cedula_trabajador integer,
    nombre text,
    apellido text,
    telefono text,
    sexo integer,
    fecha_nac_familiares date,
    parentesco_id integer,
    cedula text NOT NULL,
    tipo_de_sangre integer,
    estado_civil integer,
    grado_intruccion_id integer,
    ocupacion_id integer,
    borrado boolean DEFAULT false
);


ALTER TABLE public.familiares OWNER TO postgres;

--
-- Name: familiares2; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.familiares2 (
    id integer,
    cedula_trabajador integer,
    nombre text,
    apellido text,
    telefono text,
    sexo character varying(1),
    fecha_nac_familiares date,
    parentesco_id integer,
    cedula text,
    borrado boolean,
    tipo_de_sangre integer,
    estado_civil integer,
    grado_intruccion_id integer,
    ocupacion_id integer
);


ALTER TABLE public.familiares2 OWNER TO postgres;

--
-- Name: familiares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.familiares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.familiares_id_seq OWNER TO postgres;

--
-- Name: familiares_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.familiares_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.familiares_id_seq1 OWNER TO postgres;

--
-- Name: familiares_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.familiares_id_seq1 OWNED BY public.familiares.id;


--
-- Name: familiares_repetidos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.familiares_repetidos (
    campo7 text,
    veces bigint
);


ALTER TABLE public.familiares_repetidos OWNER TO postgres;

--
-- Name: grado_instruccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grado_instruccion (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.grado_instruccion OWNER TO postgres;

--
-- Name: grado_instruccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grado_instruccion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grado_instruccion_id_seq OWNER TO postgres;

--
-- Name: grado_instruccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grado_instruccion_id_seq OWNED BY public.grado_instruccion.id;


--
-- Name: grupo_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grupo_usuario (
    id integer NOT NULL,
    nivel_usuario character varying(100) NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.grupo_usuario OWNER TO postgres;

--
-- Name: importacion_familiares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.importacion_familiares (
    campo1 text,
    campo2 text,
    campo3 text,
    campo4 text,
    campo5 text,
    campo6 text,
    campo7 text
);


ALTER TABLE public.importacion_familiares OWNER TO postgres;

--
-- Name: medicamentorespaldo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medicamentorespaldo (
    id integer,
    id_tipo_medicamento integer,
    id_control integer,
    id_presentacion integer,
    descripcion text,
    fecha_creacion date,
    stock integer,
    borrado boolean
);


ALTER TABLE public.medicamentorespaldo OWNER TO postgres;

--
-- Name: medicamentos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medicamentos (
    id integer NOT NULL,
    id_tipo_medicamento integer NOT NULL,
    id_control integer NOT NULL,
    id_presentacion integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    stock_minimo integer DEFAULT 10,
    med_cronico boolean DEFAULT false
);


ALTER TABLE public.medicamentos OWNER TO postgres;

--
-- Name: medicamentos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medicamentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.medicamentos_id_seq OWNER TO postgres;

--
-- Name: medicamentos_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medicamentos_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.medicamentos_id_seq1 OWNER TO postgres;

--
-- Name: medicamentos_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.medicamentos_id_seq1 OWNED BY public.medicamentos.id;


--
-- Name: migedociv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migedociv (
    campo1 text,
    campo2 text,
    campo3 integer
);


ALTER TABLE public.migedociv OWNER TO postgres;

--
-- Name: modificacion_estatus_beneficiarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.modificacion_estatus_beneficiarios (
    id integer NOT NULL,
    fecha date DEFAULT CURRENT_DATE NOT NULL,
    id_usuario integer,
    cedula text,
    motivo text,
    estatus boolean DEFAULT false,
    hora character(11)
);


ALTER TABLE public.modificacion_estatus_beneficiarios OWNER TO postgres;

--
-- Name: modificacion_estatus_beneficiarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.modificacion_estatus_beneficiarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modificacion_estatus_beneficiarios_id_seq OWNER TO postgres;

--
-- Name: modificacion_estatus_beneficiarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.modificacion_estatus_beneficiarios_id_seq OWNED BY public.modificacion_estatus_beneficiarios.id;


--
-- Name: nivel_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_usuario_id_seq OWNER TO postgres;

--
-- Name: nivel_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_usuario_id_seq OWNED BY public.grupo_usuario.id;


--
-- Name: ocupacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocupacion (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.ocupacion OWNER TO postgres;

--
-- Name: ocupacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ocupacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ocupacion_id_seq OWNER TO postgres;

--
-- Name: ocupacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ocupacion_id_seq OWNED BY public.ocupacion.id;


--
-- Name: parentesco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parentesco (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.parentesco OWNER TO postgres;

--
-- Name: parentesco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.parentesco_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parentesco_id_seq OWNER TO postgres;

--
-- Name: parentesco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.parentesco_id_seq OWNED BY public.parentesco.id;


--
-- Name: pre_tipo_medicamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pre_tipo_medicamento (
    medicamento text
);


ALTER TABLE public.pre_tipo_medicamento OWNER TO postgres;

--
-- Name: presentacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.presentacion (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.presentacion OWNER TO postgres;

--
-- Name: presentacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.presentacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.presentacion_id_seq OWNER TO postgres;

--
-- Name: presentacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.presentacion_id_seq OWNED BY public.presentacion.id;


--
-- Name: reversos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reversos (
    id integer NOT NULL,
    tipo_de_reverso text,
    observacion text,
    id_usuario integer NOT NULL,
    id_medicamento integer NOT NULL,
    cantidad integer NOT NULL,
    fecha_reverso date DEFAULT CURRENT_DATE,
    id_entrada integer
);


ALTER TABLE public.reversos OWNER TO postgres;

--
-- Name: reversos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reversos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reversos_id_seq OWNER TO postgres;

--
-- Name: reversos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reversos_id_seq OWNED BY public.reversos.id;


--
-- Name: salidas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.salidas (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fecha_salida date DEFAULT CURRENT_DATE,
    id_medicamento integer,
    id_entrada integer NOT NULL,
    cedula_beneficiario text NOT NULL,
    cantidad integer NOT NULL,
    tipo_beneficiario text NOT NULL,
    borrado boolean DEFAULT false,
    control integer DEFAULT 0,
    id_medico integer
);


ALTER TABLE public.salidas OWNER TO postgres;

--
-- Name: salidas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.salidas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.salidas_id_seq OWNER TO postgres;

--
-- Name: salidas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.salidas_id_seq OWNED BY public.salidas.id;


--
-- Name: tipo_de_sangre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_de_sangre (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.tipo_de_sangre OWNER TO postgres;

--
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_de_sangre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_de_sangre_id_seq OWNER TO postgres;

--
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_de_sangre_id_seq OWNED BY public.tipo_de_sangre.id;


--
-- Name: tipo_medicamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_medicamento (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    categoria_id integer DEFAULT 1
);


ALTER TABLE public.tipo_medicamento OWNER TO postgres;

--
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_medicamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_medicamento_id_seq OWNER TO postgres;

--
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_medicamento_id_seq OWNED BY public.tipo_medicamento.id;


--
-- Name: titulares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.titulares (
    id bigint NOT NULL,
    cedula_trabajador integer NOT NULL,
    nacionalidad text NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    telefono text DEFAULT 'NO TIENE'::text NOT NULL,
    sexo integer NOT NULL,
    fecha_nacimiento date NOT NULL,
    tipo_de_personal text NOT NULL,
    ubicacion_administrativa text,
    borrado boolean DEFAULT false NOT NULL,
    estado_civil integer,
    tipo_de_sangre integer,
    grado_intruccion_id integer,
    ocupacion_id integer
);


ALTER TABLE public.titulares OWNER TO postgres;

--
-- Name: COLUMN titulares.sexo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.titulares.sexo IS '1="M",2="F"';


--
-- Name: titulares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.titulares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.titulares_id_seq OWNER TO postgres;

--
-- Name: titulares_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.titulares_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.titulares_id_seq1 OWNER TO postgres;

--
-- Name: titulares_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.titulares_id_seq1 OWNED BY public.titulares.id;


--
-- Name: titulares_migracion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.titulares_migracion (
    camapo1 text,
    camapo2 text,
    camapo3 text,
    camapo4 text,
    camapo5 text,
    camapo6 text,
    camapo7 text,
    camapo8 text,
    camapo9 text,
    camapo10 text
);


ALTER TABLE public.titulares_migracion OWNER TO postgres;

--
-- Name: titulares_repetidos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.titulares_repetidos (
    camapo1 text,
    veces bigint
);


ALTER TABLE public.titulares_repetidos OWNER TO postgres;

--
-- Name: titulares_respaldo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.titulares_respaldo (
    id bigint,
    cedula_trabajador integer,
    nacionalidad text,
    nombre text,
    apellido text,
    telefono text,
    sexo integer,
    fecha_nacimiento date,
    tipo_de_personal text,
    ubicacion_administrativa text,
    borrado boolean,
    estado_civil integer,
    tipo_de_sangre integer,
    grado_intruccion_id integer,
    ocupacion_id integer
);


ALTER TABLE public.titulares_respaldo OWNER TO postgres;

--
-- Name: unidad_medida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_medida (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.unidad_medida OWNER TO postgres;

--
-- Name: unidad_medida_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_medida_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_medida_id_seq OWNER TO postgres;

--
-- Name: unidad_medida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_medida_id_seq OWNED BY public.unidad_medida.id;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    cedula integer NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    usuario character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    tipousuario integer NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;

--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- Name: vista_beneficiarios; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vista_beneficiarios AS
 SELECT (titulares.cedula_trabajador)::text AS cedula,
    'T'::text AS tipo_beneficiario,
    titulares.nombre,
    titulares.apellido,
    titulares.telefono
   FROM public.titulares
UNION
 SELECT familiares.cedula,
    'F'::text AS tipo_beneficiario,
    familiares.nombre,
    familiares.apellido,
    familiares.telefono
   FROM public.familiares
UNION
 SELECT cortesia.cedula,
    'C'::text AS tipo_beneficiario,
    cortesia.nombre,
    cortesia.apellido,
    cortesia.telefono
   FROM public.cortesia;


ALTER TABLE public.vista_beneficiarios OWNER TO postgres;

--
-- Name: abortos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.abortos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.abortos_id_seq'::regclass);


--
-- Name: alergias_medicamentos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.alergias_medicamentos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.alergias_medicamentos_id_seq'::regclass);


--
-- Name: antecedentes_gineco_osbtetrico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_gineco_osbtetrico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_gineco_osbtetrico_id_seq'::regclass);


--
-- Name: antecedentes_patologicosf id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosf ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_patologicosf_id_seq'::regclass);


--
-- Name: antecedentes_patologicosp id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosp ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_patologicosp_id_seq'::regclass);


--
-- Name: antecedentes_quirurgicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_quirurgicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_quirurgicos_id_seq'::regclass);


--
-- Name: cesarias id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.cesarias ALTER COLUMN id SET DEFAULT nextval('historial_clinico.cesarias_id_seq'::regclass);


--
-- Name: consultas id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.consultas ALTER COLUMN id SET DEFAULT nextval('historial_clinico.consultas_id_seq'::regclass);


--
-- Name: detalle_notas_entrega id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega ALTER COLUMN id SET DEFAULT nextval('historial_clinico.detalle_notas_entrega_id_seq'::regclass);


--
-- Name: enfermedad_actual id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedad_actual ALTER COLUMN id SET DEFAULT nextval('historial_clinico.enfermedad_actual_id_seq'::regclass);


--
-- Name: enfermedades_sexuales id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedades_sexuales ALTER COLUMN id SET DEFAULT nextval('historial_clinico.enfermedades_sexuales_id_seq'::regclass);


--
-- Name: especialidades id_especialidad; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.especialidades ALTER COLUMN id_especialidad SET DEFAULT nextval('historial_clinico.especialidades_id_especialidad_seq'::regclass);


--
-- Name: examen_fisico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.examen_fisico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.examen_fisico_id_seq'::regclass);


--
-- Name: gestacion id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.gestacion ALTER COLUMN id SET DEFAULT nextval('historial_clinico.gestacion_id_seq'::regclass);


--
-- Name: habito_historial id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial ALTER COLUMN id SET DEFAULT nextval('historial_clinico.habito_historial_id_seq'::regclass);


--
-- Name: habitos_psicosociales id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habitos_psicosociales ALTER COLUMN id SET DEFAULT nextval('historial_clinico.habitos_psicosociales_id_seq'::regclass);


--
-- Name: historial_informativo id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_informativo ALTER COLUMN id SET DEFAULT nextval('historial_clinico.historial_informativo_id_seq'::regclass);


--
-- Name: historial_medico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_medico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.historial_medico_id_seq'::regclass);


--
-- Name: impresion_diag id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.impresion_diag ALTER COLUMN id SET DEFAULT nextval('historial_clinico.impresion_diag_id_seq'::regclass);


--
-- Name: medicamentos_patologias id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicamentos_patologias ALTER COLUMN id SET DEFAULT nextval('historial_clinico.medicamentos_medicina_interna_id_seq'::regclass);


--
-- Name: medicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.medicos_id_seq'::regclass);


--
-- Name: metodos_anticonceptibos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.metodos_anticonceptibos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.metodos_anticonceptibos_id_seq'::regclass);


--
-- Name: notas_entregas id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.notas_entregas ALTER COLUMN id SET DEFAULT nextval('historial_clinico.notas_entregas_id_seq'::regclass);


--
-- Name: numeros_romanos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.numeros_romanos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.numeros_romanos_id_seq'::regclass);


--
-- Name: paraclinicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.paraclinicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.paraclinicos_id_seq'::regclass);


--
-- Name: partos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.partos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.partos_id_seq'::regclass);


--
-- Name: plan id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.plan ALTER COLUMN id SET DEFAULT nextval('historial_clinico.plan_id_seq'::regclass);


--
-- Name: psicologia_primaria id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.psicologia_primaria ALTER COLUMN id SET DEFAULT nextval('historial_clinico.psicologia_primaria_id_seq'::regclass);


--
-- Name: reposos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.reposos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.reposos_id_seq'::regclass);


--
-- Name: categoria_inventario id; Type: DEFAULT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.categoria_inventario ALTER COLUMN id SET DEFAULT nextval('migracion.categoria_id_seq'::regclass);


--
-- Name: medicamentos_xls id; Type: DEFAULT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.medicamentos_xls ALTER COLUMN id SET DEFAULT nextval('migracion.medicamentos_xls_id_seq'::regclass);


--
-- Name: para_medicamentos id; Type: DEFAULT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.para_medicamentos ALTER COLUMN id SET DEFAULT nextval('migracion.para_medicamentos_id_seq'::regclass);


--
-- Name: tipo_medicamento id; Type: DEFAULT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.tipo_medicamento ALTER COLUMN id SET DEFAULT nextval('migracion.tipo_medicamento_por_categoria_id_seq'::regclass);


--
-- Name: entradas_def id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.entradas_def ALTER COLUMN id SET DEFAULT nextval('migracion_2.entradas_def_id_seq'::regclass);


--
-- Name: medicamentos_def id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.medicamentos_def ALTER COLUMN id SET DEFAULT nextval('migracion_2.medicamentos_def_id_seq'::regclass);


--
-- Name: para_entradas_prev_4 id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.para_entradas_prev_4 ALTER COLUMN id SET DEFAULT nextval('migracion_2.para_entradas_prev_4_id_seq'::regclass);


--
-- Name: para_medicamentos id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.para_medicamentos ALTER COLUMN id SET DEFAULT nextval('migracion_2.para_medicamentos_id_seq'::regclass);


--
-- Name: para_tipo_medicamento id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.para_tipo_medicamento ALTER COLUMN id SET DEFAULT nextval('migracion_2.para_tipo_medicamento_id_seq'::regclass);


--
-- Name: tipo_medicamento_1 id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.tipo_medicamento_1 ALTER COLUMN id SET DEFAULT nextval('migracion_2.tipo_medicamento_1_id_seq'::regclass);


--
-- Name: tipo_medicamento_def id; Type: DEFAULT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.tipo_medicamento_def ALTER COLUMN id SET DEFAULT nextval('migracion_2.tipo_medicamento_def_id_seq'::regclass);


--
-- Name: a1 id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.a1 ALTER COLUMN id SET DEFAULT nextval('public.a1_id_seq'::regclass);


--
-- Name: auditoria id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria ALTER COLUMN id SET DEFAULT nextval('public.auditoria_id_seq'::regclass);


--
-- Name: auditoria_de_sistema id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria_de_sistema ALTER COLUMN id SET DEFAULT nextval('public.auditoria_de_sistema_id_seq'::regclass);


--
-- Name: categoria_inventario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_inventario ALTER COLUMN id SET DEFAULT nextval('public.categoria_inventario_id_seq'::regclass);


--
-- Name: compuestos id_compuesto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos ALTER COLUMN id_compuesto SET DEFAULT nextval('public.compuestos_id_compuesto_seq1'::regclass);


--
-- Name: control id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.control ALTER COLUMN id SET DEFAULT nextval('public.control_id_seq'::regclass);


--
-- Name: cortesia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia ALTER COLUMN id SET DEFAULT nextval('public.cortesia_id_seq'::regclass);


--
-- Name: datos_titulares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.datos_titulares ALTER COLUMN id SET DEFAULT nextval('public.datos_titulares_id_seq'::regclass);


--
-- Name: entradas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradas ALTER COLUMN id SET DEFAULT nextval('public.entradas_id_seq'::regclass);


--
-- Name: estado_civil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil ALTER COLUMN id SET DEFAULT nextval('public.estado_civil_id_seq'::regclass);


--
-- Name: familiares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.familiares ALTER COLUMN id SET DEFAULT nextval('public.familiares_id_seq1'::regclass);


--
-- Name: grado_instruccion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_instruccion ALTER COLUMN id SET DEFAULT nextval('public.grado_instruccion_id_seq'::regclass);


--
-- Name: grupo_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_usuario ALTER COLUMN id SET DEFAULT nextval('public.nivel_usuario_id_seq'::regclass);


--
-- Name: medicamentos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicamentos ALTER COLUMN id SET DEFAULT nextval('public.medicamentos_id_seq1'::regclass);


--
-- Name: modificacion_estatus_beneficiarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modificacion_estatus_beneficiarios ALTER COLUMN id SET DEFAULT nextval('public.modificacion_estatus_beneficiarios_id_seq'::regclass);


--
-- Name: ocupacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion ALTER COLUMN id SET DEFAULT nextval('public.ocupacion_id_seq'::regclass);


--
-- Name: parentesco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parentesco ALTER COLUMN id SET DEFAULT nextval('public.parentesco_id_seq'::regclass);


--
-- Name: presentacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion ALTER COLUMN id SET DEFAULT nextval('public.presentacion_id_seq'::regclass);


--
-- Name: reversos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reversos ALTER COLUMN id SET DEFAULT nextval('public.reversos_id_seq'::regclass);


--
-- Name: salidas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salidas ALTER COLUMN id SET DEFAULT nextval('public.salidas_id_seq'::regclass);


--
-- Name: tipo_de_sangre id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_de_sangre ALTER COLUMN id SET DEFAULT nextval('public.tipo_de_sangre_id_seq'::regclass);


--
-- Name: tipo_medicamento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento ALTER COLUMN id SET DEFAULT nextval('public.tipo_medicamento_id_seq'::regclass);


--
-- Name: titulares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.titulares ALTER COLUMN id SET DEFAULT nextval('public.titulares_id_seq1'::regclass);


--
-- Name: unidad_medida id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_medida ALTER COLUMN id SET DEFAULT nextval('public.unidad_medida_id_seq'::regclass);


--
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- Data for Name: abortos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: alergias_medicamentos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: antecedentes_gineco_osbtetrico; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: antecedentes_patologicosf; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: antecedentes_patologicosp; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: antecedentes_quirurgicos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: cesarias; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--



--
-- Data for Name: consultas; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--


--
-- Name: abortos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.abortos_id_seq', 12, true);


--
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.alergias_medicamentos_id_seq', 17, true);


--
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_gineco_osbtetrico_id_seq', 6, true);


--
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_patologicosf_id_seq', 22, true);


--
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_patologicosp_id_seq', 31, true);


--
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_quirurgicos_id_seq', 32, true);


--
-- Name: cesarias_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.cesarias_id_seq', 11, true);


--
-- Name: consultas_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.consultas_id_seq', 291, true);


--
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.detalle_notas_entrega_id_seq', 576, true);


--
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.enfermedad_actual_id_seq', 206, true);


--
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.enfermedades_sexuales_id_seq', 10, true);


--
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.especialidades_id_especialidad_seq', 9, true);


--
-- Name: examen_fisico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.examen_fisico_id_seq', 36, true);


--
-- Name: gestacion_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.gestacion_id_seq', 12, true);


--
-- Name: habito_historial_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.habito_historial_id_seq', 31, true);


--
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.habitos_psicosociales_id_seq', 4, true);


--
-- Name: historial_informativo_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.historial_informativo_id_seq', 12, true);


--
-- Name: historial_medico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.historial_medico_id_seq', 378, true);


--
-- Name: impresion_diag_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.impresion_diag_id_seq', 39, true);


--
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.medicamentos_medicina_interna_id_seq', 30, true);


--
-- Name: medicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.medicos_id_seq', 20, true);


--
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.metodos_anticonceptibos_id_seq', 7, true);


--
-- Name: notas_entregas_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.notas_entregas_id_seq', 608, true);


--
-- Name: numeros_romanos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.numeros_romanos_id_seq', 10, true);


--
-- Name: paraclinicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.paraclinicos_id_seq', 31, true);


--
-- Name: partos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.partos_id_seq', 12, true);


--
-- Name: plan_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.plan_id_seq', 46, true);


--
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.psicologia_primaria_id_seq', 83, true);


--
-- Name: reposos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.reposos_id_seq', 26, true);


--
-- Name: categoria_id_seq; Type: SEQUENCE SET; Schema: migracion; Owner: postgres
--

SELECT pg_catalog.setval('migracion.categoria_id_seq', 3, true);


--
-- Name: medicamentos_xls_id_seq; Type: SEQUENCE SET; Schema: migracion; Owner: postgres
--

SELECT pg_catalog.setval('migracion.medicamentos_xls_id_seq', 325, true);


--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE SET; Schema: migracion; Owner: postgres
--

SELECT pg_catalog.setval('migracion.para_medicamentos_id_seq', 255, true);


--
-- Name: tipo_medicamento_por_categoria_id_seq; Type: SEQUENCE SET; Schema: migracion; Owner: postgres
--

SELECT pg_catalog.setval('migracion.tipo_medicamento_por_categoria_id_seq', 219, true);


--
-- Name: entradas_def_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.entradas_def_id_seq', 415, true);


--
-- Name: medicamentos_def_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.medicamentos_def_id_seq', 465, true);


--
-- Name: para_entradas_prev_4_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.para_entradas_prev_4_id_seq', 415, true);


--
-- Name: para_medicamentos_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.para_medicamentos_id_seq', 465, true);


--
-- Name: para_tipo_medicamento_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.para_tipo_medicamento_id_seq', 330, true);


--
-- Name: tipo_medicamento_1_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.tipo_medicamento_1_id_seq', 331, true);


--
-- Name: tipo_medicamento_def_id_seq; Type: SEQUENCE SET; Schema: migracion_2; Owner: postgres
--

SELECT pg_catalog.setval('migracion_2.tipo_medicamento_def_id_seq', 330, true);


--
-- Name: a1_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.a1_id_seq', 1391, true);


--
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auditoria_de_sistema_id_seq', 277, true);


--
-- Name: auditoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auditoria_id_seq', 7374, true);


--
-- Name: categoria_inventario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoria_inventario_id_seq', 4, true);


--
-- Name: compuestos_id_compuesto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compuestos_id_compuesto_seq', 9, true);


--
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compuestos_id_compuesto_seq1', 1, false);


--
-- Name: control_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.control_id_seq', 14, true);


--
-- Name: cortesia_adscrito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cortesia_adscrito_id_seq', 8, true);


--
-- Name: cortesia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cortesia_id_seq', 34, true);


--
-- Name: datos_titulares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.datos_titulares_id_seq', 486, true);


--
-- Name: ente_adscrito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ente_adscrito_id_seq', 4, true);


--
-- Name: entradas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.entradas_id_seq', 871, true);


--
-- Name: estado_civil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_civil_id_seq', 6, true);


--
-- Name: familiares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.familiares_id_seq', 19, true);


--
-- Name: familiares_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.familiares_id_seq1', 865, true);


--
-- Name: grado_instruccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grado_instruccion_id_seq', 6, true);


--
-- Name: medicamentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medicamentos_id_seq', 282, true);


--
-- Name: medicamentos_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medicamentos_id_seq1', 493, true);


--
-- Name: modificacion_estatus_beneficiarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.modificacion_estatus_beneficiarios_id_seq', 46, true);


--
-- Name: nivel_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_usuario_id_seq', 4, true);


--
-- Name: ocupacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ocupacion_id_seq', 4, true);


--
-- Name: parentesco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.parentesco_id_seq', 5, true);


--
-- Name: presentacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.presentacion_id_seq', 12, true);


--
-- Name: reversos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reversos_id_seq', 51, true);


--
-- Name: salidas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.salidas_id_seq', 926, true);


--
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_de_sangre_id_seq', 9, true);


--
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_medicamento_id_seq', 350, true);


--
-- Name: titulares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.titulares_id_seq', 1164, true);


--
-- Name: titulares_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.titulares_id_seq1', 434, true);


--
-- Name: unidad_medida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_medida_id_seq', 12, true);


--
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 39, true);


--
-- Name: abortos abortos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.abortos
    ADD CONSTRAINT abortos_pkey PRIMARY KEY (id);


--
-- Name: alergias_medicamentos alergias_medicamentos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.alergias_medicamentos
    ADD CONSTRAINT alergias_medicamentos_pkey PRIMARY KEY (id);


--
-- Name: antecedentes_gineco_osbtetrico antecedentes_gineco_osbtetrico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_gineco_osbtetrico
    ADD CONSTRAINT antecedentes_gineco_osbtetrico_pkey PRIMARY KEY (id);


--
-- Name: antecedentes_patologicosf antecedentes_patologicosf_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosf
    ADD CONSTRAINT antecedentes_patologicosf_pkey PRIMARY KEY (id);


--
-- Name: antecedentes_patologicosp antecedentes_patologicosp_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosp
    ADD CONSTRAINT antecedentes_patologicosp_pkey PRIMARY KEY (id);


--
-- Name: antecedentes_quirurgicos antecedentes_quirurgicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_quirurgicos
    ADD CONSTRAINT antecedentes_quirurgicos_pkey PRIMARY KEY (id);


--
-- Name: cesarias cesarias_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.cesarias
    ADD CONSTRAINT cesarias_pkey PRIMARY KEY (id);


--
-- Name: consultas consultas_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.consultas
    ADD CONSTRAINT consultas_pkey PRIMARY KEY (id);


--
-- Name: detalle_notas_entrega detalle_notas_entrega_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega
    ADD CONSTRAINT detalle_notas_entrega_pkey PRIMARY KEY (id);


--
-- Name: enfermedad_actual enfermedad_actual_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedad_actual
    ADD CONSTRAINT enfermedad_actual_pkey PRIMARY KEY (id);


--
-- Name: enfermedades_sexuales enfermedades_sexuales_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedades_sexuales
    ADD CONSTRAINT enfermedades_sexuales_pkey PRIMARY KEY (id);


--
-- Name: especialidades especialidades_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.especialidades
    ADD CONSTRAINT especialidades_pkey PRIMARY KEY (id_especialidad);


--
-- Name: examen_fisico examen_fisico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.examen_fisico
    ADD CONSTRAINT examen_fisico_pkey PRIMARY KEY (id);


--
-- Name: gestacion gestacion_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.gestacion
    ADD CONSTRAINT gestacion_pkey PRIMARY KEY (id);


--
-- Name: habitos_psicosociales habitos_psicosociales_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habitos_psicosociales
    ADD CONSTRAINT habitos_psicosociales_pkey PRIMARY KEY (id);


--
-- Name: historial_informativo historial_informativo_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_informativo
    ADD CONSTRAINT historial_informativo_pkey PRIMARY KEY (id);


--
-- Name: historial_medico historial_medico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_medico
    ADD CONSTRAINT historial_medico_pkey PRIMARY KEY (id);


--
-- Name: impresion_diag impresion_diag_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.impresion_diag
    ADD CONSTRAINT impresion_diag_pkey PRIMARY KEY (id);


--
-- Name: medicamentos_patologias medicamentos_medicina_interna_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicamentos_patologias
    ADD CONSTRAINT medicamentos_medicina_interna_pkey PRIMARY KEY (id);


--
-- Name: medicos medicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicos
    ADD CONSTRAINT medicos_pkey PRIMARY KEY (cedula);


--
-- Name: metodos_anticonceptibos metodos_anticonceptibos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.metodos_anticonceptibos
    ADD CONSTRAINT metodos_anticonceptibos_pkey PRIMARY KEY (id);


--
-- Name: notas_entregas notas_entregas_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.notas_entregas
    ADD CONSTRAINT notas_entregas_pkey PRIMARY KEY (id);


--
-- Name: numeros_romanos numeros_romanos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.numeros_romanos
    ADD CONSTRAINT numeros_romanos_pkey PRIMARY KEY (id);


--
-- Name: paraclinicos paraclinicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.paraclinicos
    ADD CONSTRAINT paraclinicos_pkey PRIMARY KEY (id);


--
-- Name: partos partos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.partos
    ADD CONSTRAINT partos_pkey PRIMARY KEY (id);


--
-- Name: habito_historial pk_habito_historial; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial
    ADD CONSTRAINT pk_habito_historial PRIMARY KEY (id);


--
-- Name: plan plan_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.plan
    ADD CONSTRAINT plan_pkey PRIMARY KEY (id);


--
-- Name: psicologia_primaria psicologia_primaria_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.psicologia_primaria
    ADD CONSTRAINT psicologia_primaria_pkey PRIMARY KEY (id);


--
-- Name: reposos reposos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.reposos
    ADD CONSTRAINT reposos_pkey PRIMARY KEY (id);


--
-- Name: categoria_inventario pd_categoria; Type: CONSTRAINT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.categoria_inventario
    ADD CONSTRAINT pd_categoria PRIMARY KEY (id);


--
-- Name: entradas_def entrada_pkey; Type: CONSTRAINT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.entradas_def
    ADD CONSTRAINT entrada_pkey PRIMARY KEY (id);


--
-- Name: medicamentos_def medicamento_pkey; Type: CONSTRAINT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.medicamentos_def
    ADD CONSTRAINT medicamento_pkey PRIMARY KEY (id);


--
-- Name: tipo_medicamento_1 tipo_medicamento_id; Type: CONSTRAINT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.tipo_medicamento_1
    ADD CONSTRAINT tipo_medicamento_id PRIMARY KEY (id);


--
-- Name: tipo_medicamento_def tipo_medicamento_pkey; Type: CONSTRAINT; Schema: migracion_2; Owner: postgres
--

ALTER TABLE ONLY migracion_2.tipo_medicamento_def
    ADD CONSTRAINT tipo_medicamento_pkey PRIMARY KEY (id);


--
-- Name: auditoria_de_sistema auditoria_de_sistema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria_de_sistema
    ADD CONSTRAINT auditoria_de_sistema_pkey PRIMARY KEY (id);


--
-- Name: auditoria auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);


--
-- Name: control control_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT control_pkey PRIMARY KEY (id);


--
-- Name: cortesia_adscrito cortesia_adscrito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT cortesia_adscrito_pkey PRIMARY KEY (id);


--
-- Name: ente_adscrito ente_adscrito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ente_adscrito
    ADD CONSTRAINT ente_adscrito_pkey PRIMARY KEY (id);


--
-- Name: entradas entrada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradas
    ADD CONSTRAINT entrada_pkey PRIMARY KEY (id);


--
-- Name: estado_civil estado_civil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil
    ADD CONSTRAINT estado_civil_pkey PRIMARY KEY (id);


--
-- Name: familiares familiares_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.familiares
    ADD CONSTRAINT familiares_pkey PRIMARY KEY (cedula);


--
-- Name: cortesia fk_cedula2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia
    ADD CONSTRAINT fk_cedula2 PRIMARY KEY (cedula);


--
-- Name: grado_instruccion grado_instruccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_instruccion
    ADD CONSTRAINT grado_instruccion_pkey PRIMARY KEY (id);


--
-- Name: medicamentos medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicamentos
    ADD CONSTRAINT medicamento_pkey PRIMARY KEY (id);


--
-- Name: modificacion_estatus_beneficiarios modificacion_estatus_beneficiarios_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modificacion_estatus_beneficiarios
    ADD CONSTRAINT modificacion_estatus_beneficiarios_pk PRIMARY KEY (id);


--
-- Name: grupo_usuario nivel_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_usuario
    ADD CONSTRAINT nivel_usuario_pkey PRIMARY KEY (id);


--
-- Name: ocupacion ocupacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_pkey PRIMARY KEY (id);


--
-- Name: categoria_inventario pd_categoria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_inventario
    ADD CONSTRAINT pd_categoria PRIMARY KEY (id);


--
-- Name: compuestos pk_id_compuesto; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos
    ADD CONSTRAINT pk_id_compuesto PRIMARY KEY (id_compuesto);


--
-- Name: parentesco pk_id_parentesco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parentesco
    ADD CONSTRAINT pk_id_parentesco PRIMARY KEY (id);


--
-- Name: presentacion presentacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_pkey PRIMARY KEY (id);


--
-- Name: reversos reversos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reversos
    ADD CONSTRAINT reversos_pkey PRIMARY KEY (id);


--
-- Name: salidas salida_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salidas
    ADD CONSTRAINT salida_id_pkey PRIMARY KEY (id);


--
-- Name: tipo_de_sangre tipo_de_sangre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_de_sangre
    ADD CONSTRAINT tipo_de_sangre_pkey PRIMARY KEY (id);


--
-- Name: tipo_medicamento tipo_medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento
    ADD CONSTRAINT tipo_medicamento_pkey PRIMARY KEY (id);


--
-- Name: titulares titulares_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.titulares
    ADD CONSTRAINT titulares_pkey PRIMARY KEY (cedula_trabajador);


--
-- Name: cortesia un_cortesia_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia
    ADD CONSTRAINT un_cortesia_id UNIQUE (id);


--
-- Name: unidad_medida unidad_medida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_medida
    ADD CONSTRAINT unidad_medida_pkey PRIMARY KEY (id);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- Name: habito_historial fk_habito_id; Type: FK CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial
    ADD CONSTRAINT fk_habito_id FOREIGN KEY (habito_id) REFERENCES historial_clinico.habitos_psicosociales(id);


--
-- Name: detalle_notas_entrega nota_entrega_id_fk; Type: FK CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega
    ADD CONSTRAINT nota_entrega_id_fk FOREIGN KEY (nota_entrega_id) REFERENCES historial_clinico.notas_entregas(id);


--
-- Name: tipo_medicamento tipo_medicamento_categoria_id_fkey; Type: FK CONSTRAINT; Schema: migracion; Owner: postgres
--

ALTER TABLE ONLY migracion.tipo_medicamento
    ADD CONSTRAINT tipo_medicamento_categoria_id_fkey FOREIGN KEY (categoria_id) REFERENCES migracion.categoria_inventario(id);


--
-- Name: auditoria auditoria_fk_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria
    ADD CONSTRAINT auditoria_fk_usuario_id FOREIGN KEY (id_usuario) REFERENCES public.usuarios(id);


--
-- Name: compuestos compuestos2_id_compuesto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos
    ADD CONSTRAINT compuestos2_id_compuesto_fkey FOREIGN KEY (id_compuesto) REFERENCES public.unidad_medida(id);


--
-- Name: entradas entrada_fk_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradas
    ADD CONSTRAINT entrada_fk_usuario_id FOREIGN KEY (user_id) REFERENCES public.usuarios(id);


--
-- Name: tipo_medicamento fk_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento
    ADD CONSTRAINT fk_categoria FOREIGN KEY (categoria_id) REFERENCES public.categoria_inventario(id);


--
-- Name: cortesia_adscrito fk_cortesia_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT fk_cortesia_id FOREIGN KEY (id_cortesia) REFERENCES public.cortesia(id);


--
-- Name: cortesia_adscrito fk_ente_asdcrito; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT fk_ente_asdcrito FOREIGN KEY (id_adscrito) REFERENCES public.ente_adscrito(id);


--
-- Name: modificacion_estatus_beneficiarios modificacion_estatus_beneficiarios_fk_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modificacion_estatus_beneficiarios
    ADD CONSTRAINT modificacion_estatus_beneficiarios_fk_usuario_id FOREIGN KEY (id_usuario) REFERENCES public.usuarios(id);


--
-- Name: salidas salida_fk_usuario_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salidas
    ADD CONSTRAINT salida_fk_usuario_id FOREIGN KEY (user_id) REFERENCES public.usuarios(id);


--
-- Name: usuarios usuarios_fk_tipousuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fk_tipousuario FOREIGN KEY (tipousuario) REFERENCES public.grupo_usuario(id);


--
-- PostgreSQL database dump complete
--

