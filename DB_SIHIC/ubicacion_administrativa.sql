--
-- PostgreSQL database dump
--

-- Dumped from database version 14.13 (Ubuntu 14.13-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 16.1 (Ubuntu 16.1-1.pgdg22.04+1)

-- Started on 2024-09-25 07:48:33 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 384 (class 1259 OID 82365)
-- Name: ubicacion_administrativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion_administrativa (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.ubicacion_administrativa OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 82364)
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_administrativa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.ubicacion_administrativa_id_seq OWNER TO postgres;

--
-- TOC entry 3762 (class 0 OID 0)
-- Dependencies: 383
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_administrativa_id_seq OWNED BY public.ubicacion_administrativa.id;


--
-- TOC entry 3610 (class 2604 OID 82368)
-- Name: ubicacion_administrativa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_administrativa ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_administrativa_id_seq'::regclass);


--
-- TOC entry 3756 (class 0 OID 82365)
-- Dependencies: 384
-- Data for Name: ubicacion_administrativa; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ubicacion_administrativa VALUES (1, 'DIRECCIÓN DE ARCHIVO Y CORREPONDENCIA', false);
INSERT INTO public.ubicacion_administrativa VALUES (2, 'DIRECCION DE COORDINACION Y APOYO LOGISTICO', false);
INSERT INTO public.ubicacion_administrativa VALUES (3, 'DIRECCION ACTOS ADMINISTRATIVOS', false);
INSERT INTO public.ubicacion_administrativa VALUES (4, 'DIRECCION DE ASUNTOS JUDICIALES', false);
INSERT INTO public.ubicacion_administrativa VALUES (5, 'DIRECCION DE CONTROL POSTERIOR', false);
INSERT INTO public.ubicacion_administrativa VALUES (6, 'INFORMACION Y PROMOCION DE LA PARTICIPACION CIUDADANA', false);
INSERT INTO public.ubicacion_administrativa VALUES (7, 'DIRECCION DE COMUNICACIONES', false);
INSERT INTO public.ubicacion_administrativa VALUES (8, 'DIRECCION DE MEDIOS AUDIOVISUALES', false);
INSERT INTO public.ubicacion_administrativa VALUES (9, 'DIRECCION DE RELACIONES PUBLICAS Y PROTOCOLO', false);
INSERT INTO public.ubicacion_administrativa VALUES (10, 'DIRECCION DE DESARROLLO ORGANIZACIONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (11, 'DIRECCION DE PLANIFICACION ESTRATEGICA', false);
INSERT INTO public.ubicacion_administrativa VALUES (12, 'DIRECCION DE PRESUPUESTO', false);
INSERT INTO public.ubicacion_administrativa VALUES (13, 'DIRECCION DE PLANIFICACION Y DESAROLLO DE PERSONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (14, 'DIRECCION DE ADMINISTRACION DE PERSONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (15, 'DIRECCION DE BIENESTAR SOCIAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (16, 'DIRECCION DE ADMINISTRACION Y FINANZAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (17, 'DIRECCION DE SERVICIOS GENERALES ', false);
INSERT INTO public.ubicacion_administrativa VALUES (18, 'DIRECCION DE BIENES PUBLICOS', false);
INSERT INTO public.ubicacion_administrativa VALUES (19, 'DIRECCION DE SOPORTE REDES Y TELECOMUNICACIONES', false);
INSERT INTO public.ubicacion_administrativa VALUES (20, 'DIRECCION DE PROYECTO E INNOVACION TECNOLOGICA ', false);
INSERT INTO public.ubicacion_administrativa VALUES (21, 'DIRECCION DE SISTEMAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (22, 'DIRECCION DE SEGURIDAD', false);
INSERT INTO public.ubicacion_administrativa VALUES (23, 'DIRECCION DE PROTECCION', false);
INSERT INTO public.ubicacion_administrativa VALUES (24, 'DIRECCION DE NEGOCIACIONES INTERNACIONALES', false);
INSERT INTO public.ubicacion_administrativa VALUES (25, 'DIRECCION DE SEGUIMIENTO DE ASUNTOS INTERNACIONALES', false);
INSERT INTO public.ubicacion_administrativa VALUES (26, 'DIRECCION DE DICTAMENES Y OPINIONES', false);
INSERT INTO public.ubicacion_administrativa VALUES (27, 'DIRECCION DE DETERMINACION DE RESPONSABILIDADES', false);
INSERT INTO public.ubicacion_administrativa VALUES (28, 'ATENCION DE DENUNCIAS QUEJAS RECLAMOS SUGERENCIAS Y PETICIONES', false);
INSERT INTO public.ubicacion_administrativa VALUES (29, 'DIRECCION GENERAL DEL DESPACHO', false);
INSERT INTO public.ubicacion_administrativa VALUES (30, 'OFICINA ESTRATÉGICA DE SEGUIMIENTO Y EVALUACIÓN DE POLÍTICAS PÚBLICAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (31, 'DESPACHO DEL MINISTRO (A)', false);
INSERT INTO public.ubicacion_administrativa VALUES (32, 'DIRECCIÓN GENERAL DE COMERCIO MAYORISTAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (33, 'OFICINA DE SEGURIDAD Y PROTECCIÓN INTEGRAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (34, 'CONSULTORIA JURIDICA', false);
INSERT INTO public.ubicacion_administrativa VALUES (35, 'DIRECCIÓN ESTADAL ANZOATEGUI', false);
INSERT INTO public.ubicacion_administrativa VALUES (36, 'DIRECCIÓN ESTADAL APURE', false);
INSERT INTO public.ubicacion_administrativa VALUES (37, 'DIRECCIÓN ESTADAL BARINAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (38, 'DIRECCIÓN ESTADAL CARABOBO', false);
INSERT INTO public.ubicacion_administrativa VALUES (39, 'DIRECCIÓN ESTADAL COJEDES', false);
INSERT INTO public.ubicacion_administrativa VALUES (40, 'DIRECCIÓN ESTADAL DELTA AMACURO', false);
INSERT INTO public.ubicacion_administrativa VALUES (41, 'DIRECCION ESTADAL DISTRITO CAPITAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (42, 'DIRECCION ESTADAL FALCON', false);
INSERT INTO public.ubicacion_administrativa VALUES (43, 'DIRECCION ESTADAL GUARICO', false);
INSERT INTO public.ubicacion_administrativa VALUES (44, 'DIRECCION ESTADAL LARA', false);
INSERT INTO public.ubicacion_administrativa VALUES (45, 'DIRECCION ESTADAL MERIDA', false);
INSERT INTO public.ubicacion_administrativa VALUES (46, 'DIRECCION ESTADAL MIRANDA', false);
INSERT INTO public.ubicacion_administrativa VALUES (47, 'DIRECCION ESTADAL MONAGAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (48, 'DIRECCION ESTADAL NUEVA ESPARTA', false);
INSERT INTO public.ubicacion_administrativa VALUES (49, 'DIRECCION ESTADAL PORTUGUESA', false);
INSERT INTO public.ubicacion_administrativa VALUES (50, 'DIRECCION ESTADAL SUCRE', false);
INSERT INTO public.ubicacion_administrativa VALUES (51, 'DIRECCION ESTADAL TACHIRA', false);
INSERT INTO public.ubicacion_administrativa VALUES (52, 'DIRECCION ESTADAL YARACUY', false);
INSERT INTO public.ubicacion_administrativa VALUES (53, 'DIRECCION ESTADAL ZULIA', false);
INSERT INTO public.ubicacion_administrativa VALUES (54, 'DIRECCION ESTADAL LA GUAIRA', false);
INSERT INTO public.ubicacion_administrativa VALUES (55, 'DIRECCION ESTADA ARAGUA', false);
INSERT INTO public.ubicacion_administrativa VALUES (56, 'DIRECCION ESTADAL BOLIVAR', false);
INSERT INTO public.ubicacion_administrativa VALUES (57, 'DIRECCION DE EVALUACION DE POLITICAS PUBLICAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (58, 'DIRECCION DE SEGUIMIENTO DE POLITICAS PUBLICAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (59, 'OFICINA DE AUDITORIA INTERNA', false);
INSERT INTO public.ubicacion_administrativa VALUES (60, 'OFICINA DE ATENCIÓN CIUDADANA', false);
INSERT INTO public.ubicacion_administrativa VALUES (61, 'OFICINA DE GESTIÓN COMUNICACIONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (62, 'OFICINA DE PLANIFICACIÓN Y PRESUPUESTO', false);
INSERT INTO public.ubicacion_administrativa VALUES (63, 'OFICINA DE GESTIÓN HUMANA', false);
INSERT INTO public.ubicacion_administrativa VALUES (64, 'OFICINA DE GESTION ADMINISTRATIVA', false);
INSERT INTO public.ubicacion_administrativa VALUES (65, 'OFICINA DE TECNOLOGÍAS DE INFORMACIÓN Y COMUNICACIÓN', false);
INSERT INTO public.ubicacion_administrativa VALUES (66, 'OFICINA DE INTEGRACIÓN Y ASUNTOS INTERNACIONALES', false);
INSERT INTO public.ubicacion_administrativa VALUES (67, 'DESPACHO DEL VICEMINISTERIO DE SEGUIMIENTO Y CONTROL DEL COMERCIO Y LA DISTRIBUCIÓN', false);
INSERT INTO public.ubicacion_administrativa VALUES (68, 'DESPACHO DEL VICEMINISTERIO SEG, EVALUACIÓN Y CONTROL DE PROCESO DE FORMACIÓN DE PRECIOS', false);
INSERT INTO public.ubicacion_administrativa VALUES (69, 'DIRECCIÓN GENERAL DE ARRENDAMIENTO COMERCIAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (70, 'DIRECCIÓN GENERAL DE RENDIMIENTOS, PRODUCTIVIDAD Y TASAS DE BENEFICIOS', false);
INSERT INTO public.ubicacion_administrativa VALUES (71, 'DIRECCIÓN GENERAL DE ANÁLISIS COMERCIAL Y ECONÓMICO', false);
INSERT INTO public.ubicacion_administrativa VALUES (72, 'DIRECCIÓN GENERAL DE POLÍTICA DE ABASTECIMIENTO', false);
INSERT INTO public.ubicacion_administrativa VALUES (73, 'DIRECCIÓN GENERAL DE PROTECCIÓN A LA PRODUCCIÓN NACIONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (74, 'DIRECCIÓN GENERAL DE CALIDAD E INNOVACIÓN', false);
INSERT INTO public.ubicacion_administrativa VALUES (75, 'DIRECCIÓN GENERAL DE CIRCUITO PUBLICO COMUNALES Y PYMES', false);
INSERT INTO public.ubicacion_administrativa VALUES (76, 'DIRECCIÓN GENERAL DE INSUMOS Y MATERIAS PRIMAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (77, 'DIRECCIÓN GENERAL DE COMERCIO MINORISTAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (78, 'DESPACHO DEL VICEMINISTERIO DE POLÍTICAS DE COMPRAS Y CONTENIDO NACIONAL', false);
INSERT INTO public.ubicacion_administrativa VALUES (79, 'DIRECCIÓN ESTADAL AMAZONAS', false);
INSERT INTO public.ubicacion_administrativa VALUES (80, 'DIRECCION ESTADAL TRUJILLO', false);


--
-- TOC entry 3763 (class 0 OID 0)
-- Dependencies: 383
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_administrativa_id_seq', 1, false);


-- Completed on 2024-09-25 07:48:33 -04

--
-- PostgreSQL database dump complete
--

